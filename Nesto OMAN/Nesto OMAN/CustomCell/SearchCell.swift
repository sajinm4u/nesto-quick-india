//
//  SearchCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 04/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var itemPic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
