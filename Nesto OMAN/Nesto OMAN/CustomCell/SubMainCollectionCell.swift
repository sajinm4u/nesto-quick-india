//
//  SubMainCollectionCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 27/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class SubMainCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    
}
