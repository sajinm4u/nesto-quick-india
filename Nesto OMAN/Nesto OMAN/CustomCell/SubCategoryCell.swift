//
//  SubCategoryCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 28/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit


protocol SubcategoryProductDelegate {
    
    func getProductDetails(product:OfferProductData)
    func loadMoreProduts()
}

class SubCategoryCell: UICollectionViewCell {
    
    
    @IBOutlet weak var productListCollectionView: UICollectionView!
    
    var delegate:SubcategoryProductDelegate?
    var totalProductCount:String?
    
    var productData:[OfferProductData]?{
        
        didSet{
            
           // print(productData)
            productListCollectionView.delegate = self
            productListCollectionView.dataSource = self
            productListCollectionView.reloadData()
            
        }
        
    }
    
    
   
    
    
}
extension SubCategoryCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       
        return self.productData?.count ?? 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
      
        
        
        if let productData = self.productData?[indexPath.row]{
                        
            delegate?.getProductDetails(product: productData)
            
        }
        
       
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
            
        
            
            if let info = self.productData?[indexPath.row]{
                
                if let title = info.product{
                    
                    cell.lblProductName.text = title
                    
                }
                
                if info.is_special_offer_product ?? false {
                                           
                                           cell.offerTagView.isHidden = false
                                       }else{
                                            cell.offerTagView.isHidden = true
                                       }
                
                
                
                if let off = info.offerPercentage {
                    
                    if Double(off)! > 0.0{
                        
                        cell.lblOff.text = off + "% OFF"
                        cell.offView.isHidden = false
                        cell.lblPrice.isHidden = false
                        
                        if info.is_special_offer_product ?? false {
                                                   
                        cell.offerTagView.isHidden = false
                        }else{
                        cell.offerTagView.isHidden = true
                        }
                        
                        if let price = info.groceryproductprice {
                                        
                                        let priceStr = mrp + currency + price
                                        
                                         cell.lblPrice.attributedText = priceStr.strikeThrough()
                                        
                                    }
                        
                        if let offerPrice = info.offerPrice{
                                           
                                           cell.lblOfferPrice.text = nrp + currency + offerPrice
                                           
                                       }
                        
                    }else{
                        
                        if let price = info.groceryproductprice {
                            
                            cell.lblOfferPrice.text = currency + price
                            
                        }
                        
                        cell.lblPrice.isHidden = true
                        cell.offView.isHidden = true
                        cell.offerTagView.isHidden = true
                        
                    }
                    
                    
                }else if let off = info.offer_percentage{
                    
                    
                    if Double(off)! > 0.0{
                                       
                                       cell.lblOff.text = off + "% OFF"
                                       cell.offView.isHidden = false
                                       cell.lblPrice.isHidden = false
                                       
                                       if info.is_special_offer_product ?? false {
                                                                  
                                       cell.offerTagView.isHidden = false
                                       }else{
                                       cell.offerTagView.isHidden = true
                                       }
                                       
                                       if let price = info.price {
                                                       
                                                       let priceStr = mrp + currency + price
                                                       
                                                        cell.lblPrice.attributedText = priceStr.strikeThrough()
                                                       
                                                   }
                                       
                                       if let offerPrice = info.offer_price{
                                                          
                                                          cell.lblOfferPrice.text = nrp + currency + offerPrice
                                                          
                                                      }
                
                    }else{
                        
                        if let price = info.price {
                            
                            cell.lblOfferPrice.text = nrp + currency + price
                            
                        }
                        
                        cell.lblPrice.isHidden = true
                        cell.offView.isHidden = true
                        cell.offerTagView.isHidden = true
                        
                    }
                    
                
                }else{
                    
                    if let price = info.price{
                        
                        cell.lblPrice.isHidden = true
                        cell.lblOfferPrice.text = nrp + currency + price
                    }
                    
                      cell.offView.isHidden = true
                    
                    if info.is_special_offer_product ?? false {
                                               
                    cell.offerTagView.isHidden = false
                    }else{
                    cell.offerTagView.isHidden = true
                    }
                     
                    
                    
                    
                }
                

                
                
                
                if let weight = info.weight {
                    
                    if let weightQty = info.qty_weight{
                        
                        let weightStr = weightQty + " " + weight
                        
                        cell.lblWeight.text = weightStr
                        
                        
                    }
                
                }
                
                if let imgUrl = info.image1{
                    
                    let url = URL(string: imgUrl)
                     let phImage = UIImage(named: ph)
                    
                     cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }
                
                
            }
        
        if indexPath.row == ((productData?.count ?? 0 )/2) - 1{
            
            if let total = Int(self.totalProductCount ?? "0"){
                
                if productData!.count < total {
                    
                     delegate?.loadMoreProduts()
                }
                
            }
      
            
            
        }
             
       
           
             
             
                return cell
            
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
              
               
               let width = (collectionView.frame.size.width - 5 * 2) / 2
        let height = width * 1.35 //ratio
               return CGSize(width: width, height: height)
               
           
    }
    
    
    
    
}
