//
//  BannerTableViewCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 10/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class BannerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var brandCollectionView: UICollectionView!
    
      var index:Int?
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
