//
//  TitleCollectionCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 28/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class TitleCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewHighlight: UIView!
    
    
    
   
    
    
    override var isSelected: Bool{
        didSet{
            
            setSelection()
            
        }
        
    }
    
    
    func setSelection(){
        
 
        
        if isSelected{
            
            lblTitle.isHighlighted = true
            viewHighlight.isHidden = false
            
        }else{
            
            lblTitle.isHighlighted = false
             viewHighlight.isHidden = true
            
        }
        
    }
    
    
    
}
