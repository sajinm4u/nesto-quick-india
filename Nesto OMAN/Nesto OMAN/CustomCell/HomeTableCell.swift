//
//  HomeTableCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 20/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit


protocol HomeTableDelegate {
    
    func viewAllProducts(at index:Int)
    
}

class HomeTableCell: UITableViewCell {
    
    
    
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    @IBOutlet weak var lblCategoryTitle: UILabel!
    var index:Int?
    var delegate:HomeTableDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func viewAllPressed(_ sender: UIButton) {
        
        if let index = self.index{
            
            delegate?.viewAllProducts(at: index)
        }
        
    }
    
}
