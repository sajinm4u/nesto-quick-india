//
//  MainCategoryCollectionCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 20/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class MainCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProduct: CurvedImage!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    
}
