//
//  HomeCollectionCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 20/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblOfferPrice: UILabel!
    @IBOutlet weak var imgProduct: CurvedImage!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOff: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var offView: CurvedView!
    @IBOutlet weak var offerTagView: UIImageView!
    
    
    
}
