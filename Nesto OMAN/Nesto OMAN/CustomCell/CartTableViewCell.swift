//
//  CartTableViewCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 26/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

protocol CartListDelegate {
    
    func updateCart(at index:Int,count:String)
    func deleteItem(at index:Int)
}

class CartTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var txtCount: UITextField!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    
    
    var itemCount = 1
    var cellIndex:Int?
    var delegate:CartListDelegate?
    var cellPrice:Double?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func removePressed(_ sender: Any) {
        
        if let index = self.cellIndex {
           
                 delegate?.deleteItem(at: index)

        }
        
        
    }
    
    @IBAction func countMinusPressed(_ sender: Any) {
        
        itemCount = Int(self.txtCount.text!) ?? 1
        
        if itemCount > 1{
                 
                 let count = self.txtCount.text
                 itemCount = (Int(count!) ?? 0) - 1
                 self.txtCount.text = "\(itemCount)"
                 self.lblCount.text = "x " + "\(itemCount)"
                 
                 if let price = self.cellPrice{
                            
                    let totalPrice = Double(itemCount) * Double(price)
                            
                    self.lblTotalAmount.text = currency + "\(totalPrice.roundToDecimal(3))"
                    
                    if let index = cellIndex{
                        if let count = self.txtCount.text{
                             delegate?.updateCart(at: index, count: count)
                            
                        }
                  
                    }
                    
                   
                    
                        }
                 
             }
        
        
    }
    
    @IBAction func countPlusPressed(_ sender: Any) {
        
        let count = self.txtCount.text
                        itemCount = (Int(count!) ?? 0) + 1
                        self.txtCount.text = "\(itemCount)"
                          self.lblCount.text = "x " + "\(itemCount)"
                        
                        if let price = self.cellPrice{
                                   
                                    let totalPrice = Double(itemCount) * Double(price)
                                   
                            self.lblTotalAmount.text = currency + "\(totalPrice.roundToDecimal(3))"
                            
                            if let index = cellIndex{
                                                  if let count = self.txtCount.text{
                                                       delegate?.updateCart(at: index, count: count)
                                                      
                                                  }
                                            
                                              }
                               }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
