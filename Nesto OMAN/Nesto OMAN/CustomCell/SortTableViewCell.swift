//
//  SortTableViewCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 07/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class SortTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var icRadio: UIImageView!
    
    var index:Int?
    
    private let checked = UIImage(named: "ic_green")
    private let unchecked = UIImage(named: "ic_radio_gray")
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    public func isSelected(_ selected: Bool) {
        setSelected(selected, animated: false)
        let image = selected ? checked : unchecked
        icRadio.image = image
    }
    
    
}

