//
//  AddressTableCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 01/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

protocol AddressTableDelegate {
    
    func EditAddress(at index:Int)
    func RemoveAddress(at index:Int)
    
}

class AddressTableCell: UITableViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var addressOne: UILabel!
    @IBOutlet weak var addressTwo: UILabel!
    @IBOutlet weak var addressThree: UILabel!
    @IBOutlet weak var addressFour: UILabel!
    @IBOutlet weak var btnRemove: CMButton!
    
    var delegate:AddressTableDelegate?
    var cellIndex:Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func editPressed(_ sender: Any) {
        
        if let index = self.cellIndex{
            
            delegate?.EditAddress(at: index)
            
        }
        
    }
    
    @IBAction func removePressed(_ sender: Any) {
        
        if let index = self.cellIndex{
                   
                   delegate?.RemoveAddress(at: index)
                   
               }
        
        
    }
    
    
    

}
