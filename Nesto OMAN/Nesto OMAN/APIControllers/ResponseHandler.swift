//
//  ResponseHandler.swift
//  Riddle Book
//
//  Created by Sajin M on 6/24/19.
//  Copyright © 2019 Sweans. All rights reserved.
//

import Foundation
import UIKit

class ResponseHandler {
    
    
    //
    var MainCategoryResponse: MainCategoryModel?
    var OfferProductsResponse: OfferProducts?
    var HomeSubCategoryResponse: OfferProducts?
    var StateData: [stateData]?
    var AreaData: [citiesData]?
    var StreetData: [streetData]?
    var LoginResponse: LoginModel?
    var LocationResponse: GroceryLocationModel?
    var SignUpResponse: SignUpModel?
    var CartListResponse: CartModel?
    var SubMainCategoryResponse: SubMainModel?
    var CategoryListResponse: CategoryListModel?
    var MainCategoryProductsResponse: ProductListModel?
    var NearestLocationResponse: StoreModel?
    var DeliveryAddressResponse: DeliveryAddressModel?
    var UserDetailsResponse: VerifyUser?
    var OrderHistoryResponse: OrderHistory?
    var FavoriteProductResponse: WishlistModel?
    var SearchResponse: SearchModel?
    var OrderDetailsResponse: OrderDetails?
    var ReturnPolicyResponse: PolicyModel?
    var ForgotPasswordResponse: ForgotModel?
    var BannerResponse: BannerModel?
    var SingleResponse: SingleProductModel?
    var BrandResponse: BrandModel?
    var BrandProductsResponse: BrandProductModel?
    var DeliverySlotResponse: TimeSlotModel?
    var DeliveryMethodResponse: DeliveryModel?
  
    
    
    
    public func goLogin(withParameter theParameter:[String : String], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string:Api.Login) else {return }
        
        
        ApiManager.shared.getLogin(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, loginData, message) in
            
            if loginData?.status == true {
                
                self.LoginResponse = loginData
                completion(true,message)
            } else {
                completion(false, message)
            }
        })
    }
    
  
    
    public func goSignUp(withParameter theParameter:[String : String], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string:Api.SignUp) else {return}
        
        
        ApiManager.shared.getSignUp(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, signUpData, message) in
            
            if signUpData?.status == true {
                
                self.SignUpResponse = signUpData
                completion(true,message)
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    
    public func getCategoryList(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.subCategory) else {return }
        
        
        ApiManager.shared.CategoryListData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, categoryListData, message) in
            
            if isSuccess{
                
                if categoryListData!.status {
                    
                    if (categoryListData?.arrSubCategory) != nil{
                        
                        self.CategoryListResponse = categoryListData
                        
                        completion(true,message)
                        
                    }
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getSubMainCategory(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.subMainCategory) else {return }
        
        
        ApiManager.shared.subMainData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, subMainData, message) in
            
            if isSuccess{
                
                if subMainData!.status {
                    
                    if (subMainData?.arrCategoryGroups) != nil{
                        
                        self.SubMainCategoryResponse = subMainData
                        
                        completion(true,message)
                        
                    }
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getSingleProduct(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.singleProduct) else {return}
        
        
        ApiManager.shared.getSingleProductData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, productData, message) in
            
            if isSuccess{
                
                if productData!.status {
                    
 
                        self.SingleResponse = productData
                        
                        completion(true,message)
     
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getCart(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.cartdetails) else {return }
        
        
        ApiManager.shared.cartListData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, cartData, message) in
            
            if isSuccess{
                
                if cartData!.status {
                    
                    if (cartData?.products) != nil{
                        
                        self.CartListResponse = cartData
                        
                        completion(true,message)
                        
                    }
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    
    public func addTocart(withParameter theParameter:[String : String], completion: @escaping ((Bool,String,String) -> Void)) {
        
        guard let url = URL(string:Api.addCart) else {return }
        
        
        ApiManager.shared.doAddToCat(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, cartData, message) in
            
            if isSuccess{
                
                if cartData!.status {
                    
                    if let cartCount = cartData?.cartCount{
                        
                        completion(true,cartCount,message)
                        
                    }
                    
                }
                
                
            }
                
            else {
                completion(false,"0",message)
            }
        })
    }
    
    
    
    
    
    public func sendOTP(withParameter theParameter:[String : String], completion: @escaping ((Bool,String,String) -> Void)) {
        
        guard let url = URL(string:Api.resendOtp) else {return }
        
        
        ApiManager.shared.sendSMS(fromUrl: url,withParameter:theParameter, completion: { (isSuccess,otpData,Otp) in
            
            if isSuccess{
                
                if otpData?.otp?.count ?? 0 > 3{
                    
                    completion(true,otpData?.message ?? "",(otpData?.otp)!)
                    
                }else{
                    
                    completion(false,otpData?.message ?? wentWrong,otpData?.otp ?? "")
                }
                
                
            }
                
            else {
                completion(false,wentWrong,"")
            }
        })
    }
    
    
    
    public func getNearestLocation(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.getNearestGrocery) else {return }
        
        
        ApiManager.shared.getNearestLocationData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, LocationData, message) in
            
            if isSuccess{
                
                if LocationData!.status {
                    
                    self.NearestLocationResponse = LocationData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getTimeSlot(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.timeSlots) else {return }
        
        
        ApiManager.shared.getDeliveryTimeslots(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, DeliveryData, message) in
            
            if isSuccess{
                
                if DeliveryData!.status {
                    
                    self.DeliverySlotResponse = DeliveryData
                    
                    completion(true,message)
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getAddressList(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.getDeliveryAddress) else {return }
        
        
        ApiManager.shared.getDeliveryAddress(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, DeliveryData, message) in
            
            if isSuccess{
                
                if DeliveryData!.status {
                    
                    self.DeliveryAddressResponse = DeliveryData
                    
                    completion(true,message)
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func getSearchLocation(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.getCustomerGrocery) else {return }
        
        
        ApiManager.shared.getSelectedLocation(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, LocationData, message) in
            
            if isSuccess{
                
                if LocationData!.status {
                    
                    self.LocationResponse = LocationData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getStates(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.getState) else {return }
        
        
        ApiManager.shared.getLocationData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, stateData, message) in
            
            if isSuccess{
                
                if stateData!.status {
                    
                    self.StateData = stateData?.state
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func getAreas(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.getStateCity) else {return }
        
        
        ApiManager.shared.getLocationData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, areaData, message) in
            
            if isSuccess{
                
                if areaData!.status {
                    
                    self.AreaData = areaData?.cities
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getCityStreet(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.getStateCityStreet) else {return }
        
        
        ApiManager.shared.getLocationData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, streetData, message) in
            
            if isSuccess{
                
                if streetData!.status {
                    
                    self.StreetData = streetData?.streets
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func logOut(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.logout) else {return}
        
        
        ApiManager.shared.confirmOrderData(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, message) in
            
            if isSuccess{
                
               
                    
                    completion(true,message)
                    
           
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getMainCategoryProducts(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.subCategoryProducts) else {return}
        
        
        ApiManager.shared.getCategoryProductListData(fromUrl: url, withParameter: theParameter, completion: { (isSuccess, productData, message) in
            
            if isSuccess{
                
                if productData!.status {
                    
                    self.MainCategoryProductsResponse = productData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getHomeSubCategory(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.homeSubCategory) else {return }
        
        
        ApiManager.shared.getOfferProductData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, OfferData, message) in
            
            if isSuccess{
                
                if OfferData!.status {
                    
                    self.HomeSubCategoryResponse = OfferData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    
    public func getOrderHistory(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.myOrders) else {return }
        
        
        ApiManager.shared.getOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, orderData, message) in
            
            if isSuccess{
                
                self.OrderHistoryResponse = orderData
                
                completion(true,message)
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    
    public func cartUpdate(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
            
            guard let url = URL(string:Api.cartUpdate) else {return}
            
            
            ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
                
                if isSuccess{
                    
                    completion(true,message)
                    
                }
                    
                else {
                    completion(false,message)
                }
            })
     }
    
    public func AddtoWishList(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
            
            guard let url = URL(string:Api.makeFavorite) else {return}
            
            
            ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
                
                if isSuccess{
                    
                    completion(true,message)
                    
                }
                    
                else {
                    completion(false,message)
                }
            })
     }
    
    
    
    public func PasswordReset(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
               
               guard let url = URL(string:Api.updatePassword) else {return}
               
               
               ApiManager.shared.updatePasswordData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
                   
                   if isSuccess{
                       
                       completion(true,message)
                       
                   }
                       
                   else {
                       completion(false,message)
                   }
               })
        }
    
    
    public func cancelOrder(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
               
               guard let url = URL(string:Api.cancelOrder) else {return}
               
               
               ApiManager.shared.cancelOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
                   
                   if isSuccess{
                       
                       completion(true,message)
                       
                   }
                       
                   else {
                       completion(false,message)
                   }
               })
        }
      
    
    public func RemoveWishList(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
             
             guard let url = URL(string:Api.makeUnFavorite) else {return}
             
             
             ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
                 
                 if isSuccess{
                     
                     completion(true,message)
                     
                 }
                     
                 else {
                     completion(false,message)
                 }
             })
      }
    
    
    
    public func deleteUserAddress(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
                 
                 guard let url = URL(string:Api.deleteAddress) else {return}
                 
                 
                 ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
                     
                     if isSuccess{
                         
                         completion(true,message)
                         
                     }
                         
                     else {
                         completion(false,message)
                     }
                 })
          }
    
    
    public func updateAddressData(url:String, withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
              
              guard let url = URL(string:url) else {return}
              
              
              ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
                  
                  if isSuccess{
                      
                      completion(true,message)
                      
                  }
                      
                  else {
                      completion(false,message)
                  }
              })
       }
    
    
    public func searchItem(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
                 
                 guard let url = URL(string:Api.searchProduct) else {return}
                 
                 
                 ApiManager.shared.searchProductData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess,searchData,message) in
                     
                     if isSuccess{
                        
                        self.SearchResponse = searchData
                         
                         completion(true,message)
                         
                     }
                         
                     else {
                         completion(false,message)
                     }
                 })
          }
         
      
    
    
    
    
    public func removeCartItem(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
           
           guard let url = URL(string:Api.removeCart) else {return}
           
           
           ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
               
               if isSuccess{
                   
                   completion(true,message)
                   
               }
                   
               else {
                   completion(false,message)
               }
           })
    }
    
    
    public func forgotPassword(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
            
            guard let url = URL(string:Api.forgotPassword) else {return}
            
            
            ApiManager.shared.forgotPasswordData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess,forgotData,message) in
                
                if isSuccess{
                    
                    self.ForgotPasswordResponse = forgotData
                    
                    completion(true,message)
                    
                }
                    
                else {
                    completion(false,message)
                }
            })
        }
     
    
    
    public func signInFCM(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
           
           guard let url = URL(string:Api.fcmSignIn) else {return}
           
           
           ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
               
               if isSuccess{
                   
                   completion(true,message)
                   
               }
                   
               else {
                   completion(false,message)
               }
           })
       }
    
    public func registerFCM(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
           
           guard let url = URL(string:Api.fcmRegister) else {return}
           
           
           ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
               
               if isSuccess{
                   
                   completion(true,message)
                   
               }
                   
               else {
                   completion(false,message)
               }
           })
       }
    
    public func checkOutOrder(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.cartCheckout) else {return}
        
        
        ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
            
            if isSuccess{
                
                completion(true,message)
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func getBrandList(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.brandList) else {return}
        
        
        ApiManager.shared.getBrandListData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess,BrandData, message) in
            
            if isSuccess{
                
                
                self.BrandResponse = BrandData
                
                completion(true,message)
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func basedOnBrand(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
          
          guard let url = URL(string:Api.byBrands) else {return}
          
          
          ApiManager.shared.getBrandProducts(fromUrl: url,withParameter:theParameter, completion: { (isSuccess,BrandProductData, message) in
              
              if isSuccess{
                  
                  
                  self.BrandProductsResponse = BrandProductData
                  
                  completion(true,message)
                  
              }
                  
              else {
                  completion(false,message)
              }
          })
      }
    
    
    
    
    public func getOrderDetails(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
          
          guard let url = URL(string:Api.orderDetails) else {return }
          
          
          ApiManager.shared.OrderDetailData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, orderData, message) in
              
              if isSuccess{
                  
                  self.OrderDetailsResponse = orderData
                  completion(true,message)
                  
              }
                  
              else {
                  completion(false,message)
              }
          })
      }
    
    
    public func returnPolicy(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.refundPolicy) else {return }
        
        
        ApiManager.shared.returnPolicyData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, returnData, message) in
            
            if isSuccess{
                
                self.ReturnPolicyResponse = returnData
                completion(true,message)
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func verifyUser(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.fcmRegistration) else {return }
        
        
        ApiManager.shared.completeVerificationWithFcm(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, customerData, message) in
            
            if isSuccess{
                
                self.UserDetailsResponse = customerData
                completion(true,message)
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getDeliveryMethods(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.deliveryMethod) else {return }
        
        
        ApiManager.shared.getDeliveryMethodData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, DeliveryData, message) in
            
            if isSuccess{
                
                if DeliveryData!.status {
                    
                    self.DeliveryMethodResponse = DeliveryData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func getOfferProducts(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.offerProduct) else {return }
        
        
        ApiManager.shared.getOfferProductData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, OfferData, message) in
            
            if isSuccess{
                
                if OfferData!.status {
                    
                    self.OfferProductsResponse = OfferData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    
    public func getFavouriteProducts(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.myWishList) else {return }
        
        
        ApiManager.shared.getFvouriteProductList(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, favoriteData, message) in
            
            if isSuccess{
                
                if favoriteData!.status {
                    
                    self.FavoriteProductResponse = favoriteData
                    
                    completion(true,message)
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getMainCategory(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.category) else {return }
        
        
        ApiManager.shared.getMainCategoryData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, categoryData, message) in
            
            if isSuccess{
                
                if categoryData!.status {
                    
                    self.MainCategoryResponse = categoryData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    
    public func getBanner(withParameter theParameter:[String : String], completion: @escaping ((Bool,[BannerData]?,String) -> Void)) {
        
        guard let url = URL(string:Api.banners) else {return }
        
        
        ApiManager.shared.getBannerData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, bannerData, message) in
            
            if isSuccess{
                
                if bannerData!.result {
                    
                    self.BannerResponse = bannerData
                    
                    completion(true,(bannerData?.banners)!,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                
                let banner = [BannerData]()
                
                completion(false,banner,message)
            }
        })
    }
    
    
    
}
