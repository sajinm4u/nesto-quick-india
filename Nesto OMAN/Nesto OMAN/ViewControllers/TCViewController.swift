//
//  TCViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 01/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import WebKit

class TCViewController: BaseViewController {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         webView.load(URLRequest(url: URL(string:tcUrl)!))

        // Do any additional setup after loading the view.
    }
    


    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    
}
