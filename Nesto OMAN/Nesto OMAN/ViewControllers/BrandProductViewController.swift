//
//  BrandProductViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 11/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import BadgeSwift

class BrandProductViewController: BaseViewController {
    
    
    @IBOutlet weak var bannerImg: UIImageView!
     
     @IBOutlet weak var productCollectionView: UICollectionView!
     var brandProducts:[OfferProductData]?
     
     var brand:String?
     var bannerUrl:String?
     
     var sortType = ""
     var customerId:String? = "0"
     var index:Int? = 0
     var totalCount:Int?
     var page = 1
    
    @IBOutlet weak var sortTableView: UITableView!
    
    var sortArray : [Sort] = [Sort(title:"A -- Z",selected:false),Sort(title:"Price -- Low to High",selected:false),Sort(title:"Price -- High to Low",selected:false)]
    
      var sortKey = ["","ASC","DESC"]
      
    private var selectedSort: Int? {
           didSet {
               sortTableView.reloadData()
           }
       }

    var isSelectedIndex:Int = 0
    
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var btnSortCancel: UIButton!
    @IBOutlet weak var lblBadge: BadgeSwift!
    
  
     
     @IBOutlet weak var lblTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    
    func initialSetup(){
        
        
        print(brandProducts)
        
               self.transparentView.isHidden = true
               self.btnSortCancel.isHidden = true
        
            if let custId = defualts.string(forKey: "customerId"){
                
                 self.customerId = custId
            }
            
            
           
            if let title = self.brand{
                
                self.lblTitle.text = title
           
            }
            
           
        
        guard let brand = self.brand else{
            
            return
            
        }
        
        self.getBrandProducts(brand: brand, sort: self.sortType, page: "\(self.page)")

        self.sortTableView.delegate = self
        self.sortTableView.dataSource = self
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
           
           if let custId = defualts.string(forKey: "customerId"){
           
           if let count = defualts.string(forKey: "badge"){
               
               if count != "0"{
                   
                   self.lblBadge.isHidden = false
                   self.lblBadge.text = count
                   
               }else{
                    self.lblBadge.isHidden = true
                   
               }
               
              
            }
           }else{
               
                self.lblBadge.isHidden = true
               
           }
           
       }
    
    
    
    @IBAction func cartPressed(_ sender: Any) {
        
        
        if defualts.value(forKey: "customerId") != nil{
            
            
            
            if let id = defualts.string(forKey: "customerId"){
                
                self.customerId = id
                
                let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
                           
                           if let navigator = self.navigationController {
                               
                               navigator.pushViewController(CartScene, animated: true)
                           }
                           
                           
                
            }
            
        }else{
            
            
            self.STAlert.alert(title:AppName , message: pleaseLogin )
                .action(.destructive("Login")){
                    
                    let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                        navigator.pushViewController(loginScene, animated: true)
                        
                        
                        
                    }
                    
                    
            }.action(.cancel("Cancel"))
                .show(on: self)
            
            
        }
        
        
        
        
        
        
    }
    
    
       
    
    @IBAction func sortPressed(_ sender: Any) {
        
       
           self.transparentView.isHidden = false
           self.btnSortCancel.isHidden = false
              
              let screenSize = UIScreen.main.bounds.size
        self.sortView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)

        self.transparentView.alpha = 0
              
        self.transparentView.backgroundColor = UIColor.clear
                            
              UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                
                self.transparentView.alpha = 1
                
                self.transparentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6698416096)
                   self.sortView.frame = CGRect(x: 0, y: screenSize.height - height, width: screenSize.width, height: height)
              }, completion: nil)
        
        
        
        
        
    }
    
    
    
    
    private func updateSelectedIndex(_ index: Int) {
        selectedSort = index
    }

    
    @IBAction func cancelSortPressed(_ sender: Any) {
        
        onClickTransparentView()
        
    }
    
    
    
    
         func onClickTransparentView() {
            
    
            let screenSize = UIScreen.main.bounds.size
           
            

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                self.transparentView.alpha = 0
                //self.transparentView.backgroundColor = UIColor.clear
                 self.sortView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)
                 
            }, completion: nil)
           // self.transparentView.isHidden = true
        }
    
    
    
    
    func getBrandProducts(brand:String,sort:String,page:String){
        
        self.STProgress.show()
                
                guard let groceryId = defualts.string(forKey: "groceryId") else {
                         return
                     }
                       
        let params = BrandProductParams(groceryId:groceryId,brand:brand,customerId:self.customerId!,limit:page,sortKey:self.sortType).Values
                              
                              ApiRequest.basedOnBrand(withParameter: params) { (isSuccess,message) in
                               
                                self.STProgress.dismiss()
                                              
                                              if isSuccess {
                                                  
                                                if let data = self.ApiRequest.BrandProductsResponse?.products {
                                                      
                                                    if let imgUrl = self.ApiRequest.BrandProductsResponse?.brand_banner {
                                                                   
                                                               let url = URL(string: imgUrl)
                                                                                   let phImage = UIImage(named: ph)
                                                                                  
                                                        self.bannerImg?.kf.setImage(with: url, placeholder: phImage)
                                                           
                                                                   }
                                                    
                                                    if let total = self.ApiRequest.BrandProductsResponse?.total_count{
                                                        
                                                        self.totalCount = Int(total) ?? 0
                                                        
                                                    }
                                                    
                                                    if data.count > 0{
                                                        
                                                        self.brandProducts = data
                                                        self.productCollectionView.delegate = self
                                                        self.productCollectionView.dataSource = self
                                                        self.productCollectionView.reloadData()
                                                        
                                                        
                                                    }
                                                      
                                                  }
                                              
                                         
                                             
                                               
                                              }
                
                }
                
                
             
             
         }
    
    
 

}

extension BrandProductViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                 
                
                 
                 let width = (collectionView.frame.size.width - 5 * 2) / 2
          let height = width * 1.35 //ratio
                 return CGSize(width: width, height: height)
                 
             
      }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.brandProducts?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        if let info = self.brandProducts?[indexPath.row]{
            
            let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                ProductDetailsScene.productData = info
                
                navigator.pushViewController(ProductDetailsScene, animated: true)
                
                
                
            }
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
              
                  
        if let info = self.brandProducts?[indexPath.row]{
                  
                  
                  
                  if let title = info.product{
                      
                      cell.lblProductName.text = title
                      
                  }
                  
                if info.is_special_offer_product ?? false {
                                           
                                           cell.offerTagView.isHidden = false
                                       }else{
                                            cell.offerTagView.isHidden = true
                                       }
                  
                  
                  if let off = info.offer_percentage{
                      
                      if Double(off)! > 0.0{
                          
                          cell.lblOff.text = off + "% OFF"
                          cell.offView.isHidden = false
                          cell.lblPrice.isHidden = false
                          
                          if let price = info.price{
                              
                              let priceStr = mrp + currency + price
                              
                              cell.lblPrice.attributedText = priceStr.strikeThrough()
                              
                          }
                        
                        
                          
                          if let offerPrice = info.offer_price{
                              
                              cell.lblOfferPrice.text = nrp + currency + offerPrice
                              
                          }
                          
                      }else{
                          
                          
                          if let price = info.price{
                              
                              cell.lblOfferPrice.text = currency + price
                          }
                          
                          cell.lblPrice.isHidden = true
                          cell.offView.isHidden = true
                          
                          
                          
                      }
                      
                      
                  }
                  
                  
                  
                  if let weight = info.weight {
                      
                      if let weightQty = info.qty_weight{
                          
                          let weightStr = weightQty + " " + weight
                          
                          cell.lblWeight.text = weightStr
                          
                          
                      }
                      
                  }
                  
                  if let imgUrl = info.image1{
                      
                      let url = URL(string: imgUrl)
                      let phImage = UIImage(named: ph)
                      
                      cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                      
                      
                  }
            
            
                if indexPath.row == ((brandProducts?.count ?? 0 )/2) - 1{
                    
                    if let total = self.totalCount {
                        
                        if brandProducts!.count < total {
                            
                            if let brand = self.brand {
                            
                                 self.page = page + 1
                                  self.getBrandProducts(brand:brand, sort: self.sortType, page: "\(self.page)")
                                
                            }
                            
                           
                            
                          
                            
                            // delegate?.loadMoreProduts()
                        }
                        
                    }
              
                    
                    
                }
            
                  
                  
            }
                  
                  
                  return cell
                  
      
                  
        
    }
    
    

    
}
extension BrandProductViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sortArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = self.sortTableView.dequeueReusableCell(withIdentifier: sortCell, for: indexPath) as! SortTableViewCell

            let sort = sortArray[indexPath.row]
            
            let currentIndex = indexPath.row
            
            let selected = currentIndex == selectedSort

            cell.lblTitle.text = (sort.title)
            
            cell.isSelected(selected)
        
            
                
                return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
                updateSelectedIndex(indexPath.row)
                onClickTransparentView()
                
               self.page = 1
               self.sortType = sortKey[indexPath.row]
        
               guard let brand = self.brand else{
                          
                          return
                          
                      }
        
               self.getBrandProducts(brand: brand, sort: self.sortType, page: "\(self.page)")
        
               
        
    }
    
    
    
    
}
