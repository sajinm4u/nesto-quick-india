//
//  CartViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 26/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import BadgeSwift

class CartViewController: BaseViewController {
    
    @IBOutlet weak var cartListTableView: UITableView!
    @IBOutlet weak var bottomView: CurvedView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var deliveryFee: UILabel!
    
    @IBOutlet weak var totalAmount: UILabel!
    
    @IBOutlet weak var specialInstructionView: UIView!
    
    @IBOutlet weak var txtInstruction: UITextField!
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var lblSubtotalTxt: UILabel!
    
    
  
    var minPruchaseAmount:Double = 0.0
    var totalValue:Double = 0.0
    var delFee:Double = 50.00
    var customerId:String?
    var groceryId:String?
    var isVary:Bool = false
    var isInstantDeliveryAvailable = true
   // var specialInstruction:String = ""
    
    var cartData:[CartProducts]?
    var deliveryMethod:[methodData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
        self.bottomView.isHidden = true
        self.emptyView.isHidden = true
        self.specialInstructionView.isHidden = true
        self.cartListTableView.isHidden = true
        self.bottomView.isHidden = true
        
        self.cartListTableView.tableFooterView = UIView(frame:.zero)
        if defualts.value(forKey: "customerId") != nil{
            
            if let id = defualts.string(forKey: "customerId"){
                
                if let groceryId = defualts.string(forKey: "groceryId"){
                    
                    self.groceryId = groceryId
                    
                    if isConnected(){
                        
                        
                                    let idValue = id
                                    self.customerId = id
                                       
                              getDeliveryMethod(customerId: idValue, groceryId: self.groceryId!)
                              getCartData(customerId: idValue, groceryId: self.groceryId!)
                             
                                       
                                   }
                    
                }
            
                
               
                
                
            }
            
        }
        
        
        
    }
    
    
    

    
    @IBAction func instructionOkPressed(_ sender: Any) {
        self.specialInstructionView.isHidden = true
    }
    
    
    
    
    
    @IBAction func addSpecialInstruction(_ sender: Any) {
        
        self.specialInstructionView.isHidden = false
        
    }
    
    
    
    @IBAction func continuePressed(_ sender: Any) {
        
        
        DispatchQueue.main.async {
            
            if self.isVary{
                
                self.showAlert(message: varyMessage)
            }
            
            
        }
        
        
        
        let DeliveryScene = DeliveryDetailsViewController.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            DeliveryScene.deliveryMethod = self.deliveryMethod
            DeliveryScene.minPurchaseAmt = self.minPruchaseAmount
            DeliveryScene.subtotalAmt = self.totalValue
            DeliveryScene.itemsCount = self.lblSubtotalTxt.text
            DeliveryScene.instantDeliveryAvailable = self.isInstantDeliveryAvailable
            navigator.pushViewController(DeliveryScene, animated: true)
            
            
            
        }
        
        
        
    }
    
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    func getCartData(customerId:String,groceryId:String){
        
        
        self.STProgress.show()
        
        let cartParam = CartParams(customerId:customerId,groceryId: groceryId).Values
        
        self.ApiRequest.getCart(withParameter:cartParam) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.CartListResponse?.products{
                    
                    
                    if self.cartData?.count ?? 0 > 0{
                        
                        
                        self.cartData?.removeAll()
                    }
                    
                    self.cartData = info
                
                    if self.cartData?.count ?? 0 > 0 {
                        
                        if self.cartData?.count == 1{
                            
                            self.lblSubtotalTxt.text = sub + "(\(self.cartData?.count ?? 0) item)"
                            
                        }else{
                            
                            self.lblSubtotalTxt.text = sub + "(\(self.cartData?.count ?? 0) items)"
                            
                        }
                        
                        
                                               
                       
                        
                        if let minPurchase = self.ApiRequest.CartListResponse?.min_purchase_amount{
                            
                            self.minPruchaseAmount = Double(minPurchase)!
                            
                        }
                        
                        if let count = self.cartData?.count{
                            
                            let count = "\(count)"
                           
                            defualts.set(count, forKey: "badge")
                            
                            self.totalValue = 0.0
                        }
                        
                        
                        for i in self.cartData!{
                            
                            if let price = i.price {
                                
                                if let qty = i.qty {
                                    
                                    let priceValue = Double(price)! * Double(qty)!
                                    self.totalValue = self.totalValue + priceValue
                                  
                                    
                                    
                                }
                                
                                
                            }
                    
                            
                        }
                        
                      
                        
                        if self.totalValue < self.minPruchaseAmount{
                            
                          
                            DispatchQueue.main.async {
                            
                                
                                self.lblSubTotal.text = currency + "\(self.totalValue.roundToDecimal(3))"
                                self.deliveryFee.text = currency + " \(self.delFee)"
                                self.deliveryFee.textColor = UIColor.black
                                self.totalAmount.text = currency + "\((self.delFee + self.totalValue).roundToDecimal(3))"
                               

                            }
                            
                            
                        }else{
                            
                               DispatchQueue.main.async {
                            
                            self.deliveryFee.text = "FREE"
                            self.deliveryFee.font = self.deliveryFee.font.bold()
                            self.deliveryFee.textColor = Colors.NOGreen
                            self.lblSubTotal.text = currency + "\(self.totalValue.roundToDecimal(3))"
                            self.totalAmount.text = currency + "\((self.totalValue).roundToDecimal(3))"
                            }
                            
                        }
                
                        
                        self.cartListTableView.delegate = self
                        self.cartListTableView.dataSource = self
                        self.cartListTableView.reloadData()
                        self.cartListTableView.isHidden = false
                        self.bottomView.isHidden = false
                        
                    }else{
                        
                        defualts.set("0", forKey: "badge")
                        self.cartListTableView.isHidden = true
                        self.emptyView.isHidden = false
                        self.bottomView.isHidden = true
                        
                    }
                    
                    
                }
                
                
            }else{
                
            }
        }
        
    }
    
    
    func getDeliveryMethod(customerId:String,groceryId:String){
        
        self.STProgress.show()
              
              let cartParam = CartParams(customerId:customerId,groceryId: groceryId).Values
              
              self.ApiRequest.getDeliveryMethods(withParameter:cartParam) { (isSuccess,message) in
                  
                  self.STProgress.dismiss()
                  
                if isSuccess {
                      
                    if let info = self.ApiRequest.DeliveryMethodResponse?.result{
                          
                    
                        if info.count > 0 {
                            
                             self.deliveryMethod = info
                            
                            if let isInstant = info[0].is_method_active{
                                
                                if isInstant == 0{
                                    
                                    self.isInstantDeliveryAvailable = false
                                    
                                }
                            }
                            
                            
                            if let freeCharge =  info[1].addition_charge{
                                
                                self.delFee = Double(freeCharge)!
                                
                            }
                            
                        }
                          
                          
                      }
                      
                      
                  }
              }
        
        
    }
    
    
    func updateCartItem(productId:String,quanity:String,price:String){
        
        guard let customerId = defualts.string(forKey: "customerId") else {
            return
        }
        
        
        let cartParam = updateParam(id: productId, quantity: quanity, price: price, customerId:customerId).Values
        
        self.ApiRequest.cartUpdate(withParameter:cartParam) { (isSuccess,message) in
            
           // self.STProgress.dismiss()
            
            if isSuccess {
                
                if message.contains("mum") {
                     self.showAlert(message: message)
                }
                
                
                if let id = self.customerId{
                    
                    self.getCartData(customerId: id, groceryId: self.groceryId!)
                    
                }
               
                
            }else{
                
                self.showAlert(message: message)
            }
        }
        
    }
    
    
    func removeCartItem(productId:String){
        
        
       guard let customerId = defualts.string(forKey: "customerId") else {
                  return
              }
        
        
        self.STProgress.show()
        
        let cartParam = ["id":productId,"customerId":customerId]
        
        self.ApiRequest.removeCartItem(withParameter:cartParam) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let id = self.customerId{
                    
                
                    
                    self.getCartData(customerId: id, groceryId: self.groceryId!)
                    
                }
               
               
                            
            
                
            }else{
                
                self.showAlert(message: message)
            }
        }
        
    }
    
    
    
    
}
extension CartViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.cartData?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.cartListTableView.dequeueReusableCell(withIdentifier: cartCell) as! CartTableViewCell
        
        
        if let info = self.cartData?[indexPath.row] {
            
            if let title = info.product_name{
                
                cell.lblTitle.text = title
                
            }
            
            
            if let quantity = info.qty{
                
                
                if let price = info.price{
                    
                    let priceValue = Double(price)!.roundToDecimal(3)
                    
                    cell.cellPrice = priceValue
                    
                    cell.lblPrice.text = currency + "\(priceValue)"
                    
                    cell.lblCount.text = "x " + quantity
                    
                    cell.txtCount.text = quantity
                    
                    let totalPrice = (Double(price)!.roundToDecimal(3) * Double(quantity)!).roundToDecimal(3)
                    
                    cell.lblTotalAmount.text = currency + "\(totalPrice)"
                    
                   
                    
                    
                    
                }
                
            }
            
            if let weight = info.qty_weight{
                
                if let weightType = info.weight{
                    
                    if weightType != "Pc"{
                        
                        self.isVary = true
                    }
                    
                    cell.lblWeight.text = weight + " " + weightType
                    
                }
                
                
            }
            
            
            if let imgUrl = info.product_image1{
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            
        }
        
        cell.cellIndex = indexPath.row
        cell.delegate = self
        
        return cell
    }
    
    
    
}

extension CartViewController:CartListDelegate{
    
    
    func updateCart(at index: Int, count: String) {
        
        
        if let info = cartData?[index]{
            
            if let price = info.price{
                
                if let id = info.id{
                    
                     self.updateCartItem(productId:"\(id)",quanity:count,price:price)
                    
                    
                }
                
               
            }
            
           
            
        }
        
        
    }
    
    func deleteItem(at index: Int) {
        
        if let info = cartData?[index]{
            
            if let id = info.id{
                
                self.removeCartItem(productId: "\(id)")
            }
            
            
        }
        
    }
    
    
    
    
}
