//
//  OrderDetailsViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 07/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import BadgeSwift

class OrderDetailsViewController: BaseViewController {
    
    
    var groceryName:String?
    var orderSysId:String?
    var totalValue:Double = 0.0
    var orderId:String?
    var delFee:Double = 1.50
    var deliveryDate:String?
    var netAmount:String?
    var itemCount:String?
    var status:String?
    
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStore: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblSysId: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var btnReturn: CMButton!
    @IBOutlet weak var cancelView: UIView!
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblQty: BadgeSwift!
    
    var orderData:[orderDetailsData]?
    
     @IBOutlet weak var cartListTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        initalSetup()
        // Do any additional setup after loading the view.
    }
    
    
    
    func initalSetup(){
        
        if let status = self.status{
            
            
            if status == "1"{
                self.cancelView.isHidden = false
                
            }
            else{
                
                self.cancelView.isHidden = true
            }
            
        }
        
        
        self.btnReturn.isHidden = true
        
        if let city = defualts.string(forKey: "city"){
            
            self.lblCity.text = city
        }
        
        
        if let name = self.groceryName{
            
            self.lblStore.text = name
            
        }
        if let netAmt = self.netAmount{
                  
                  self.lblTotal.text = currency + netAmt
                  
              }
        if let count = self.itemCount{
                  
                  self.lblQty.text = count
                  
              }
        if let status = self.status{
            
            if status == "4"{
                
                self.btnReturn.isHidden = false
            }else{
                 self.btnReturn.isHidden = true
                
            }
                  
                self.lblStatus.text = Utils.orderStatus(type: status)
                self.lblStatus.textColor = Utils.orderStatusColor(type: status)
                
                  
              }
        if let sysId = self.orderSysId{
            
            self.lblSysId.text = sysId
        }
            
        if let date = self.deliveryDate{
            
            self.lblDate.text = date
        }
              
              
        
        
        
        
        self.cartListTableView.tableFooterView = UIView(frame:.zero)
       
                    
                    if isConnected(){
                        if let id = self.orderId{
                            
                              getCartData(orderId:id)
                            
                        }
                        
                                
                            
                                       
                                       
                                   }
         
        
    }
    
    
    @IBAction func helpPressed(_ sender: Any) {
        
        guard let phone = defualts.string(forKey: "groceryPhone") else{
               
               return
           }
           


           let whatsUrl = "whatsapp://send?phone="
           let whatsMessage = "&text=Hi%2C%20Nesto"
           
           var whatsStr = whatsUrl + phone + whatsMessage
           
          
           
           whatsStr = whatsStr.replacingOccurrences(of: "+", with: "")
          
           
           let whatsappURL = URL(string: whatsStr)
           if let whatsappURL = whatsappURL {
               if UIApplication.shared.canOpenURL(whatsappURL) {
                   UIApplication.shared.open(whatsappURL)
               }
           }
           
    }
    
    
    
    @IBAction func returnPolicyPressed(_ sender: Any) {
        
        
        let ReturnScene = ReturnViewController.instantiate(fromAppStoryboard: .Main)
            
                                                if let navigator = self.navigationController {
                                                    
                                                   
                                                    
                                                    ReturnScene.groceryCity = self.lblCity.text
                                                    ReturnScene.groceryTitle = self.lblStore.text
                                                
                                                    
                                                    navigator.pushViewController(ReturnScene, animated: true)
            
            
            
                                                }
        
    }
    
    
    
    @IBAction func cancelOrderPressed(_ sender: Any) {
        
        
                   self.STAlert.alert(title:confirm, message: confimMessage )
                                     .action(.destructive("Continue")){
                                        
                                        guard let custId = defualts.string(forKey: "customerId") else{
                                                   return
                                                   
                                               }
                                        
                                        guard let orderId = self.orderId else{
                                            return
                                                                                          
                                                                                      }
                                        
                                        
                                         
                                        self.cancelOrder(orderId: orderId,customerId:custId)
                                      
                                         
                                 }.action(.cancel("Cancel"))
                                     .show(on: self)
        
        
    }
    
    
    func cancelOrder(orderId:String,customerId:String){
        
        
          self.STProgress.show()
        
        let params = CancelProductParameters(orderId: orderId, customerId: customerId).Values
                      
                      ApiRequest.cancelOrder(withParameter: params) { (isSuccess,message) in
                       
                        self.STProgress.dismiss()
                                      
                                      if isSuccess {
                                        
                                         NotificationCenter.default.post(name: Notification.Name(MyOrderReload), object: nil)
                                        
                                        DispatchQueue.main.async {
                                                      self.showAlert(message: message)
                                                      }
                                          
                                        self.backNavigation()
                                                
                                                       
                                      }else{
                                        
                                        
                                        DispatchQueue.main.async {
                                                      self.showAlert(message: message)
                                                      }
                                          
                                        self.backNavigation()
                                    
                                              
                                          }
                                      
                                 
                                     
                                       
                                      }
        
        }
        

    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
   
    func getCartData(orderId:String){
        
        
        guard let customerId = defualts.string(forKey: "customerId") else{
         return
            
        }
        
        
        self.STProgress.show()
        
       
        
        self.ApiRequest.getOrderDetails(withParameter:["orderId":orderId,"customerId":customerId]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.OrderDetailsResponse?.orderDetails{
                    
                      self.orderData = info
                     
                    if let shipAddres = self.ApiRequest.OrderDetailsResponse?.shipAddress{
                        
                         self.lblCity.text = shipAddres
                        
                    }
                  
                    
                    if self.orderData?.count ?? 0 > 0{
                        
                        
                    }
                
                        
                        for i in self.orderData!{
                            
                            if let price = i.product_price {
                                
                                if let qty = i.qty {
                                    
                                    let priceValue = Double(price)! * Double(qty)!
                                    self.totalValue = self.totalValue + priceValue
                                  
                                
                                }
                                
                                
                            }
                    
                            
                        }
                        
                      
                        if self.totalValue < 20{
                            
                            self.delFee = 1.500
                  
                            
                        }
                        self.cartListTableView.delegate = self
                        self.cartListTableView.dataSource = self
                        self.cartListTableView.reloadData()
                        self.cartListTableView.isHidden = false
                       
                        
                    }else{
                        
                    
                      
                        self.cartListTableView.isHidden = true
                      
                        
                    }
                    
                    
                }
                
        
        }
        
    }

}

extension OrderDetailsViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.orderData?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.cartListTableView.dequeueReusableCell(withIdentifier: cartCell) as! CartTableViewCell
        
        
        if let info = self.orderData?[indexPath.row] {
            
            if let title = info.product_name{
                
                cell.lblTitle.text = title
                
            }
            
            
            if let quantity = info.qty{
                
                
                if let price = info.product_price{
                    
                    let priceValue = Double(price)!.roundToDecimal(3)
                    
                    cell.cellPrice = priceValue
                    
                    cell.lblPrice.text = currency + "\(priceValue)"
                    
                    if let qty = info.qty {
                        cell.lblCount.text = "x " + qty
                                                                
                    }
                    
                   
                    
                    let totalPrice = (Double(price)!.roundToDecimal(3) * Double(quantity)!).roundToDecimal(3)
                    
                    cell.lblTotalAmount.text = currency + "\(totalPrice)"
                    
                   
                    
                    
                    
                }
                
            }
            
            if let weight = info.qty_weight{
                
                if let weightType = info.weight{
                    
                    cell.lblWeight.text = weight + " " + weightType
                    
                }
                
                
            }
            
            
            if let imgUrl = info.product_image1{
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            
        }
        
    
        return cell
    }
    
    
    
}
