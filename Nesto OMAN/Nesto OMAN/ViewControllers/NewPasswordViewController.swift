//
//  NewPasswordViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 07/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class NewPasswordViewController: BaseViewController {
    
    
    @IBOutlet weak var txtNewPassword: TextfiledWithImage!
    
    var phoneNumber:String?
    var customerId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        
        guard let txtPassword = self.txtNewPassword.text, !txtPassword.isEmpty  else {
            
            showAlert(message:"Please enter a valid password")
            return
        }
        
        
        if txtPassword.count < 5 {
            
            showAlert(message: passwordMessage)
            return
            
        }
        
        
        guard let customerId = self.customerId else {
            
            return
            
        }
        
        
        self.submitNewPassword(customerId: customerId,password: txtPassword)
        
        
    }
    
    
    func submitNewPassword(customerId:String,password:String){
        
        
        self.STProgress.show()
        
        
        self.ApiRequest.PasswordReset(withParameter:["customerId":customerId,"newPassword":password]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                
                
                
                let LoginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    LoginScene.newPasswordSet = true
                    navigator.pushViewController(LoginScene, animated: true)
                    
                    
                    
                }
                
                self.showAlert(message: "Password Updated Successfully")
                
                
                
                
            }else{
                
                
                
                self.showAlert(message: wentWrong)
                
            }
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
}
