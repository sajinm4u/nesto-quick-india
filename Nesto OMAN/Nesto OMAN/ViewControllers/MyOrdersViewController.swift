//
//  MyOrdersViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 03/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class MyOrdersViewController: BaseViewController {
    
    @IBOutlet weak var orderTableView: UITableView!
    
    var orderList:[OrderData]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()

        // Do any additional setup after loading the view.
    }
    
    
    
    
    func initialSetup(){
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.CancelOrderNotification(notification:)), name: Notification.Name(MyOrderReload), object: nil)
        
        guard let custId = defualts.string(forKey: "customerId") else{
            return
            
        }
        
        self.orderTableView.tableFooterView = UIView(frame:.zero)
        getOrderList(customerId: custId, limit: "0" )
        
    }
    
    
    
      @objc func CancelOrderNotification(notification: Notification) {
        
        guard let custId = defualts.string(forKey: "customerId") else{
                   return
                   
               }
        
         getOrderList(customerId: custId, limit: "0" )
        
    }
    
    @IBAction func startShopping(_ sender: Any) {
        
         self.backNavigation()
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
   
    func getOrderList(customerId:String,limit:String){
    
            
             self.STProgress.show()
            
            let Param = CustomerParams(customerId:customerId,limit: "0").Values
                        self.ApiRequest.getOrderHistory(withParameter:Param) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                             
                                                if let info = self.ApiRequest.OrderHistoryResponse?.customerhistory{
                                                    
                                                    self.orderList = info
                                                    
                                                    if self.orderList?.count ?? 0 > 0 {
                                                        
                                                        self.orderTableView.delegate = self
                                                        self.orderTableView.dataSource = self
                                                        self.orderTableView.reloadData()
                                                        self.orderTableView.isHidden = false
                                                            
                                                    }else{
                                                        self.orderTableView.isHidden = true
                                                        
                                                    }
                                                        
                          
                                                        }
                                                        
                                                        
                                            
                                              
                                             }else{
                                                
                                                
                                                self.showAlert(message: wentWrong)
                                                
                                               
                                                                                                                                                 
                                                                                                                                                 
                                                                                                                                                                                    }
                }
               
               }
}
extension MyOrdersViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.orderList?.count ?? 0
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let info = self.orderList![indexPath.row]
        
                                            
                                            let OrderScene = OrderDetailsViewController.instantiate(fromAppStoryboard: .Main)
        
                                            if let navigator = self.navigationController {
                                                
                                               
        
                                                OrderScene.orderId = info.order_id
                                                OrderScene.orderSysId = info.orderIdSys
                                                OrderScene.netAmount = info.net_amount
                                                OrderScene.deliveryDate = info.delivery_date
                                                OrderScene.groceryName = info.store_name
                                                OrderScene.itemCount = info.totItems
                                                OrderScene.status = info.order_status
                                                
                                                navigator.pushViewController(OrderScene, animated: true)
        
        
        
                                            }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.orderTableView.dequeueReusableCell(withIdentifier: orderCell, for: indexPath) as! OrderCell
        
        if let info = orderList?[indexPath.row]{
            
            if let title = info.store_name{
                
                cell.lblOrderTitle.text = title
                
            }
            if let orderId = info.orderIdSys{
                
                cell.lblOrderId.text = orderId
                
            }
            if let status = info.order_status{
                
                cell.lblOrderStatus.text = Utils.orderStatus(type: status)
                cell.lblOrderStatus.textColor = Utils.orderStatusColor(type: status)
                
            }
            if let amount = info.net_amount{
                
                cell.lblOrderAmount.text =  currency + amount
                
            }
            if let count = info.totItems{
                
                cell.lblOrderCount.text = count
                
                
            }
            
            if let date = info.delivery_date{
                
                cell.lblDay.text = date.getDay()
                cell.lblMonth.text = date.getMonth()
                cell.lblYear.text = date.getYear()
            }
            
            
            
            
            
        }
        
        return cell
        
    }
    
    
    
    
    
}
