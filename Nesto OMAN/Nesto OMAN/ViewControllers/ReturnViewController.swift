//
//  ReturnViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 07/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class ReturnViewController: BaseViewController {
    
    @IBOutlet weak var txtPolicyView: UITextView!
    
    @IBOutlet weak var lblGrocery: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    
    var groceryTitle:String?
    var groceryCity:String?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
        
        guard let store = defualts.string(forKey: "groceryId") else {
            
            return
        }
        
        if let title = self.groceryTitle{
            
            self.lblGrocery.text = title
        }
        
        if let city = self.groceryCity{
                 
                 self.lblCity.text = city
             }
        
        self.getPolicy(storeId: store)
        
        
    }
    
    @IBAction func mapPressed(_ sender: Any) {
        
        
        guard let long = defualts.string(forKey:"shopLong") else {
            return
        }
        
        guard let lat = defualts.string(forKey:"shopLat") else{
                return
              }
        
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(lat),\(long)&directionsmode=driving")! as URL)

            } else {
                NSLog("Can't use comgooglemaps://");
            }
    
        
        
    }
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
        
    }
    
        func getPolicy(storeId:String){
            
            
            self.STProgress.show()
            
         
            
            self.ApiRequest.returnPolicy(withParameter:["storeId":storeId]) { (isSuccess,message) in
                
                self.STProgress.dismiss()
                
                if isSuccess {
                    
                    if let info = self.ApiRequest.ReturnPolicyResponse?.arrPolicy{
                        
                        
                        if info.count > 0{
                            
                            self.txtPolicyView.text = info[0].return_policy
                            
                            
                            
                        }
                        
                      
                            
                        }
                        
                        
                }else{
                    
                    self.showAlert(message: message)
                    
                }
                    
            
            }
            
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
