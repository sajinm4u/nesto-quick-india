//
//  SignUpViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 25/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {
    
    
    @IBOutlet weak var txtName: TextfiledWithImage!
    @IBOutlet weak var txtPhone: TextfiledWithImage!
    @IBOutlet weak var txtEmail: TextfiledWithImage!
    @IBOutlet weak var txtPassword: TextfiledWithImage!
    @IBOutlet weak var txtCode: TextfiledWithImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.txtPhone.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func signUpPressed(_ sender: Any) {
        
        
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
        txtPhone.resignFirstResponder()
        txtName.resignFirstResponder()
        
        
        
        if isConnected(){
            
            
            
            guard let textEmail = self.txtEmail.text, let textPwd = self.txtPassword.text,let textName = self.txtName.text, let textPhone = self.txtPhone.text,!textEmail.isEmpty,!textPwd.isEmpty,!textName.isEmpty,!textPhone.isEmpty else {
                
                self.showNotification(message: fillAllFields)
                
                return
            }
            
            
            if textPhone.count < 10{
                
                self.showNotification(message: notValidPhone)
                return
            }
            
            if textPwd.count < 6{
                           
                self.showNotification(message: passwordMessage)
                return
                       }
            
            
            if textEmail.isValidEmail(){
                
                
                self.doSignUp(name: textName.capitalizingFirstLetter(), password: textPwd, email:textEmail, phone:textPhone )
                
                
                
            }else{
                
                self.showNotification(message: notValidEmail)
                
            }
            
            
            
        }
        
        
        
    }
    
    func doSignUp(name:String,password:String,email:String,phone:String){
        
        
        
        self.STProgress.show()
        
        
        
        let params = SignUpParameters(firstname: name, email_id: email, password: password, phone: phone).Values
        
        ApiRequest.goSignUp(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let data = self.ApiRequest.SignUpResponse{
                    
                    
                    
                    if data.verify {
                    
                    if let id = data.customer_id{
                        
                        let OtpScene = OTPVerifyViewController.instantiate(fromAppStoryboard: .Main)
                             
                             if let navigator = self.navigationController {
                             
                                 OtpScene.phoneNumber = phone
                                 OtpScene.customerId = id
                                 OtpScene.isForgotPassword = false
                                 
                                 navigator.pushViewController(OtpScene, animated: true)
                                 
                                 
                                 
                             }
                        
                        
                        //self.getOtp(phone: phoneNumber, id: id)
                        
                    }
                    
                    
                    }else{
                        
                         self.backNavigation()
                        self.showAlert(message: "User Account already exists, Please try to login")
                        
                        
                    }
                    
                    
                    
                }
            }else{
                
                
                
                
                self.showAlert(message: message)
                
            }
            
        }
    }
    
    
    func getOtp(phone:String,id:String){
        
        let otpNumber = random(digits: 6)
        
        
        if otpNumber != nil || otpNumber != ""{
            
            
            let messageStr = "Dear Customer " + otpNumber + " is your one time password(OTP). Please enter the OTP to proceed. Thank You, Team Nesto."
        
            let params = SendSms(mobile:phone).Values
            
            self.ApiRequest.sendOTP(withParameter:params) {(isSuccess,message,otp) in
                
                self.STProgress.dismiss()
                
                if isSuccess {
                    
                    let loginScene = OTPVerifyViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                       // loginScene.otpNumber = otpNumber
                        loginScene.phoneNumber = phone
                        loginScene.customerId = id
                        
                        navigator.pushViewController(loginScene, animated: true)
                        
                        
                        
                    }
               
                    
                }else{
                    
            
                    self.showAlert(message: message)
                    
                }
            }
            
            
        }
    }
    
    
    
    
    func random(digits:Int) -> String {
        var result = ""
        repeat {
            // create a string with up to 4 leading zeros with a random number 0...9999
            result = String(format:"%06d", arc4random_uniform(10000) )
            // generate another random number if the set of characters count is less than four
        } while (result.count) < 6
        return result
        
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SignUpViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // make sure the result is under 16 characters
        return updatedText.count <= 10
    }

    
    
    
    
}
