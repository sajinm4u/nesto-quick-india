//
//  ForgotPasswordViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 07/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtMobileNumber: TextfiledWithImage!
    
    @IBOutlet weak var txtCode: TextfiledWithImage!
    
    var isForgot:Bool = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        intialSetup()

    }
    
    func intialSetup(){
        
//        if isForgot{
//            guard let mobileTxt = self.txtMobileNumber.text, !mobileTxt.isEmpty  else {
//
//
//                       return
//                   }
//
//            getOtp(phone: mobileTxt)
//
//        }
        
        self.txtMobileNumber.delegate = self
        

        
    }
    
    @IBAction func resetPasswordPressed(_ sender: Any) {
        
        guard let mobileTxt = self.txtMobileNumber.text, !mobileTxt.isEmpty  else {
            
            showAlert(message:notValidPhone)
            return
        }
        

        
        
        getOtp(phone: mobileTxt)
        
        
    }
    
    
    
    
    func random(digits:Int) -> String {
        var result = ""
        repeat {
            // create a string with up to 4 leading zeros with a random number 0...9999
            result = String(format:"%06d", arc4random_uniform(10000) )
            // generate another random number if the set of characters count is less than four
        } while (result.count) < 6
        return result
        
        
        
    }
    
    @IBAction func gotoRegisterPressed(_ sender: Any) {
        
        let SignScene = SignUpViewController.instantiate(fromAppStoryboard: .Main)
                           
                           if let navigator = self.navigationController {
                               
                             
                               
                               navigator.pushViewController(SignScene, animated: true)
                               
                               
                               
                           }
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    func getOtp(phone:String){
        

        self.ApiRequest.forgotPassword(withParameter:["phoneNo":phone]) { (isSuccess,message) in
                
                self.STProgress.dismiss()
                
                if isSuccess {
                    
                    if let info = self.ApiRequest.ForgotPasswordResponse{
                        
                        guard let otpNumber = info.otp else{
                            
                            self.showAlert(message: wentWrong)
                            return
                            
                        }
                        
                        guard let customerId = info.customerId else{
                                                
                                                self.showAlert(message: wentWrong)
                                                return
                                                
                                            }
                        
                        let OtpScene = OTPVerifyViewController.instantiate(fromAppStoryboard: .Main)
                        
                        if let navigator = self.navigationController {
                            
                            
                            
                            OtpScene.otpNumber = otpNumber
                            OtpScene.phoneNumber = phone
                            OtpScene.customerId = customerId
                            OtpScene.isForgotPassword = true
                            
                            navigator.pushViewController(OtpScene, animated: true)
                            
                            
                            
                        }
                        
                    }
                    

               
                    
                }else{
                    
            
                    self.showAlert(message: message)
                    
                }
       
    }
}
}

extension ForgotPasswordViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // make sure the result is under 16 characters
        return updatedText.count <= 10
    }

    
    
    
    
}
