//
//  AboutUsViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 01/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import MessageUI

class AboutUsViewController: BaseViewController {

    @IBOutlet weak var lblVersion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
               
               guard let version = appVersion else{
                   
                
                   return
               }
        
        self.lblVersion.text = "Version " + "\(version)"

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    @IBAction func fbPressed(_ sender: Any) {
        
        openFacebook()
    }
    
    @IBAction func instaPressed(_ sender: Any) {
     
        openInstagram()
    }
    
 
    @IBAction func gmailPressed(_ sender: Any) {
        
        sendEmail()
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([customerCareEmail])
           

            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }

    
    func openFacebook() {
             guard let url = URL(string: "fb://profile/2337861096493888")  else { return }
             if UIApplication.shared.canOpenURL(url) {
                 if #available(iOS 10.0, *) {
                     UIApplication.shared.open(url, options: [:], completionHandler: nil)
                 } else {
                     UIApplication.shared.openURL(url)
                 }
             }
         }
    
    
         func openInstagram() {
             guard let url = URL(string: "https://instagram.com/nestohyperindia")  else { return }
             if UIApplication.shared.canOpenURL(url) {
                 if #available(iOS 10.0, *) {
                     UIApplication.shared.open(url, options: [:], completionHandler: nil)
                 } else {
                     UIApplication.shared.openURL(url)
                 }
             }
         }
         
    
}
extension AboutUsViewController:MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
