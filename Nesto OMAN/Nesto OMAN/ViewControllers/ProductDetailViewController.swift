//
//  ProductDetailViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 25/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import Kingfisher
import BadgeSwift

class ProductDetailViewController: BaseViewController {
    
    
    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var offerTag: UIImageView!
    
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblCategoryView: CurvedView!
    
    @IBOutlet weak var lblActualPrice: UILabel!
    @IBOutlet weak var lblOfferPrice: UILabel!
    @IBOutlet weak var lblOff: UILabel!
    @IBOutlet weak var lblSaved: UILabel!
    
    @IBOutlet weak var offView: CurvedView!
    
    @IBOutlet weak var txtCount: UITextField!
    
    @IBOutlet weak var wishListView: CurvedView!
    @IBOutlet weak var ic_wishList: UIImageView!
    @IBOutlet weak var lblWishlist: UILabel!
    @IBOutlet weak var btnWishList: UIButton!
    
    @IBOutlet weak var descView: CurvedView!
    
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var similarView: UIView!
    
    @IBOutlet weak var descriptionHeightConstant: NSLayoutConstraint!
    
    var isSearch:Bool? = false
    var productId:String = ""
    var groceryId:String = ""
    var quantity:String = "1"
    var weight:String = ""
    var price:String = ""
    var article:String = ""
    var desc:String = ""
    var isMakeFavourite:Bool = false
    
    var minCount = 1
    var selectedItemPrice = 0.0
    var customerId:String?
    var isBuyNow:Bool = false
    var bannerProductId:String?
    var isBannerProduct:Bool? = false
    
    @IBOutlet weak var lblBadge: BadgeSwift!
    
    var productData:OfferProductData?
    var searchData:CartProducts?
    var similarProducts:[CartProducts]?
    
    @IBOutlet weak var similarCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
    }
    
    func initialSetup(){
        self.similarView.isHidden = true
        self.descView.isHidden = false
        
        if let custId = defualts.string(forKey: "customerId"){
            
            self.customerId = custId
            
            
        }
        
        self.lblBadge.isHidden = true
        
        
        if let id = defualts.string(forKey: "groceryId"){
            
            self.groceryId = id
            
        }
        
        if isBannerProduct! {
            
            if let productId = self.bannerProductId{
                
                
                self.getSingleProduct(productId:productId,customerId:self.customerId ?? "0",gorceryId:self.groceryId)
                
                
            }
            

            
        }else{
           
              showProducts()
            
        }
        
      
        
    }
    
    
    
    func showProducts(){
        
        if isSearch ?? false {
            
            if self.similarProducts?.count ?? 0 > 0{
                
                self.descriptionHeightConstant.constant = 60
                
                self.similarCollectionView.delegate = self
                self.similarCollectionView.dataSource = self
                self.similarCollectionView.reloadData()
                self.similarView.isHidden = false
            }
            
            self.lblCategoryView.isHidden = true
            
            if let info = self.searchData{
                
                
                if let id = info.id{
                    
                    self.productId = "\(id)"
                }else{
                    
                    if let id = info.product_id{
                        self.productId = id
                        
                    }
                    
                    
                    
                }
                
                
                
                if let title = info.product_name{
                    
                    self.lblProduct.text = title
                    self.lblProductName.text = title
                    self.article = title
                    
                }
                
                
                
                if let off = info.offer_percentage{
                    
                    if Double(off)! > 0.0{
                        
                        
                        self.lblOff.text = off + "% OFF"
                        self.offerTag.isHidden = false
                        self.lblActualPrice.isHidden = false
                        self.offView.isHidden = false
                        
                        
                    }else{
                        
                        self.lblActualPrice.isHidden = true
                        self.offerTag.isHidden = true
                        self.offView.isHidden = true
                        
                        
                    }
                    
                    
                    
                }
                
                
                
                if let offerPrice = info.offer_price{
                    
                    if let price = info.price{
                        
                        if Double(offerPrice) ?? 0 > 0{
                            
                            if Double(price)! <= 0.0{
                                
                                self.lblActualPrice.isHidden = true
                            }else{
                                self.lblActualPrice.isHidden = false
                            }
                            
                            if Double(price)! > Double(offerPrice)!{
                                
                                let saved = Double(price)! - Double(offerPrice)!
                                self.lblSaved.text = "You have saved " + currency + "\(saved.roundToDecimal(3))" + " on this product"
                                
                                self.lblSaved.isHidden = false
                                
                            }else{
                                
                                self.lblSaved.isHidden = true
                            }
                            
                            
                            self.price = offerPrice
                            self.selectedItemPrice = Double(offerPrice)?.roundToDecimal(3) ?? 0
                            self.lblOfferPrice.text = nrp + currency + ("\(self.selectedItemPrice)")
                            // self.lblTotalAmount.text = currency + offerPrice
                            let priceStr = mrp + currency + price
                        
                            self.lblActualPrice.attributedText = priceStr.strikeThrough()
                            
                        }else{
                            
                            self.price = price
                            self.selectedItemPrice = Double(price) ?? 0
                            let priceStr = nrp + currency + price
                            
                            // self.lblTotalAmount.text = priceStr
                            self.lblOfferPrice.text = priceStr
                            self.lblActualPrice.isHidden = true
                            
                            
                            
                        }
                        
                    }
                    
                }else{
                    
                    if let price = info.price{
                        
                        
                        
                        self.offView.isHidden = true
                        self.lblOfferPrice.text = currency + price
                        self.lblActualPrice.isHidden = true
                        self.offerTag.isHidden = true
                    }
                    
                    
                    
                    
                    
                }
                
                if let weight = info.weight{
                    
                    self.weight = weight
                    
                }
                
                
                if let favorite = info.favourite{
                                     
                                     if favorite == "1"{
                                         setWishListView(status: 1)
                                         
                                     }else{
                                         
                                         setWishListView(status: 0)
                                     }
                                     
                                 }
                
          
                
             
                if let desc = info.descriptions{
                                   
                                   self.descView.isHidden = false
                                   self.descTxtView
                                    
                                    .text = desc
                                   
                               }
                
                if let imgUrl = info.product_image1{
                    
                    let url = URL(string: imgUrl)
                    let phImage = UIImage(named: ph)
                    
                    self.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }else{
                    
                    if let imgUrl = info.product_image1{
                        
                        let url = URL(string: imgUrl)
                        let phImage = UIImage(named: ph)
                        
                        self.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                        
                    }
                    
                }
                
                
                
            }
                
            }else{
                
         
          
              self.descriptionHeightConstant.constant = 143
                
                if let info = self.productData{
                    
                    
                    if let desc = info.descriptions{

                        self.descView.isHidden = false
                        self.descTxtView.text = desc

                    }
                    

                    
            
                    if let favorite = info.favourite{
                        
                        if favorite == "1"{
                            setWishListView(status: 1)
                            
                        }else{
                            
                            setWishListView(status: 0)
                        }
                        
                    }
                    
                    if let id = info.id{
                        
                        self.productId = "\(id)"
                    }
                    
                    if let title = info.productName{
                        
                        self.lblProduct.text = title
                        self.lblProductName.text = title
                        self.article = title
                        
                    }else{
                        
                        if let title = info.product{
                            
                            self.lblProductName.text = title
                            self.article = title
                        }
                        
                        
                    }
                    
                    if let category = info.category{
                        
                        self.lblCategory.text = category
                        self.lblCategoryView.isHidden = false
                        
                    }else{
                        
                        self.lblCategoryView.isHidden = true
                    }
                    
                    
                    if let off = info.offerPercentage{
                        
                        if Double(off)! > 0.0{
                            
                            
                            self.lblOff.text = off + "% OFF"
                            self.offerTag.isHidden = false
                            self.lblActualPrice.isHidden = false
                            self.offView.isHidden = false
                            
                        }else{
                            
                            self.lblActualPrice.isHidden = true
                            self.offerTag.isHidden = true
                            self.offView.isHidden = true
                            
                            
                        }
                        
                        
                        
                    }else if let off = info.offer_percentage{
                        
                        if Double(off)! > 0.0{
                            
                            
                            self.lblOff.text = off + "% OFF"
                            self.offerTag.isHidden = false
                            self.lblActualPrice.isHidden = false
                            self.offView.isHidden = false
                            
                        }else{
                            
                            self.lblActualPrice.isHidden = true
                            self.offerTag.isHidden = true
                            self.offView.isHidden = true
                            
                            
                        }
                        
                        
                        
                    }
                    
                    
                    if let offerPrice = info.offerPrice{
                        
                        if let price = info.groceryproductprice{
                            
                            if Double(offerPrice) ?? 0 > 0{
                                
                                if Double(price)! <= 0.0{
                                    
                                    self.lblActualPrice.isHidden = true
                                }else{
                                    self.lblActualPrice.isHidden = false
                                }
                                
                                if Double(price)! > Double(offerPrice)!{
                                    
                                    let saved = Double(price)! - Double(offerPrice)!
                                    self.lblSaved.text = "You have saved " + currency + "\(saved.roundToDecimal(3))" + " on this product"
                                    
                                    self.lblSaved.isHidden = false
                                    
                                }else{
                                    
                                    self.lblSaved.isHidden = true
                                }
                                
                                
                                self.price = offerPrice
                                self.selectedItemPrice = Double(offerPrice)?.roundToDecimal(3) ?? 0
                                self.lblOfferPrice.text = nrp + currency + ("\(self.selectedItemPrice)")
                                // self.lblTotalAmount.text = currency + offerPrice
                                let priceStr = mrp + currency + price
                                self.lblActualPrice.attributedText = priceStr.strikeThrough()
                                
                            }else{
                                
                                self.price = price
                                self.selectedItemPrice = Double(price) ?? 0
                                let priceStr = nrp + currency + price
                                
                                // self.lblTotalAmount.text = priceStr
                                self.lblOfferPrice.text = priceStr
                                self.lblActualPrice.isHidden = true
                                
                                
                                
                            }
                            
                        }
                        
                    }else if let offerPrice = info.offer_price {
                        
                        if let price = info.price{
                            
                            if Double(offerPrice) ?? 0 > 0{
                                
                                if Double(price)! <= 0.0{
                                    
                                    self.lblActualPrice.isHidden = true
                                }else{
                                    self.lblActualPrice.isHidden = false
                                }
                                
                                if Double(price)! > Double(offerPrice)!{
                                    
                                    let saved = Double(price)! - Double(offerPrice)!
                                    self.lblSaved.text = "You have saved " + currency + "\(saved.roundToDecimal(3))" + " on this product"
                                    
                                    self.lblSaved.isHidden = false
                                    
                                }else{
                                    
                                    self.lblSaved.isHidden = true
                                }
                                
                                
                                self.price = offerPrice
                                self.selectedItemPrice = Double(offerPrice)?.roundToDecimal(3) ?? 0
                                self.lblOfferPrice.text = nrp + currency + ("\(self.selectedItemPrice)")
                                // self.lblTotalAmount.text = currency + offerPrice
                                let priceStr = mrp + currency + price
                                self.lblActualPrice.attributedText = priceStr.strikeThrough()
                                
                            }else{
                                
                                self.price = price
                                self.selectedItemPrice = Double(price) ?? 0
                                let priceStr = nrp + currency + price
                                
                                // self.lblTotalAmount.text = priceStr
                                self.lblOfferPrice.text = priceStr
                                self.lblActualPrice.isHidden = true
                                
                                
                                
                            }
                            
                      
                    }
                }else{
                        
                        if let price = info.price{
                            
                            self.offView.isHidden = true
                            self.lblOfferPrice.text = nrp + currency + price
                            self.lblActualPrice.isHidden = true
                            self.offerTag.isHidden = true
                            self.price = price
                        }
                        
                        
                        
                        
                        
                    }
                    
                    if let weight = info.weight{
                        
                        self.weight = weight
                        
                    }
                    
                    
                    
                    
                    if let imgUrl = info.product_image1{
                        
                        let url = URL(string: imgUrl)
                        let phImage = UIImage(named: ph)
                        
                        self.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                        
                        
                    }else{
                        
                        if let imgUrl = info.image1{
                            
                            let url = URL(string: imgUrl)
                            let phImage = UIImage(named: ph)
                            
                            self.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                            
                        }
                        
                    }
                    
                    
                }
       
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let custId = defualts.string(forKey: "customerId"){
        
        if let count = defualts.string(forKey: "badge"){
            
            if count != "0"{
                
                self.lblBadge.isHidden = false
                           self.lblBadge.text = count
                
            }else{
                 self.lblBadge.isHidden = true
                
            }
            
           
         }
        }
        
    }
    
    // MARK: Wish List
    
    @IBAction func wishListPressed(_ sender: UIButton) {
        
        if let customerId = defualts.string(forKey: "customerId"){
            
            
            switch sender.tag {
            case 0:
                
                self.addtoWishList(productId: self.productId)
               
             case 1:
                
                self.removeWishList(productId: self.productId)
                
            default:
                break
            }
 
            
        }else{
            
           
                self.STAlert.alert(title:"", message: pleaseLogin )
                    .action(.destructive("Login")){
                        
                        let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                        
                        if let navigator = self.navigationController {
                            
                            navigator.pushViewController(loginScene, animated: true)
                            
                            
                            
                        }
                        
                        
                }.action(.cancel("Cancel"))
                    .show(on: self)
                
            
            
        }
        
        // if   sender.tag == 0
        
        
        
    }
    
    
    func setWishListView(status:Int)  {
    
        switch status {
        case 1:
            
            self.wishListView.backgroundColor = Colors.NOGreen
            self.ic_wishList.image = UIImage(named: "ic_wish_selected")
            self.lblWishlist.textColor = UIColor.white
            self.btnWishList.tag = 1
            
        case 0:
            
            self.wishListView.backgroundColor = UIColor.white
            self.ic_wishList.image = UIImage(named: "ic_wish")
            self.lblWishlist.textColor = Colors.NOGreen
            self.btnWishList.tag = 0
            
         default:
            break
            
        }
        
        
    }
    
    func getSingleProduct(productId:String,customerId:String,gorceryId:String){
        
        self.STProgress.show()
            
               
        let params = FavParams(customerId: customerId, groceryId: groceryId, productId: productId).Values
               
               
               ApiRequest.getSingleProduct(withParameter: params) { (isSuccess,message) in
                   
                   self.STProgress.dismiss()
                   
                   if isSuccess {
                    
                    
                    if let data = self.ApiRequest.SingleResponse?.products?[0]{
                                        
                        
                         let info = data
                          
                          
                          if let id = info.id{
                              
                              self.productId = "\(id)"
                          }else{
                              
                              if let id = info.product_id{
                                  self.productId = id
                                  
                              }
                              
                              
                              
                          }
                          
                          
                          
                          if let title = info.product_name{
                              
                              self.lblProduct.text = title
                              self.lblProductName.text = title
                              self.article = title
                              
                          }
                          
                          
                          
                          if let off = info.offer_percentage{
                              
                              if Double(off)! > 0.0{
                                  
                                  
                                  self.lblOff.text = off + "% OFF"
                                  self.offerTag.isHidden = false
                                  self.lblActualPrice.isHidden = false
                                  self.offView.isHidden = false
                                  
                              }else{
                                  
                                  self.lblActualPrice.isHidden = true
                                  self.offerTag.isHidden = true
                                  self.offView.isHidden = true
                                  
                                  
                              }
                              
                              
                              
                          }
                          
                          
                          
                          if let offerPrice = info.offer_price{
                              
                              if let price = info.price{
                                  
                                  if Double(offerPrice) ?? 0 > 0{
                                      
                                      if Double(price)! <= 0.0{
                                          
                                          self.lblActualPrice.isHidden = true
                                      }else{
                                          self.lblActualPrice.isHidden = false
                                      }
                                      
                                      if Double(price)! > Double(offerPrice)!{
                                          
                                          let saved = Double(price)! - Double(offerPrice)!
                                          self.lblSaved.text = "You have saved " + currency + "\(saved.roundToDecimal(3))" + " on this product"
                                          
                                          self.lblSaved.isHidden = false
                                          
                                      }else{
                                          
                                          self.lblSaved.isHidden = true
                                      }
                                      
                                      
                                      self.price = offerPrice
                                      self.selectedItemPrice = Double(offerPrice)?.roundToDecimal(3) ?? 0
                                      self.lblOfferPrice.text = nrp + currency + ("\(self.selectedItemPrice)")
                                      // self.lblTotalAmount.text = currency + offerPrice
                                      let priceStr = mrp + currency + price
                                      self.lblActualPrice.attributedText = priceStr.strikeThrough()
                                      
                                  }else{
                                      
                                      self.price = price
                                      self.selectedItemPrice = Double(price) ?? 0
                                      let priceStr = nrp + currency + price
                                      
                                      // self.lblTotalAmount.text = priceStr
                                      self.lblOfferPrice.text = priceStr
                                      self.lblActualPrice.isHidden = true
                                      
                                      
                                      
                                  }
                                  
                              }
                              
                          }else{
                              
                              if let price = info.price{
                                  
                                  
                                  
                                  self.offView.isHidden = true
                                  self.lblOfferPrice.text = currency + price
                                  self.lblActualPrice.isHidden = true
                                  self.offerTag.isHidden = true
                              }
                              
                              
                              
                              
                              
                          }
                          
                          if let weight = info.weight{
                              
                              self.weight = weight
                              
                          }
                          
                          
                          if let favorite = info.favourite{
                                               
                                               if favorite == "1"{
                                                self.setWishListView(status: 1)
                                                   
                                               }else{
                                                   
                                                self.setWishListView(status: 0)
                                               }
                                               
                                           }
                          
                    
                          
                       
                          if let desc = info.descriptions{
                                             
                                             self.descView.isHidden = false
                                             self.descTxtView
                                              
                                              .text = desc
                                             
                                         }
                          
                          if let imgUrl = info.product_image1{
                              
                              let url = URL(string: imgUrl)
                              let phImage = UIImage(named: ph)
                              
                              self.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                              
                              
                          }else{
                              
                              if let imgUrl = info.image1{
                                  
                                  let url = URL(string: imgUrl)
                                  let phImage = UIImage(named: ph)
                                  
                                  self.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                                  
                              }
                              
                          }
                          
                        }
                          
                   }else{
                    
                    
                     self.backNavigation()
                    DispatchQueue.main.async {
                        self.showAlert(message: wentWrong)
                        
                       
                    }
                    
                    
                }
                       
                   
                   
               }
               

        
    }
    
    
    
    func removeWishList(productId:String){
        
        self.STProgress.show()
            
               
        let params = FavParams(customerId: self.customerId!, groceryId: self.groceryId, productId: productId).Values
               
               
               ApiRequest.RemoveWishList(withParameter: params) { (isSuccess,message) in
                   
                   self.STProgress.dismiss()
                   
                   if isSuccess {
                       
                    
                    self.setWishListView(status: 0)
                     self.isMakeFavourite = true
                       
                       
                       
                   }
                   
               }
               

        
    }
    
    func addtoWishList(productId:String){
        
        
        
        guard let customerId = defualts.string(forKey: "customerId") else {
            
            
            showAlert(message: pleaseLogin)
            
            return
            
        }
        
        
        self.STProgress.show()
            
               
        let params = FavParams(customerId: customerId, groceryId: self.groceryId, productId: productId).Values
               
               
               ApiRequest.AddtoWishList(withParameter: params) { (isSuccess,message) in
                   
                   self.STProgress.dismiss()
                   
                   if isSuccess {
                       
                        self.isMakeFavourite = true
                        self.setWishListView(status: 1)
                       
                    
                       
                       
                   }
                   
               }
               
               
        
        
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {

        if self.isMakeFavourite{
            
             NotificationCenter.default.post(name: Notification.Name(DashBoardReload), object: nil)
        }
        
        
    }
    
    // MARK: Add to cart
    
    
    @IBAction func addToCartPressed(_ sender: Any) {
        
        self.isBuyNow = false
        

            
            if let id = defualts.string(forKey: "customerId"){
                
                self.customerId = id
                
                
              
                
                if self.productId != "" && self.weight != "" && self.price != "" && self.article != "" {
                    
                    
                  
                               
                               addToCart(productId: self.productId, groceryId: self.groceryId, quantity: self.quantity, weight:self.weight, price: self.price, article: self.article, customerId: self.customerId!, description: self.desc)
                               
                               
                           }else{
                               
                               showAlert(message: wentWrong)
                           }
                           
                
            }else{
                
                
                self.STAlert.alert(title:"", message: pleaseLogin )
                            .action(.destructive("Login")){
                                
                                let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                                
                                if let navigator = self.navigationController {
                                    
                                    navigator.pushViewController(loginScene, animated: true)
                                    
                                    
                                    
                                }
                                
                                
                        }.action(.cancel("Cancel"))
                            .show(on: self)
                        
                    }
                
            
        }
        
    
    func addToCart(productId:String,groceryId:String,quantity:String,weight:String,price:String,article:String,customerId:String,description:String){
        
        self.STProgress.show()
        
      
        
        let params = AddtoCartParam(productId: productId, groceryId: groceryId, quantity: quantity, weight: weight, price: price, article: article, customerId: customerId, description: description).Values
        
        
        ApiRequest.addTocart(withParameter: params) { (isSuccess,cartCount,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                DispatchQueue.main.async {
                    
                    
                    self.lblBadge.text = cartCount
                    self.lblBadge.isHidden = false
                    defualts.set(cartCount, forKey: "badge")
                    
                    self.showNotificationAddProduct(message: productAdded)
                    
                    if self.isBuyNow{
                        
                        
                        let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
                        
                        if let navigator = self.navigationController {
                            
                            navigator.pushViewController(CartScene, animated: true)
                        }
                        
                        
                    }
                    
                    
                }
                
                
                
                
            }else{
                
                self.showAlert(message: message)
                
            }
            
        }
        
        
        
        
    }
    
    
    
    @IBAction func cartPressed(_ sender: Any) {
        
        
        if let customerId = defualts.string(forKey: "customerId"){
            
            
            let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
                 
                 if let navigator = self.navigationController {
                     
                     navigator.pushViewController(CartScene, animated: true)
                 }
                 
            
            
            
        }else{
            
          
            
            
            self.STAlert.alert(title:"", message: pleaseLogin )
                        .action(.destructive("Login")){
                            
                            let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                            
                            if let navigator = self.navigationController {
                                
                                navigator.pushViewController(loginScene, animated: true)
                                
                                
                                
                            }
                            
                            
                    }.action(.cancel("Cancel"))
                        .show(on: self)
                    
            
            
        }
        
        
        
        
        
     
        
    }
    
    
    
    @IBAction func buyNowPressed(_ sender: Any) {
        
        
        self.isBuyNow = true
        
        
        if let customerId = defualts.string(forKey: "customerId"){
            
            if self.productId != "" && self.weight != "" && self.price != "" && self.article != "" {
           
                
                addToCart(productId: self.productId, groceryId: self.groceryId, quantity: self.quantity, weight:self.weight, price: self.price, article: self.article, customerId: customerId, description: self.desc)
                
                
            }else{
                
                showAlert(message: wentWrong)
            }
            
            
        }else{
            
            self.STAlert.alert(title:AppName , message: pleaseLogin )
                .action(.destructive("Login")){
                    
                    let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                        navigator.pushViewController(loginScene, animated: true)
                        
                        
                        
                    }
                    
                    
            }.action(.cancel("Cancel"))
                .show(on: self)
            
        }
        
        
        
        
        
    }
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    
    @IBAction func increaseCount(_ sender: Any) {
        
        self.minCount = minCount + 1
        
        let totalPrice = (selectedItemPrice.roundToDecimal(3) * Double(self.minCount)).roundToDecimal(3)
        self.txtCount.text = "\(self.minCount)"
        // self.lblCount.text = "x " + "\(self.minCount)"
        // self.lblTotalAmount.text =  currency + "\(totalPrice)"
        self.quantity = "\(self.minCount)"
        self.price = "\(totalPrice)"
        
        
    }
    
    @IBAction func decreaseCount(_ sender: Any) {
        
        if self.minCount > 1{
            
            self.minCount  =  self.minCount - 1
            let totalPrice = (selectedItemPrice.roundToDecimal(3) * Double(self.minCount)).roundToDecimal(3)
            self.txtCount.text = "\(self.minCount)"
            // self.lblCount.text = "x " + "\(self.minCount)"
            // self.lblTotalAmount.text =  currency + "\(totalPrice)"
            self.price = "\(totalPrice)"
            self.quantity = "\(self.minCount)"
            
        }
        
        
        
    }
    
    
    
}

extension ProductDetailViewController:UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.similarProducts?.count ?? 0
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
               
               
               
               if let info = self.similarProducts?[indexPath.row]{
                   
                if let title = info.product_name{
                       
                       cell.lblProductName.text = title
                       
                   }
                   
                if let off = info.offer_percentage{
                       
                       if Double(off)! > 0.0{
                           
                           cell.lblOff.text = off + "% OFF"
                           cell.offView.isHidden = false
                           
                        if let price = info.price{
                               
                               let priceStr = mrp + currency + price
                               
                               cell.lblPrice.attributedText = priceStr.strikeThrough()
                               
                           }
                           
                        if let offerPrice = info.offer_price{
                               
                               cell.lblOfferPrice.text = nrp + currency + offerPrice
                               
                           }
                           
                    
                       }else{
                        
                         if let price = info.price{
                            
                             let priceStr = currency + price
                              cell.lblOfferPrice.text = priceStr
                              cell.lblPrice.isHidden = true
                        }
                        
                        
                        cell.offView.isHidden = true
                        
                    }
                   
            
                   if let weight = info.weight {
                       
                       if let weightQty = info.qty_weight{
                           
                           let weightStr = weightQty + " " + weight
                           
                           cell.lblWeight.text = weightStr
                           
                           
                       }
                       
                   }
                   
                   if let imgUrl = info.product_image1{
                       
                       let url = URL(string: imgUrl)
                       let phImage = UIImage(named: ph)
                       
                       cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                       
                       
                   }
                   
                   
               }
               
 
       
    }
    

      return cell
    
    
}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
          self.lblCategoryView.isHidden = true
          
        if let info = self.similarProducts?[indexPath.row]{
              
              
              if let id = info.id{
                  
                  self.productId = "\(id)"
              }else{
                  
                  if let id = info.product_id{
                      self.productId = id
                      
                  }
                  
                  
                  
              }
              
              
              
              if let title = info.product_name{
                  
                  self.lblProduct.text = title
                  self.lblProductName.text = title
                  self.article = title
                  
              }
              
              
              
              if let off = info.offer_percentage{
                  
                  if Double(off)! > 0.0{
                      
                      
                      self.lblOff.text = off + "% OFF"
                      self.offerTag.isHidden = false
                      self.lblActualPrice.isHidden = false
                      self.offView.isHidden = false
                      
                  }else{
                      
                      self.lblActualPrice.isHidden = true
                      self.offerTag.isHidden = true
                      self.offView.isHidden = true
                      
                      
                  }
                  
                  
                  
              }
              
              
              
              if let offerPrice = info.offer_price{
                  
                  if let price = info.price{
                      
                      if Double(offerPrice) ?? 0 > 0{
                          
                          if Double(price)! <= 0.0{
                              
                              self.lblActualPrice.isHidden = true
                          }else{
                              self.lblActualPrice.isHidden = false
                          }
                          
                          if Double(price)! > Double(offerPrice)!{
                              
                              let saved = Double(price)! - Double(offerPrice)!
                              self.lblSaved.text = "You have saved " + currency + "\(saved.roundToDecimal(3))" + " on this product"
                              
                              self.lblSaved.isHidden = false
                              
                          }else{
                              
                              self.lblSaved.isHidden = true
                          }
                          
                          
                          self.price = offerPrice
                          self.selectedItemPrice = Double(offerPrice)?.roundToDecimal(3) ?? 0
                          self.lblOfferPrice.text = nrp + currency + ("\(self.selectedItemPrice)")
                          // self.lblTotalAmount.text = currency + offerPrice
                          let priceStr = mrp + currency + price
                          self.lblActualPrice.attributedText = priceStr.strikeThrough()
                          
                      }else{
                          
                          self.price = price
                          self.selectedItemPrice = Double(price) ?? 0
                          let priceStr =  currency + price
                          
                          // self.lblTotalAmount.text = priceStr
                          self.lblOfferPrice.text = priceStr
                          self.lblActualPrice.isHidden = true
                          
                          
                          
                      }
                      
                  }
                  
              }else{
                  
                  if let price = info.price{
                      
                      
                      
                      self.offView.isHidden = true
                      self.lblOfferPrice.text = currency + price
                      self.lblActualPrice.isHidden = true
                      self.offerTag.isHidden = true
                  }
                  
                  
                  
                  
                  
              }
              
              if let weight = info.weight{
                  
                  self.weight = weight
                  
              }
              
              
              if let favorite = info.favourite{
                                   
                                   if favorite == "1"{
                                       setWishListView(status: 1)
                                       
                                   }else{
                                       
                                       setWishListView(status: 0)
                                   }
                                   
                               }
              
        
              
           
              if let desc = info.descriptions{
                                 
                                 self.descView.isHidden = false
                                 self.descTxtView
                                  
                                  .text = desc
                                 
                             }
              
              if let imgUrl = info.product_image1{
                  
                  let url = URL(string: imgUrl)
                  let phImage = UIImage(named: ph)
                  
                  self.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                  
                  
              }else{
                  
                  if let imgUrl = info.product_image1{
                      
                      let url = URL(string: imgUrl)
                      let phImage = UIImage(named: ph)
                      
                      self.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                      
                  }
                  
              }
              
              
              
          }
        
        
    }
    
    
}
