//
//  ProductListingViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 02/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import BadgeSwift

class ProductListingViewController: BaseViewController {
    
    
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var lblBadge: BadgeSwift!
    
    @IBOutlet weak var productCollectionView: UICollectionView!
    var offerProducts:OfferProducts?
    var subCategories = [[OfferProductData]]()
    

    var categoryTitle:String?
    var bannerUrl:String?
    
    var customerId:String? = "0"
    var index:Int? = 0
    var sortType = ""
    var page = 1

     
     @IBOutlet weak var sortTableView: UITableView!
     
     var sortArray : [Sort] = [Sort(title:"A -- Z",selected:false),Sort(title:"Price -- Low to High",selected:false),Sort(title:"Price -- High to Low",selected:false)]
     
       var sortKey = ["","ASC","DESC"]
       
     private var selectedSort: Int? {
            didSet {
                sortTableView.reloadData()
            }
        }
    
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var btnSortCancel: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        
       initialSetup()
        
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
        self.transparentView.isHidden = true
        self.btnSortCancel.isHidden = true
        
        if let custId = defualts.string(forKey: "customerId"){
            
             self.customerId = custId
        }
        
        
       
        if let title = self.categoryTitle{
            
            self.lblTitle.text = title
       
        }
        
        if let imgUrl = self.bannerUrl {
            
        let url = URL(string: imgUrl)
                            let phImage = UIImage(named: ph)
                           
        bannerImg?.kf.setImage(with: url, placeholder: phImage)
    
                       }
        
        if index == 0{
            
              getOfferProducts()
        }else{
            
            getHomeSubCategory()
            
        }
        
       
        self.sortTableView.delegate = self
        self.sortTableView.dataSource = self
   
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
     
         if let custId = defualts.string(forKey: "customerId"){
         
         if let count = defualts.string(forKey: "badge"){
             
             if count != "0"{
                 
                 self.lblBadge.isHidden = false
                 self.lblBadge.text = count
                 
             }else{
                  self.lblBadge.isHidden = true
                 
             }
             
            
          }
         }else{
            
             self.lblBadge.isHidden = true
            
        }
         
     }
    
    
    @IBAction func sortPressed(_ sender: Any) {
        
       
           self.transparentView.isHidden = false
           self.btnSortCancel.isHidden = false
              
              let screenSize = UIScreen.main.bounds.size
        self.sortView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)

        self.transparentView.alpha = 0
              
        self.transparentView.backgroundColor = UIColor.clear
                            
              UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                
                self.transparentView.alpha = 1
                
                self.transparentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6698416096)
                   self.sortView.frame = CGRect(x: 0, y: screenSize.height - height, width: screenSize.width, height: height)
              }, completion: nil)
        
        
        
        
        
    }
    
    
    
    
    private func updateSelectedIndex(_ index: Int) {
        selectedSort = index
    }

    
    @IBAction func cancelSortPressed(_ sender: Any) {
        
        onClickTransparentView()
        
    }
    
    
    
    
         func onClickTransparentView() {
            
    
            let screenSize = UIScreen.main.bounds.size
           
            

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                self.transparentView.alpha = 0
                //self.transparentView.backgroundColor = UIColor.clear
                 self.sortView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)
                 
            }, completion: nil)
           // self.transparentView.isHidden = true
        }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    func getHomeSubCategory(){
        
        self.STProgress.show()
        
        guard let groceryId = defualts.string(forKey: "groceryId") else {
            return
        }
        
        let params = CategoryWithCutomerId(groceryId: groceryId,customerId: self.customerId!, sortKey:self.sortType).Values
        
        
        ApiRequest.getHomeSubCategory(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let data = self.ApiRequest.HomeSubCategoryResponse {
                    
                    
                    
                    if let firstCategory = data.eachCategory?.first?.category{
                        
                        let foundItems = data.eachCategory?.filter{ $0.category == firstCategory }
                        
                        if foundItems?.count ?? 0 > 0{
                            
                            self.subCategories.append(foundItems!)
                        }
                        
                    }
                    if let lastCategory = data.eachCategory?.last?.category{
                        
                        let foundItems = data.eachCategory?.filter{ $0.category == lastCategory }
                        
                        if foundItems?.count ?? 0 > 0{
                            
                            self.subCategories.append(foundItems!)
                        }
                        
                    }
                    
                    
                    if self.subCategories.count > 0 {
                        
                         self.productCollectionView.delegate = self
                          self.productCollectionView.dataSource = self
                          self.productCollectionView.reloadData()
                        
                    }
                    
                    
                    
                }
                
                
                
                
            }
            
        }
        
        
        
        
    }
    
    
   @IBAction func cartPressed(_ sender: Any) {
        
        
        if defualts.value(forKey: "customerId") != nil{
            
            
            
            if let id = defualts.string(forKey: "customerId"){
                
                self.customerId = id
                
                let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
                           
                           if let navigator = self.navigationController {
                               
                               navigator.pushViewController(CartScene, animated: true)
                           }
                           
                           
                
            }
            
        }else{
            
            
            self.STAlert.alert(title:AppName , message: pleaseLogin )
                .action(.destructive("Login")){
                    
                    let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                        navigator.pushViewController(loginScene, animated: true)
                        
                        
                        
                    }
                    
                    
            }.action(.cancel("Cancel"))
                .show(on: self)
            
            
        }
        
        
        
        
        
        
    }
    
    
    
    
    func getOfferProducts(){
        
        self.STProgress.show()
                
                guard let groceryId = defualts.string(forKey: "groceryId") else {
                         return
                     }
                       
        let params = OfferProductParameters(groceryId:groceryId, customerId: self.customerId!,sortKey: self.sortType).Values
                              
                              ApiRequest.getOfferProducts(withParameter: params) { (isSuccess,message) in
                               
                                self.STProgress.dismiss()
                                              
                                              if isSuccess {
                                                  
                                                  if let data = self.ApiRequest.OfferProductsResponse {
                                                      
                                                       self.offerProducts = data
                                                    
                                                    if data.eachCategory?.count ?? 0 > 0{
                                                        
                                                        self.productCollectionView.delegate = self
                                                        self.productCollectionView.dataSource = self
                                                        self.productCollectionView.reloadData()
                                                        
                                                        
                                                    }
                                                      
                                                  }
                                              
                                         
                                             
                                               
                                              }
                
                }
                
                
             
             
         }


}

extension ProductListingViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        
        if index == 1{
            
            if self.subCategories[0].count > 0{
                
                  return self.subCategories[0].count
            }

        }
        
        
        if index == 2{
                   
                   if self.subCategories[1].count > 0{
                       
                         return self.subCategories[1].count
                   }

               }
        
        return self.offerProducts?.eachCategory?.count ?? 0
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  
    if self.index == 0{
        
        if let info = self.offerProducts?.eachCategory?[indexPath.row]{
            
            let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                ProductDetailsScene.productData = info
                
                navigator.pushViewController(ProductDetailsScene, animated: true)
                
                
                
            }
        }
        
        
    }
    if self.index == 1{
        
        if self.subCategories[0][indexPath.row] !=  nil{
            
            let info = self.subCategories[0][indexPath.row]
            
            
            
            
            let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                ProductDetailsScene.productData = info
                
                navigator.pushViewController(ProductDetailsScene, animated: true)
                
            }
            
        }
        
    }
    
    
    if self.index == 2{
        
        if self.subCategories[1][indexPath.row] !=  nil{
            
            let info = self.subCategories[1][indexPath.row]
            
            
            
            
            let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                ProductDetailsScene.productData = info
                
                navigator.pushViewController(ProductDetailsScene, animated: true)
                
            }
            
        }
        
    }
        
        
    }
      
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        
        
        if index == 1{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
        
            
            let info = self.subCategories[0][indexPath.row]
            
            
            
            if let title = info.productName{
                
                cell.lblProductName.text = title
                
            }
            
            if info.is_special_offer_product ?? false {
                                       
                                       cell.offerTagView.isHidden = false
                                   }else{
                                        cell.offerTagView.isHidden = true
                                   }
            
            
            
            if let off = info.offerPercentage{
                
                if Double(off)! > 0.0{
                    
                    cell.lblOff.text = off + "% OFF"
                    cell.offView.isHidden = false
                    cell.lblPrice.isHidden = false
                    
                    if let price = info.groceryproductprice{
                        
                        let priceStr = mrp + currency + price
                        
                        cell.lblPrice.attributedText = priceStr.strikeThrough()
                        
                    }
                    
                    if let offerPrice = info.offerPrice{
                        
                        cell.lblOfferPrice.text = nrp + currency + offerPrice
                        
                    }
                    
                }else{
                    
                    
                    if let price = info.groceryproductprice{
                        
                        cell.lblOfferPrice.text = nrp + currency + price
                    }
                    
                    cell.lblPrice.isHidden = true
                    cell.offView.isHidden = true
                    
                    
                    
                }
                
                
            }
            
            
            
            if let weight = info.weight {
                
                if let weightQty = info.qty_weight{
                    
                    let weightStr = weightQty + " " + weight
                    
                    cell.lblWeight.text = weightStr
                    
                    
                }
                
            }
            
            if let imgUrl = info.product_image1{
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
    
            
            
            return cell
            
            
            
        }
        
        
        
        
        if index == 2{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
            
            
            
            
            let info = self.subCategories[1][indexPath.row]
            
            
            
            if let title = info.productName{
                
                cell.lblProductName.text = title
                
            }
            
            if info.is_special_offer_product ?? false {
                                       
                                       cell.offerTagView.isHidden = false
                                   }else{
                                        cell.offerTagView.isHidden = true
                                   }
            
            
            if let off = info.offerPercentage{
                
                if Double(off)! > 0.0{
                    
                    cell.lblOff.text = off + "% OFF"
                    cell.offView.isHidden = false
                    cell.lblPrice.isHidden = false
                    
                    if let price = info.groceryproductprice{
                        
                        let priceStr = mrp + currency + price
                        
                        cell.lblPrice.attributedText = priceStr.strikeThrough()
                        
                    }
                    
                    if let offerPrice = info.offerPrice{
                        
                        cell.lblOfferPrice.text = nrp + currency + offerPrice
                        
                    }
                    
                }else{
                    
                    
                    if let price = info.groceryproductprice{
                        
                        cell.lblOfferPrice.text = nrp + currency + price
                    }
                    
                    cell.lblPrice.isHidden = true
                    cell.offView.isHidden = true
                    
                    
                    
                }
                
                
            }
            
            
            
            if let weight = info.weight {
                
                if let weightQty = info.qty_weight{
                    
                    let weightStr = weightQty + " " + weight
                    
                    cell.lblWeight.text = weightStr
                    
                    
                }
                
            }
            
            if let imgUrl = info.product_image1{
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            
            
            
            
            
            return cell
            
            
            
        }
        
        
       
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
            
        
            
            if let info = self.offerProducts?.eachCategory?[indexPath.row]{
                
                if let title = info.productName{
                    
                    cell.lblProductName.text = title
                    
                }
                
                
                
                if info.is_special_offer_product ?? false {
                                           
                                           cell.offerTagView.isHidden = false
                                       }else{
                                            cell.offerTagView.isHidden = true
                                       }
                if let off = info.offerPercentage{
                    
                    if Double(off)! > 0.0{
                        
                        cell.lblOff.text = off + "% OFF"
                        cell.offView.isHidden = false
                        
                        if let price = info.groceryproductprice{
                                        
                                        let priceStr = mrp + currency + price
                                        
                                         cell.lblPrice.attributedText = priceStr.strikeThrough()
                                        
                                    }
                        
                        if let offerPrice = info.offerPrice{
                                           
                                           cell.lblOfferPrice.text = nrp + currency + offerPrice
                                           
                                       }
                        
                    }else{
                        
                        if let price = info.price{
                            
                           cell.lblOfferPrice.text = nrp + currency + price
                            cell.offView.isHidden = true
                            cell.lblPrice.isHidden = true
                        }
                        
                        
                        
                     
                        
                    }
                    
                    
                }
                
            
                
               
                
                if let weight = info.weight {
                    
                    if let weightQty = info.qty_weight{
                        
                        let weightStr = weightQty + " " + weight
                        
                        cell.lblWeight.text = weightStr
                        
                        
                    }
                
                }
                
                if let imgUrl = info.product_image1{
                    
                    let url = URL(string: imgUrl)
                     let phImage = UIImage(named: ph)
                    
                     cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }
                
                
            }
             
           
             
             
                return cell
            
  
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
              
               
               let width = (collectionView.frame.size.width - 5 * 2) / 2
        let height = width * 1.35 //ratio
               return CGSize(width: width, height: height)
               
           
    }
    
    
}

extension ProductListingViewController: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sortArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = self.sortTableView.dequeueReusableCell(withIdentifier: sortCell, for: indexPath) as! SortTableViewCell

            let sort = sortArray[indexPath.row]
            
            let currentIndex = indexPath.row
            
            let selected = currentIndex == selectedSort

            cell.lblTitle.text = (sort.title)
            
            cell.isSelected(selected)
        
            
                
                return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
                updateSelectedIndex(indexPath.row)
                onClickTransparentView()
                
               self.page = 1
               self.sortType = sortKey[indexPath.row]
        
            
              if index == 0{
                         
                           getOfferProducts()
                     }else{
                         
                         getHomeSubCategory()
                         
                     }
        
              
        
               
        
    }
    
    
    
    
}
