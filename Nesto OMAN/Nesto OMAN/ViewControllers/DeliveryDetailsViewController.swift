//
//  DeliveryDetailsViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 30/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class DeliveryDetailsViewController: BaseViewController {
    
    
    @IBOutlet weak var nonDeliverMessageBar: UIView!
    
    @IBOutlet weak var nonDeliverBarHeightConstriant: NSLayoutConstraint!
    
    @IBOutlet weak var addressView: CurvedView!
    @IBOutlet weak var paymentView: CurvedView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var addOne: UILabel!
    @IBOutlet weak var addTwo: UILabel!
    
    @IBOutlet weak var addThree: UILabel!
    @IBOutlet weak var addFour: UILabel!
    @IBOutlet weak var imgCOD: UIImageView!
    @IBOutlet weak var imgCAD: UIImageView!
    @IBOutlet weak var addressScrollView: UIScrollView!
    @IBOutlet weak var specialInstructionView: UIView!
    @IBOutlet weak var btnAddress: CMButton!
    @IBOutlet weak var txtInstruction: UITextField!
    @IBOutlet weak var lblAddInstruction: UILabel!
    @IBOutlet weak var btnRemove: CMButton!
    
    @IBOutlet weak var timeSlotTableview: UITableView!
    @IBOutlet weak var slotView: UIView!
    
    @IBOutlet weak var lblInstantFee: UILabel!
    @IBOutlet weak var imgRadioFreeDelivery: UIImageView!
    @IBOutlet weak var imgRadioInstantDelivery: UIImageView!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblInstantDelivery: UILabel!
    @IBOutlet weak var btnInstantDelivery: UIButton!
    
    @IBOutlet weak var btnConfirmOrder: CMButton!
    
    var isCOD:Bool = true
    var paymentMethod:String = "cash_od"
    
    
    
    
    var shipRoomNo:String =  ""
    var shipBuildingName:String = ""
    var shipLatitude:String = ""
    var shipLongitude:String = ""
    var shipCityId:String = ""
    var specialInstruction:String =  ""
    var shipContact:String = ""
    var deliveryOption = "2"
    var subtotalAmt:Double?
    var minPurchaseAmt:Double?
    var addressData:[DeliveryAddressData]?
    var selectedAddress:DeliveryAddressData?
    var timeslots = [timeSlotData]()
    var slotDay = [String]()
    var deliveryMethod:[methodData]?
    var itemsCount:String?
    var instantFee:Double = 50.00
    var timeSlot:String = ""
    var addressId:String?
    var instantDeliveryAvailable = true
    
    @IBOutlet weak var lblisFree: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()

        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
      
        if instantDeliveryAvailable{
            
            self.btnInstantDelivery.isUserInteractionEnabled = true
            self.lblInstantDelivery.textColor = .black
            
            if let instantPrice = self.deliveryMethod?[0].addition_charge{
                       
                       self.lblInstantFee.text =  "(" + currency + instantPrice + ")"
                       
                   }
                   
            
        }else{
            
            self.btnInstantDelivery.isUserInteractionEnabled = false
            self.lblInstantDelivery.textColor = .lightGray
            
                       
        self.lblInstantFee.text =  "( " + "Not available now" + " )"
                       
            
                   
        }
        
        self.lblisFree.isHidden = true
        
        if let items = self.itemsCount{
            self.lblSubtotal.text = items + " : "
        }
        
       
        
        if let total = self.subtotalAmt {
            self.lblTotalAmount.text = currency + "\(total.roundToDecimal(3))"
            if let minValue = self.minPurchaseAmt{
                
                if total >= minValue{
                    
                    let valueStr = "FREE Delivery"
                    
                    let message = "* Your are eligible for FREE Delivery"
                    
                     self.lblisFree.attributedText = message.attributedString(subStr: valueStr, color:Colors.NOGreen)
                    self.lblisFree.isHidden = false

                    
                    
                   
                }else{
                    
                     let value = minValue - total
                     let valueStr = currency + "\(value.roundToDecimal(3))"
                     let message = "* To qualify for FREE Delivery, add " +  currency + "\(value.roundToDecimal(3))" + " of eligible items"
            

                    self.lblisFree.attributedText = message.attributedString(subStr: valueStr, color:Colors.NOGreen)
                   
                     self.lblisFree.isHidden = false
                    
                        }
                
                
            }
            
            
        }
        
        
        self.specialInstructionView.isHidden = true
        self.nonDeliverMessageBar.isHidden = true
        self.nonDeliverBarHeightConstriant.constant = 0
        self.addressScrollView.isHidden = true
        self.btnAddress.isHidden = true
        self.btnRemove.isHidden = true
        
        guard let cityId = defualts.string(forKey: "cityId") else{
               
               return
           }
        self.shipCityId = cityId

        
        if defualts.value(forKey: "customerId") != nil{
                   
            if let id = defualts.string(forKey: "customerId"){
                if let groceryId = defualts.string(forKey: "groceryId"){
                let idValue = id
                    if isConnected(){
                        getTimeSlots(customerId:idValue,groceryId:groceryId,methodId:"2")
                        
                    }
                }
                       
                   }
                   
               }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.AddressSelectedNotification(notification:)), name: Notification.Name(addressSelectNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.AddressEmptyNotification(notification:)), name: Notification.Name(addressEmptyNotification), object: nil)

        
        
        
  
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
      
        
        if let Address = self.selectedAddress{
            
            
            if let addressId = Address.address_Id{
                
                self.addressId = addressId
            }
            
            self.addressScrollView.isHidden = false
                                
                let address = Address
                                
            if let name = address.customer{
            
                                    self.lblName.text = name
                                    
                                }
                                
                    
            if let addressTitle = address.address_title{
                                
                    self.lblType.text = addressTitle
                                    
                                }
                                
            if let building = address.house_name{
                                    
                self.shipBuildingName = building
                                    
                                }
                                
          
            if let houseName = address.house_name{
                                                                                           
                        self.addOne.text = houseName
                                    
                                                                                                                
                                }
            
                                
            if let landmark = address.land_mark {
                                    
                                    
                self.addThree.text = landmark + landMark
                                    
                                    
            }
                                
            if let customerArea = address.customer_area{
                                    
                    self.addTwo.text = customerArea
                                    
                                }
                                
            if let longitude = address.longitude{
                                    
                                    self.shipLongitude = longitude
                                }
                                
            if let latitude = address.latitude{
                                    
                                    self.shipLatitude = latitude
                                }
                                
                                
                                
            if let phone = address.customer_mobile{
                                                                                
                                    self.addFour.text = phone
                                    self.shipContact = phone
                                    }
                                
            if let isDeliverable = address.is_deliverable{
                                    
                                    if Int(isDeliverable) == 1{
                                        
                                        self.nonDeliverMessageBar.isHidden = true
                                        self.nonDeliverBarHeightConstriant.constant = 0
                                        self.btnConfirmOrder.backgroundColor = Colors.NOGreen
                                        self.btnConfirmOrder.isUserInteractionEnabled = true
                                        
                                        
                                    }else{
                                        
                                        self.btnConfirmOrder.backgroundColor = Colors.NOGray
                                        self.btnConfirmOrder.isUserInteractionEnabled = false
                                        self.nonDeliverMessageBar.isHidden = false
                                        self.nonDeliverBarHeightConstriant.constant = 48
                                        
                                    }
                                    
                                }
                                                                                       
                     
                       
            
            
        }else{
            

            if defualts.value(forKey: "customerId") != nil{
                       
                if let id = defualts.string(forKey: "customerId"){
                    
                    if let groceryId = defualts.string(forKey: "groceryId"){
                           
                    let idValue = id
                    
                        if isConnected(){
                            
                            getAddressList(customerId:idValue , groceryId: groceryId)
                           
                            
                        }
                    }
                           
                       }
                       
                   }
            
            
        }
        
       
        
    }
    
    
     @objc func AddressEmptyNotification(notification: Notification) {
        
         self.selectedAddress = nil
        
    }
    
    
    @objc func AddressSelectedNotification(notification: Notification) {
             
        
        
        if let addressData = notification.userInfo?["address"] as? DeliveryAddressData{
        
            self.selectedAddress = addressData
            
        
        
        if let Address = self.selectedAddress{
            
            
            self.addressScrollView.isHidden = false
                                
                let address = Address
            
            if let addressId = address.address_Id{
                
                self.addressId = addressId
            }
                                
            if let name = address.customer{
            
                                    self.lblName.text = name
                                    
                                }
                                
                    
            if let addressTitle = address.address_title{
                                
                    self.lblType.text = addressTitle
                                    
                                }
                                
            if let building = address.house_name{
                                    
                self.shipBuildingName = building
                                    
                                }
                                
          
            if let houseName = address.house_name{
                                                                                           
                        self.addOne.text = houseName
                                    
                                                                                                                
                                }
            
                                
            if let landmark = address.land_mark {
                                    
                                    
                self.addThree.text = landmark + landMark
                                    
                                    
            }
                                
            if let customerArea = address.customer_area{
                                    
                    self.addTwo.text = customerArea
                                    
                                }
                                
            if let longitude = address.longitude{
                                    
                                    self.shipLongitude = longitude
                                }
                                
            if let latitude = address.latitude{
                                    
                                    self.shipLatitude = latitude
                                }
                                
                                
                                
            if let phone = address.customer_mobile{
                                                                                
                                    self.addFour.text = phone
                                    self.shipContact = phone
                                    }
                                
            if let isDeliverable = address.is_deliverable{
                                    
                                     if Int(isDeliverable) == 1{
                                                                            
                                                                            self.nonDeliverMessageBar.isHidden = true
                                                                            self.nonDeliverBarHeightConstriant.constant = 0
                                                                            self.btnConfirmOrder.backgroundColor = Colors.NOGreen
                                                                            self.btnConfirmOrder.isUserInteractionEnabled = true
                                                                            
                                                                            
                                                                        }else{
                                                                            
                                                                            self.btnConfirmOrder.backgroundColor = Colors.NOGray
                                                                            self.btnConfirmOrder.isUserInteractionEnabled = false
                                                                            self.nonDeliverMessageBar.isHidden = false
                                                                            self.nonDeliverBarHeightConstriant.constant = 48
                                                                            
                                                                        }
                                    
                                }
                                                                                       
                     
                       
            
            
        }
          
        }else{
            
            if defualts.value(forKey: "customerId") != nil{
                       
                if let id = defualts.string(forKey: "customerId"){
                    
                    if let groceryId = defualts.string(forKey: "groceryId"){
                           
                    let idValue = id
                    
                        if isConnected(){
                            
                            getAddressList(customerId:idValue , groceryId: groceryId)
                           
                            
                        }
                    }
                           
                       }
                       
                   }
            
            
        }
             
         }
    
    
    
    func getTimeSlots(customerId:String,groceryId:String,methodId:String){
        
        
        let timeParam = TimeSlotParameters(groceryId:groceryId,methodId:methodId,customerId:customerId).Values
        
        self.ApiRequest.getTimeSlot(withParameter:timeParam) { (isSuccess,message) in
                          
                           self.STProgress.dismiss()
                                         
                                         if isSuccess {
                                         
                                            if let info = self.ApiRequest.DeliverySlotResponse?.arrAvailableTimeSlotes{
                                                
                                                self.timeslots = info
                                        
                                                
                                                let slots = Dictionary(grouping: self.timeslots, by: { $0.slotDate })
                                                
                                                
                                                let componentArray = Array(slots.keys)
                                                
                                                for key in componentArray{
                                                    
                                                    self.slotDay.append(key!)
                                                }
                                                 
                                            
                                                if self.timeslots.count > 0 {
                                                    
                                                    self.timeSlotTableview.delegate = self
                                                    self.timeSlotTableview.dataSource = self
                                                    self.timeSlotTableview.reloadData()
                                                    self.timeSlotTableview.isHidden = false
                                                    
                                                }else{
                                                    
                                                    self.timeSlotTableview.isHidden = true
                                                    
                                                }
                                           
                                                
   
//

                                    
                                            }
                                        
                                          
                                         }else{
                                            
                                             self.timeSlotTableview.isHidden = true
                                            
                                            
            }
            
            
            }
        
        
        
    }
    
    
    @IBAction func btnRemovePressed(_ sender: Any) {
        
        self.lblAddInstruction.text = "Add Special Instructions"
        self.specialInstruction = ""
        
        
    }
    
    
    @IBAction func orderConfirmPressed(_ sender: Any) {
        
        
        guard let custId = defualts.string(forKey: "customerId") else {
            return
        }
        guard let gorceryId = defualts.string(forKey: "groceryId") else {
            return
        }
        
        
        if self.shipLatitude != "" && self.shipLongitude != ""{
            
             self.confirmOrder(customerId: custId, groceryId: gorceryId)
            
        }else{
            
            showAlert(message: "Please select delivery address")
        }
            
        
       
        
        
    }
    
    
    @IBAction func instructionOkPressed(_ sender: Any) {
          self.specialInstructionView.isHidden = true
        
        if self.specialInstruction != "" {
            
            self.lblAddInstruction.text = self.specialInstruction
            self.btnRemove.isHidden = false
            
        }
      }
      
      
      
      
      
      @IBAction func addSpecialInstruction(_ sender: Any) {
          
          self.specialInstructionView.isHidden = false
          
      }
    
    
    
    @IBAction func addInstructions(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            
                self.txtInstruction.text = "Do not ring bell"
                self.specialInstruction = "Do not ring bell"
        
            break
            
        case 2:
        
                
                self.txtInstruction.text = "Call for directions"
                self.specialInstruction = "Call for directions"
    
            break
        case 3:
        
                
                self.txtInstruction.text = "Just Knock"
                 self.specialInstruction = "Just Knock"
                
  
            break
        case 4:
         
                
                self.txtInstruction.text = "Please Come fast"
                 self.specialInstruction = "Please Come fast"
      
            break
        case 5:
          
                
                self.txtInstruction.text = "Bring swipe machine"
                 self.specialInstruction = "Bring swipe machine"
                
        
            break
        case 6:
         
                
                self.txtInstruction.text = "Bring change"
                 self.specialInstruction = "Bring change"
                
      
            break
            
        default:
            break
        }
        
        
        
        
    }
    
    
    
    @IBAction func backPressed(_ sender: Any) {
    
        navigationController?.popToViewController(ofClass: CartViewController.self)
       
    }
    
    
    @IBAction func instantDeliveryPressed(_ sender: Any) {
        
        self.imgRadioFreeDelivery.image = UIImage(named:"ic_radio")
        self.imgRadioInstantDelivery.image = UIImage(named:"ic_radio_sel")
        self.lblDeliveryTime.text = "Instant Delivery"
        
        self.deliveryOption = "1"
        self.slotView.isHidden = true
        
    }
    
    @IBAction func freeDeliveryPressed(_ sender: Any) {
        
          self.deliveryOption = "2"
        self.imgRadioFreeDelivery.image = UIImage(named:"ic_radio_sel")
         self.imgRadioInstantDelivery.image = UIImage(named:"ic_radio")
    }
    
    @IBAction func changeSlotPressed(_ sender: Any) {
        
        self.slotView.isHidden = false
    }
    
    
    
    @IBAction func codPressed(_ sender: Any) {
        
        self.imgCAD.image = UIImage(named: "ic_radio")
        self.imgCOD.image = UIImage(named: "ic_radio_sel")
        self.isCOD = true
        self.paymentMethod = "cash_od"
    }
    
    
    @IBAction func cadPressed(_ sender: Any) {
        
        self.imgCAD.image = UIImage(named: "ic_radio_sel")
        self.imgCOD.image = UIImage(named: "ic_radio")
        self.isCOD = false
        self.paymentMethod = "card_od"
    }
    
    
    @IBAction func changeAddressPressed(_ sender: Any) {
        
        let AddressScene = AddressListingViewController.instantiate(fromAppStoryboard: .Main)
        
            if let navigator = self.navigationController {
                
                AddressScene.addressData = self.addressData
        
            navigator.pushViewController(AddressScene, animated: true)
        
        
        
        }
                                     
        
    }
    
    
  
    @IBAction func addAddressPressed(_ sender: Any) {
        
        
        let AddressScene = UpdateAddressViewController.instantiate(fromAppStoryboard: .Main)
            
                        if let navigator = self.navigationController {
                        
                            AddressScene.isEdit = false
                           
                        navigator.pushViewController(AddressScene, animated: true)
        
        }
        
    }
    
    
    
    func confirmOrder(customerId:String,groceryId:String){
    
            
         
        
   
        guard let addressId = self.addressId else {return}
        
        let param = CartCheckOut(paymentMethod: self.paymentMethod, specialInstructions: self.specialInstruction, addressId:addressId, groceryId: groceryId, customerId: customerId, timeSlotId:self.timeSlot, deliveryTypeId:self.deliveryOption).Values
        
 
        
             self.STProgress.show()

            
            self.ApiRequest.checkOutOrder(withParameter:param) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                             
                                                
                                                DispatchQueue.main.async {
                                                    
                                                    
                                                    
                                                    
                                                    let HomeScene = HomeViewController.instantiate(fromAppStoryboard: .Main)
                                                         
                                                             if let navigator = self.navigationController {
                                                                 
                                                        
                                                             navigator.pushViewController(HomeScene, animated: true)
                                      
                                                        
                                                         }
                                                    
                                                    
                                                    defualts.set("0",forKey: "badge")
                                                    self.showAlert(message: "Order Placed Successfully")
                                                    
                                                }
                                                
                                                            
                                             }else{
                                                
                                                
                                                self.showAlert(message: wentWrong)
                                                        
                                                        
                                                        
                                                            
                                                        }
                                                                                                               
                                                        
                                                        
                                                       
                                                        
                                                        
                                                  
                                                        
                                                    }
                                                    

                                                    
                                                }
                                            
        
   
    func getAddressList(customerId:String,groceryId:String){
    
            
             self.STProgress.show()
        
        
        guard let groceryId = defualts.string(forKey: "groceryId") else{
            
            return
        }
            
            let cartParam = CartParams(customerId:customerId,groceryId:groceryId).Values
            
            self.ApiRequest.getAddressList(withParameter:cartParam) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                             
                                                if let info = self.ApiRequest.DeliveryAddressResponse?.arrAdress{
                                                    
                                                    self.addressData = info
                                                    
                                                    if let area = self.ApiRequest.DeliveryAddressResponse?.deliverableKm{
                                                        
                                                        defualts.setValue(area,forKey:"deliverableArea")
                                                        
                                                        
                                                    }
                                                    
                                                    if self.addressData?.count ?? 0 > 0 {
                                                        
                                                        
                                                        
                                                        
                                                       self.addressScrollView.isHidden = false

                                                        
                                                        let address = self.addressData?[0]
                                                        
                                                        if let name = address?.customer{
                                                            
                                                            
                                                            self.lblName.text = name
                                                            
                                                        }
                                                        
                                                        
                                                        if let addressId = address?.address_Id{
                                                            
                                                            self.addressId = addressId
                                                        }
                                                        
                                                        
                                                        
                                                        if let addressTitle = address?.address_title{
                                                            
                                                            
                                                            self.lblType.text = addressTitle
                                                            
                                                        }
                                                        
                                                        if let houseName = address?.house_name{
                                                                                                                                       
                                                                    self.addOne.text = houseName
                                                                                
                                                                   // self.shipRoomNo = roomNo
                                                                              
                                                                            }
                                                        
                                                                            
                                                        if let landmark = address?.land_mark {
                                                                                
                                                            
                                                            if let customerCity = address?.customer_City{
                                                         
                                                                self.addThree.text = landmark + landMark + ", " + customerCity
                                                                }
                                                                                

                                                                                
                                                        }
                                                                            
                                                        if let customerArea = address?.customer_area{
                                                                                
                                                                self.addTwo.text = customerArea
                                                                                
                                                                            }
                                                        
                                                       
                                                        
                                                     
                                                        
                                                        if let longitude = address?.longitude{
                                                            
                                                            self.shipLongitude = longitude
                                                        }
                                                        
                                                        if let latitude = address?.latitude{
                                                            
                                                            self.shipLatitude = latitude
                                                        }
                                                        
                                                        
                                                        
                                                        if let phone = address?.customer_mobile{
                                                                                                        
                                                            self.addFour.text = mob + phone
                                                            self.shipContact = phone
                                                            }
                                                        
                                                        if let isDeliverable = address?.is_deliverable{
                                                            
                                                              if Int(isDeliverable) == 1{
                                                                                                    
                                                                                                    self.nonDeliverMessageBar.isHidden = true
                                                                                                    self.nonDeliverBarHeightConstriant.constant = 0
                                                                                                    self.btnConfirmOrder.backgroundColor = Colors.NOGreen
                                                                                                    self.btnConfirmOrder.isUserInteractionEnabled = true
                                                                                                    
                                                                                                    
                                                                                                }else{
                                                                                                    
                                                                                                    self.btnConfirmOrder.backgroundColor = Colors.NOGray
                                                                                                    self.btnConfirmOrder.isUserInteractionEnabled = false
                                                                                                    self.nonDeliverMessageBar.isHidden = false
                                                                                                    self.nonDeliverBarHeightConstriant.constant = 48
                                                                                                    
                                                                                                }
                                                            
                                                        }
                                                                                                               
                                             
                                                    }else{
                                                        
                                                         self.btnAddress.isHidden = false
                                                        
                                                        self.addressView.isHidden = true
                                                        
                                                        
                                                    }
                                                    

                                        
                                                }
                                            
                                              
                                             }else{
                                                
                                                
                                                 self.nonDeliverMessageBar.isHidden = true
                                                 self.nonDeliverBarHeightConstriant.constant = 0
                                                 self.addressScrollView.isHidden = true
                                                self.btnAddress.isHidden = false
                                                
                                                //self.showAlert(message: wentWrong)
                                                
                                               
                                                                                                                                                 
                           }
                }
               
               }

}

extension DeliveryDetailsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
                
        
        let slots = Dictionary(grouping: self.timeslots, by: { $0.slotDate })
        
        let timeSlots = slots.filter { $0.key == self.slotDay[section]}
     
        
        return timeSlots[self.slotDay[section]]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.timeSlotTableview.dequeueReusableCell(withIdentifier: sortCell, for: indexPath) as! SortTableViewCell
        
        
        let slots = Dictionary(grouping: self.timeslots, by: { $0.slotDate })
        let timeslots = slots.filter { $0.key == self.slotDay[indexPath.section]}
        
       
        cell.lblTitle.text  = timeslots[self.slotDay[indexPath.section]]?[indexPath.row].timeslotes
        return cell
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return self.slotDay.count
      }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let slots = Dictionary(grouping: self.timeslots, by: { $0.slotDate })
         let timeslots = slots.filter { $0.key == self.slotDay[indexPath.section]}
    
        
        if let time = timeslots[self.slotDay[indexPath.section]]?[indexPath.row].timeslotes{
            let date = self.slotDay[indexPath.section].fullDate()
            
         
            
            
            self.lblDeliveryTime.text = date + ", " + time
            
            if let slotId = timeslots[self.slotDay[indexPath.section]]?[indexPath.row].id{
                
               self.timeSlot = slotId
            }
            if let message = timeslots[self.slotDay[indexPath.section]]?[indexPath.row].alert_message{
               
                if message != null && message != "null"{
                    
                    showAlert(message: message)
                }
            
           }

            
        }
        
             self.slotView.isHidden = true
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
   
        let header = view as! UITableViewHeaderFooterView
          
          view.tintColor = .white
          header.textLabel?.font =  header.textLabel?.font.withSize(14)
          header.textLabel?.textColor =  #colorLiteral(red: 0, green: 0.5820022225, blue: 0.2493254542, alpha: 1)

      }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return  self.slotDay[section].fullDate()
       }
      
      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          
          return 35.0
      }
      

    
    
}


