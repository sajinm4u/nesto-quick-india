//
//  MyWishListViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 03/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class MyWishListViewController: BaseViewController {
    
    
    @IBOutlet weak var bannerImg: UIImageView!
 
       
       @IBOutlet weak var productCollectionView: UICollectionView!
      
       var favouriteData:[OfferProductData]?
       var categoryTitle:String?
       var bannerUrl:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
   

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
             initialSetup()
        
    }

    
     func initialSetup(){
        
        guard let custId = defualts.string(forKey: "customerId") else{
         
        return
    
        }
        
        
        guard let groceryId = defualts.string(forKey: "groceryId") else{
               
              return
          
              }
        
            getWishlist(groceryId: groceryId, customerId: custId)
        
        
        
     }
    
    func getWishlist(groceryId:String,customerId:String){
        
  
        
        self.STProgress.show()
        

        let params = OfferProductParameters(groceryId: groceryId, customerId: customerId).Values
                              
                              ApiRequest.getFavouriteProducts(withParameter: params) { (isSuccess,message) in
                               
                                self.STProgress.dismiss()
                                              
                                              if isSuccess {
                                                  
                                                  if let data = self.ApiRequest.FavoriteProductResponse {
                                                      
                                                    self.favouriteData = data.favouriteProducts
                                                    
                                                    if let imgUrl = data.appFavouriteURL{
                                                        
                                                        let url = URL(string: imgUrl)
                                                                                let phImage = UIImage(named: ph)
                                                                               
                                                        self.bannerImg?.kf.setImage(with: url, placeholder: phImage)
                                                        
                                                               
                                                    }
                                                    
                                                    if self.favouriteData?.count ?? 0 > 0{
                                                        
                                                        self.productCollectionView.delegate = self
                                                        self.productCollectionView.dataSource = self
                                                        self.productCollectionView.reloadData()
                                                        
                                                        
                                                    }
                                                      
                                                  }
                                              
                                         
                                             
                                               
                                              }
                
                }
                
                
             
             
         }
    

    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
        
    }
    
}
extension MyWishListViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        
        return self.favouriteData?.count ?? 0
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
        if let info = self.favouriteData?[indexPath.row]{
        
        let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            ProductDetailsScene.productData = info
            
            navigator.pushViewController(ProductDetailsScene, animated: true)
            
            
            }
        }
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
            
        
            
            if let info = self.favouriteData?[indexPath.row]{
                
                if let title = info.productName{
                    
                    cell.lblProductName.text = title
                    
                }
                
                if info.is_special_offer_product ?? false {
                                           
                                           cell.offerTagView.isHidden = false
                                       }else{
                                            cell.offerTagView.isHidden = true
                                       }
                
                if let off = info.offerPercentage{
                    
                    if Double(off)! > 0.0{
                        
                        cell.lblOff.text = off + "% OFF"
                        cell.offView.isHidden = false
                        cell.lblPrice.isHidden = false
                        
                        if let price = info.groceryproductprice {
                                        
                                        let priceStr = mrp + currency + price
                                        
                                         cell.lblPrice.attributedText = priceStr.strikeThrough()
                                        
                                    }
                        
                        if let offerPrice = info.offerPrice{
                                           
                                           cell.lblOfferPrice.text = nrp + currency + offerPrice
                                           
                                       }
                        
                    }else{
                        
                        if let price = info.groceryproductprice {
                            
                            cell.lblOfferPrice.text = nrp + currency + price
                            
                        }
                        
                        cell.lblPrice.isHidden = true
                        cell.offView.isHidden = true
                        
                    }
                    
                    
                }else{
                    
                    if let price = info.price{
                        
                        cell.lblPrice.isHidden = true
                        cell.lblOfferPrice.text = nrp + currency + price
                    }
                    
                      cell.offView.isHidden = true
                    
                    
                    
                }

                
                
                
                
            
                
               
                
                if let weight = info.weight {
                    
                    if let weightQty = info.qty_weight{
                        
                        let weightStr = weightQty + " " + weight
                        
                        cell.lblWeight.text = weightStr
                        
                        
                    }
                
                }
                
                if let imgUrl = info.product_image1{
                    
                    let url = URL(string: imgUrl)
                     let phImage = UIImage(named: ph)
                    
                     cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }
                
                
            }
             
           
             
             
                return cell
            
  
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
              
               
               let width = (collectionView.frame.size.width - 5 * 2) / 2
        let height = width * 1.35 //ratio
               return CGSize(width: width, height: height)
               
           
    }
    
    
}
