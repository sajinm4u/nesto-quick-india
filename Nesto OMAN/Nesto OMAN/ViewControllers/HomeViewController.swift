//
//  HomeViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 18/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import FSPagerView
import Kingfisher
import BadgeSwift
import SwiftyGif

class HomeViewController: BaseViewController {
    
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var mainCategoryCollectionView: UICollectionView!
    @IBOutlet weak var categoryViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageControl: FSPageControl!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblBadge: BadgeSwift!
    @IBOutlet weak var addProductView: UIView!
    @IBOutlet weak var offerTag: UIImageView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var lblActualPrice: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblOfferPrice: UILabel!
    @IBOutlet weak var lblOff: UILabel!
    @IBOutlet weak var offView: UILabel!
    @IBOutlet weak var lblStoreTitle: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var txtCount: UITextField!
    
    @IBOutlet weak var updateView: UIView!
    @IBOutlet weak var bannerMessageIcon: UIImageView!
    @IBOutlet weak var bannerMessageTitle: UILabel!
    @IBOutlet weak var bannerMessageDescription: UILabel!
    @IBOutlet weak var givView: UIImageView!
    
    var searchFullArray = [CartProducts]()
    var totalSearchCount:String?
    var currentSearchCount:Int?
    
    var firstSubCategoryBanner:String? = ""
    var secondSubCategoryBanner:String? = ""
    
    var listCount = 0
    
    
    
    @IBOutlet weak var bannerView: FSPagerView!{
        didSet {
            self.bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: bannerCell)
        }
    }
    
    
    
    var groceryId:String?
    var minCount = 1
    var selectedItemPrice = 0.0
    var customerId:String = "0"
    var bannerData:[BannerData]?
    var categoryData:MainCategoryModel?
    var offerProducts:OfferProducts?
    var subCategories = [[OfferProductData]]()
    var brands:[BrandModelData]?
 
    
    var groceryTitle:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // checkForUpdation()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
        
        
     
        
        self.updateView.isHidden = true
        if let grocId = defualts.string(forKey: "groceryId"){
            
            self.groceryId = grocId
        }
  
        
        self.txtSearch.delegate = self
        self.searchView.isHidden = true
        self.searchTableView.tableFooterView = UIView(frame:.zero)
        self.btnMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
        
        
          NotificationCenter.default.addObserver(self, selector: #selector(self.HomeReloadNotification(notification:)), name: Notification.Name(DashBoardReload), object: nil)
        
        
        if let title = self.groceryTitle{
            
            self.lblStoreTitle.text = title
            
        }else{
            
            if let title = defualts.string(forKey: "groceryName"){
                
                self.lblStoreTitle.text = title
            }
            
            
        }
        
        
        self.lblBadge.isHidden = true
        if defualts.value(forKey: "customerId") != nil{
            
            
            if let id = defualts.string(forKey: "customerId") {
            
                
                self.customerId = id
                
        
            }
            
            
            
        }
        
        
        
        
        self.addProductView.isHidden = true
        
        
        if isConnected(){
        
            getBanners()
            getCategories()
            getOfferProducts()
            getBrands()
            getHomeSubCategory()
        }
        
        self.tableviewHeightConstraint.constant = (290 * 3) + 200
        
        
    }
    
    
    func checkForUpdation() {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        guard let version = appVersion else{
            
            return
        }
        
        let parms = VersionParams(version: version).Values
        ApiManager.shared.checkVersion(fromUrl:URL(string: Api.versionUpdate)!,  withParameter: parms) { (isSuccess, noUpdate, message) in
            
            if isSuccess{
                
                if noUpdate{
                    
                    self.updateView.isHidden = true
                    
                }else{
                    
                    self.updateView.isHidden = false
                    
                }
                
                            }
            
        }
        
    }
    
    
    @objc func HomeReloadNotification(notification: Notification) {
          
          
        getOfferProducts()
        getHomeSubCategory()
    
          
      }
    
    
    
    @IBAction func searchCancelPressed(_ sender: Any) {
        
        self.searchView.isHidden = true
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
    
      
       setBadge()
        
    }
    
    func setBadge(){
        
        
        if let custId = defualts.string(forKey: "customerId"){
        
        if let count = defualts.string(forKey: "badge"){
            
            if count != "0"{
                
                self.lblBadge.isHidden = false
                self.lblBadge.text = count
                
            }else{
                 self.lblBadge.isHidden = true
                
            }
            
           
         }
        }
        
        
    }
    
    func search(product:String,limit:String){
        
        
        guard let id = defualts.string(forKey: "groceryId") else{
            
            
            return
        }
        
        
        self.STProgress.show()
        
        
        let params = SearchParam(groceryId: id, searchKey: product, limit: limit).Values
        
        ApiRequest.searchItem(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                if let info = self.ApiRequest.SearchResponse?.products{
                    
                    
                    
                    if info.count > 0{
                        
                        self.totalSearchCount = self.ApiRequest.SearchResponse?.totalCount
                        
                        self.searchFullArray = info
                        self.searchTableView.delegate = self
                        self.searchTableView.dataSource = self
                        self.searchTableView.reloadData()
                        self.searchView.isHidden = false
                        
                        
                    }
                    
                    
                    
                }
                
                
                
                
            }
            
        }
        
        
    }
    
    
    
    
    func getBrands(){
        
        
        self.STProgress.show()
        
        
        ApiRequest.getBrandList(withParameter: [:]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                if let info = self.ApiRequest.BrandResponse?.brands{
                    
                    
                    if info.count > 0 {
                        
                        
                        
                        self.listCount = self.listCount + 1
                        self.brands = info
                        
                      
                        self.homeTableView.delegate = self
                        self.homeTableView.dataSource = self
                        self.homeTableView.reloadData()
                        
                        
                    }
                    
                    
                }
                

                
                
            }
            
        }
        
        
    }
    
    
    
    
    func getBanners(){
        
        
        self.STProgress.show()
        
        
        
        let params = BannerParameters(groceryId:self.groceryId ?? "2").Values
        
        ApiRequest.getBanner(withParameter: params) { (isSuccess,bannerData,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                if let info = self.ApiRequest.BannerResponse{
                    
                    if let gifUrl = info.homeGif {
                        
                       
                        let loader = UIActivityIndicatorView(style: .white)
                        
                        guard let gif = URL(string: gifUrl) else {return}
                        self.givView.setGifFromURL(gif, customLoader: loader)
                    
                      
                        
                    }
                    
                    
                    
                    if let imgUrl = info.bannerIcon{
                        
                        let url = URL(string: imgUrl)
                                      let phImage = UIImage(named: phSmall)
                                      
                        self.bannerMessageIcon.kf.setImage(with: url, placeholder: phImage)
                        
                    }
                    
                    
                    if let bannerTitle = info.bannerTitle{
                        
                        self.bannerMessageTitle.text = bannerTitle
                        
                    }
                    
                    if let bannerBody = info.bannerBody{
                                           
                        self.bannerMessageDescription.text = bannerBody
                                           
                                       }
                    
                }
                
                if bannerData?.count ?? 0 > 0 {
                    
                    self.pageControl.numberOfPages = bannerData?.count as! Int
                    self.pageControl.setFillColor(.lightGray, for: .normal)
                    self.pageControl.setFillColor(Colors.NOGreen, for: .selected)
                    self.pageControl.contentHorizontalAlignment = .center
                    
                    self.bannerData = bannerData
                    self.bannerView.delegate = self
                    self.bannerView.dataSource = self
                    self.bannerView.reloadData()
                    
                }
                
                
            }
            
        }
        
        
    }
    
    @objc func textFieldDidChange(textField : UITextField){
        if self.txtSearch.text?.count ?? 0 > 3{
            
        
            
        }
    }
    
    
    func getOfferProducts(){
        
        self.STProgress.show()
        
        
        
        let params = OfferProductParameters(groceryId: self.groceryId ??  "2", customerId:self.customerId).Values
        
        ApiRequest.getOfferProducts(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let data = self.ApiRequest.OfferProductsResponse {
                    
                    self.offerProducts = data
                    
                    if data.eachCategory?.count ?? 0 > 0{
                        
                        self.listCount = 1
                        
                        self.homeTableView.delegate = self
                        self.homeTableView.dataSource = self
                        self.homeTableView.reloadData()
                        
                        
                    }
                    
                }
                
                
                
                
            }
            
        }
        
        
        
        
    }
    

    
    func getHomeSubCategory(){
        
        self.STProgress.show()
        
        
        
        let params = CategoryWithCutomerId(groceryId:self.groceryId ?? "2",customerId: self.customerId).Values
        
        
        ApiRequest.getHomeSubCategory(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let data = self.ApiRequest.HomeSubCategoryResponse {
                    
                    
                    if let banner = data.appFifteenURL_Image_1{
                        
                        self.firstSubCategoryBanner = banner
                    }
                    
                    if let banner = data.appFifteenURL_Image_2{
                        
                        self.secondSubCategoryBanner = banner
                    }
                    
                   
                    
                    
                    if let firstCategory = data.eachCategory?.first?.category{
                        
                        let foundItems = data.eachCategory?.filter{ $0.category == firstCategory }
                        
                        if foundItems?.count ?? 0 > 0{
                            
                            self.subCategories.append(foundItems!)
                        }
                        
                    }
                    if let lastCategory = data.eachCategory?.last?.category{
                        
                        let foundItems = data.eachCategory?.filter{ $0.category == lastCategory }
                        
                        if foundItems?.count ?? 0 > 0{
                            
                            self.subCategories.append(foundItems!)
                        }
                        
                    }
                    
                    
                    if self.subCategories.count > 0 {
                        
                   
                        
                        self.listCount = self.listCount + self.subCategories.count
                        
                        self.homeTableView.delegate = self
                        self.homeTableView.dataSource = self
                        self.homeTableView.reloadData()
                        
                    }
                    
                    
                    
                }
                
                
                
                
            }
            
        }
        
        
        
        
    }
    
    
    
    
    func getCategories(){
        
        
        self.STProgress.show()
        
        
        
        var params = CategoryParameters(groceryId: self.groceryId ?? "2").Values
        
        if self.customerId != "0" {
            
            params = CategoryWithCutomerId(groceryId: self.groceryId ?? "2",customerId: self.customerId).Values
            
        }
        
        
        
        ApiRequest.getMainCategory(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let data = self.ApiRequest.MainCategoryResponse {
                    
                    
                    
                    
                    if let cartCount = data.cartCount{
                        
                         defualts.set(cartCount, forKey: "badge")
                        
                        if let custId = defualts.string(forKey: "customerId"){
                        
                        if Int(cartCount) ?? 0 > 0{
                            
                           
                            self.lblBadge.text = cartCount
                            self.lblBadge.isHidden = false
                            
                    
                            
                            
                        }else{
                            
                             defualts.set("0", forKey: "badge")
                             self.lblBadge.isHidden = true
                            
                            }
                    }
                        
                    }
                    
                    self.categoryData = data
                    
                    
                    if data.mainCategory.count > 0 {
                        
                        
                        
                        let bannerHeight = (self.bannerView.frame.size.height - self.mainCategoryCollectionView.frame.size.height)
                        
                        let heightDifference = (self.mainCategoryCollectionView.frame.size.height + bannerHeight) - 10
                        
                        
                        self.mainCategoryCollectionView.dataSource = self
                        self.mainCategoryCollectionView.delegate = self
                        self.mainCategoryCollectionView.reloadData()
                        
                        
                        let rowValue = (Double(data.mainCategory.count)/3.0).rounded()
                        
                        self.categoryViewHeightConstraint.constant = CGFloat(CGFloat(rowValue) * heightDifference)
                        
                        
                        
                        
                    }
                    
                }
                
                
                
            }
            
        }
        
        
        
        
    }
    
    
    
    // MARK: Add to cart
    
    
//    @IBAction func addToCartPressed(_ sender: Any) {
//
//
//        if defualts.value(forKey: "customerId") != nil{
//
//            if let id = defualts.value(forKey: "customerId") as? String{
//
//                self.customerId = id
//
//            }
//
//        }
//
//
//        if self.customerId != "0"{
//
//            if self.productId != "" && self.weight != "" && self.price != "" && self.article != "" {
//
//                addToCart(productId: self.productId, groceryId: "2", quantity: self.quantity, weight:self.weight, price: self.price, article: self.article, customerId:self.customerId, description: self.desc)
//
//
//            }else{
//
//                showAlert(message: wentWrong)
//            }
//
//
//        }else{
//
//            self.STAlert.alert(title:AppName , message: pleaseLogin )
//                .action(.destructive("Login")){
//
//                    let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
//
//                    if let navigator = self.navigationController {
//
//                        navigator.pushViewController(loginScene, animated: true)
//
//
//
//                    }
//
//
//            }.action(.cancel("Cancel"))
//                .show(on: self)
//
//        }
//
//
//
//    }
    
    
//    func addToCart(productId:String,groceryId:String,quantity:String,weight:String,price:String,article:String,customerId:String,description:String){
//
//        self.STProgress.show()
//
//
//
//        let params = AddtoCartParam(productId: productId, groceryId: groceryId, quantity: quantity, weight: weight, price: price, article: article, customerId: customerId, description: description).Values
//
//        ApiRequest.addTocart(withParameter: params) { (isSuccess,cartCount,message) in
//
//            self.STProgress.dismiss()
//
//            if isSuccess {
//
//
//                DispatchQueue.main.async {
//
//
//                    self.addProductView.isHidden = true
//                    self.lblBadge.text = cartCount
//                    self.lblBadge.isHidden = false
//
//                    self.showNotificationAddProduct(message: productAdded)
//
//                }
//
//
//
//
//            }
//
//        }
//
//
//
//
//    }
    
    
    @IBAction func cancelAddProductPressed(_ sender: Any) {
        
        self.addProductView.isHidden = true
        
    }
    
    
    @IBAction func cartPressed(_ sender: Any) {
        
        
        if defualts.value(forKey: "customerId") != nil{
            
            
            
            if let id = defualts.string(forKey: "customerId"){
                
                self.customerId = id
                
                let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
                           
                           if let navigator = self.navigationController {
                               
                               navigator.pushViewController(CartScene, animated: true)
                           }
                           
                           
                
            }
            
        }else{
            
            
            self.STAlert.alert(title:AppName , message: pleaseLogin )
                .action(.destructive("Login")){
                    
                    let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                        navigator.pushViewController(loginScene, animated: true)
                        
                        
                        
                    }
                    
                    
            }.action(.cancel("Cancel"))
                .show(on: self)
            
            
        }
        
        
        
        
        
        
    }
    
    
    

    
    
    
}








extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if tableView == searchTableView{
            
            let info = searchFullArray[indexPath.row]
            
            self.searchView.isHidden = true
            self.txtSearch.text = ""


            let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {


                ProductDetailsScene.searchData = info
                ProductDetailsScene.isSearch = true
                ProductDetailsScene.similarProducts = searchFullArray
                navigator.pushViewController(ProductDetailsScene, animated: true)



            }
               
               
            
        }
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == searchTableView{
            
            return self.searchFullArray.count
            
        }
        else{
            return 4 //self.listCount
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if tableView == searchTableView{
            
            
            let cell = searchTableView.dequeueReusableCell(withIdentifier: searchCell, for: indexPath) as! SearchCell
            
            
            
            let info = self.searchFullArray[indexPath.row]
            
            cell.lblTitle.text = info.product_name
            
            if let imgUrl = info.product_image1{
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                
                cell.itemPic?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            if (indexPath.row == self.searchFullArray.count - 1) {
                var count = 10
                let totalCount = Int(self.totalSearchCount ?? "10")!
                if self.searchFullArray.count < totalCount {
                    count = self.searchFullArray.count + 10
                    self.search(product: self.txtSearch.text!, limit: "\(count)")
                    
                }
            }
            
            return cell
            
        } else  {
            
            
            let cell = homeTableView.dequeueReusableCell(withIdentifier: homeCell, for: indexPath) as! HomeTableCell
            
            
            
            
            if indexPath.row == 0{
                
                
                cell.lblCategoryTitle.text = "NESTO DEALS"
                cell.categoryCollectionView.tag = indexPath.row
                cell.categoryCollectionView.dataSource = self
                cell.categoryCollectionView.delegate = self
                cell.categoryCollectionView.reloadData()
                cell.index = indexPath.row
                cell.delegate = self
                
                
            }
            
            if indexPath.row == 1 {
                
            let cell = homeTableView.dequeueReusableCell(withIdentifier: BrandTableCell, for: indexPath) as! BannerTableViewCell
                
                cell.brandCollectionView.tag = indexPath.row
                cell.brandCollectionView.dataSource = self
                cell.brandCollectionView.delegate = self
                cell.brandCollectionView.reloadData()
                cell.index = indexPath.row
                
                
                return cell
                
            }
            
            if indexPath.row == 2{
                
               
                
                if self.subCategories.count > 0{
                    
                    if self.subCategories[0].count > 0{
                        
                        if let title = self.subCategories[0][0].category{
                            
                            cell.categoryCollectionView.tag = indexPath.row
                            cell.lblCategoryTitle.text = title.uppercased()
                            cell.categoryCollectionView.dataSource = self
                            cell.categoryCollectionView.delegate = self
                            cell.categoryCollectionView.reloadData()
                            cell.index = indexPath.row
                            cell.delegate = self
                        }
                        
                    }
                    
                }
            }
            
            if indexPath.row == 3{
                
                if self.subCategories.count > 0{
                    
                    if self.subCategories[1].count > 0{
                        
                        if let title = self.subCategories[1][0].category{
                            
                            cell.categoryCollectionView.tag = indexPath.row
                            cell.lblCategoryTitle.text = title.uppercased()
                            cell.categoryCollectionView.dataSource = self
                            cell.categoryCollectionView.delegate = self
                            cell.categoryCollectionView.reloadData()
                            cell.index = indexPath.row
                            cell.delegate = self
                            
                            
                        }
                        
                    }
                    
                }
            }
            
            return cell
            
        }
        
        let cell = UITableViewCell()
        
        
        return cell
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cell = homeTableView.dequeueReusableCell(withIdentifier:homeCell, for: indexPath) as! HomeTableCell
        
        
        cell.categoryCollectionView.tag = indexPath.row
        cell.categoryCollectionView.reloadData()
        
        
    }
    
    
}


extension HomeViewController: UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        self.minCount = 1
       // self.addProductView.isHidden = false
        self.txtCount.text = "\(minCount)"
        self.lblCount.text = "x " + "\(minCount)"
        
        
        if collectionView == self.mainCategoryCollectionView{
            
            if let info = self.categoryData?.mainCategory[indexPath.row]{
                
                if let isGroup = info.isGroup {
                    
                    if isGroup == 0{
                        
                        
                        let LoadCategoryScene = LoadFromCategoryViewController.instantiate(fromAppStoryboard: .Main)
                                               
                                                      if let navigator = self.navigationController {
                                                       
                                                         let groupID = info.id
                                                           
                                                           if let title = info.mainCategory{
                                                               
                                                                LoadCategoryScene.category_Title = title
                                                           }
                                                           
                                                          
                                                           LoadCategoryScene.groupId =  "\(groupID)"
                                                           LoadCategoryScene.flagKey = "0"
                                                           navigator.pushViewController(LoadCategoryScene, animated: true)
                                                                                                 
                                                      }
                        
                        
                        
                    }else{
                    
                    
                    let SubMainCategoryScene = SubMainCategoryViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                        SubMainCategoryScene.mainCataID = "\(info.id)"
                        
                        if let category = info.mainCategory{
                            
                            SubMainCategoryScene.categoryTitle = category
                        }
                        
                        
                        navigator.pushViewController(SubMainCategoryScene, animated: true)
                        }
                    }
                    
                }
                
              
                
            }
            
            
            
        }
        
        
        
        if collectionView.tag == 0{
            
            if let info = self.offerProducts?.eachCategory?[indexPath.row]{
                
                let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    ProductDetailsScene.productData = info
                    
                    navigator.pushViewController(ProductDetailsScene, animated: true)
                    
                    
                    
                }
            }
            
            
        }
        
        if collectionView.tag == 1{
            
            if let info = self.brands?[indexPath.row]{
                
                
                let BrandScene = BrandProductViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    BrandScene.brand = info.brand

                    navigator.pushViewController(BrandScene, animated: true)
                    
                    
                    
                }
                
                
                
            }
            
            
        }
        
        
        
        
        if collectionView.tag == 2{
            
            if self.subCategories[0][indexPath.row] !=  nil{
                
                let info = self.subCategories[0][indexPath.row]
                
                
                
                
                let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    ProductDetailsScene.productData = info
                    
                    navigator.pushViewController(ProductDetailsScene, animated: true)
                    
                }
                
            }
            
        }
        
        
        if collectionView.tag == 3{
            
            if self.subCategories[1][indexPath.row] !=  nil{
                
                let info = self.subCategories[1][indexPath.row]
                
                
                
                
                let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    ProductDetailsScene.productData = info
                    
                    navigator.pushViewController(ProductDetailsScene, animated: true)
                    
                }
                
            }
            
        }
        
        
    }
    
    
    
    
}


extension HomeViewController: UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == mainCategoryCollectionView {
            
            let width = (self.view.frame.size.width - 6 * 3) / 3
            let height = width * 1.3 //ratio
            return CGSize(width: width, height: height)
            
        }else if collectionView.tag == 1{
            
            return CGSize(width: 172, height: 125)
            
            
        }else{
            
            return CGSize(width: 147, height: 254)
        }
        
        
    }
    
}

extension HomeViewController: UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       
        
        if collectionView == mainCategoryCollectionView{
            
            return self.categoryData?.mainCategory.count ?? 0
            
            
        }else if collectionView.tag == 0{
            
        return self.offerProducts?.eachCategory?.count ?? 0
            
            
        }
        else if collectionView.tag == 1{
            
            print(brands?.count)
            
            if brands?.count ?? 0 > 0{
                
                return brands?.count ?? 0
            }
            
        }
            
        else if collectionView.tag == 2{
            
            if self.subCategories.count > 0{
                
                if self.subCategories[0].count > 0{
                    
                    return self.subCategories[0].count
                    
                }
                
                
            }
            
            
        }else if collectionView.tag == 3{
            
            if self.subCategories.count > 0{
                
                if self.subCategories[1].count > 0{
                    
                    return self.subCategories[1].count
                    
                }
                
                
            }
            
            
        }
        
        
        
        return 0
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == mainCategoryCollectionView{
            
            let cell = mainCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: mainCategoryCell, for: indexPath) as! MainCategoryCollectionCell
            
            if let info = self.categoryData?.mainCategory[indexPath.row]{
                
                if let title = info.mainCategory{
                    
                    cell.lblTitle.text = title
                }
                
                if let imgUrl = info.categoryImage{
                    
                    let url = URL(string: imgUrl)
                    let phImage = UIImage(named: ph)
                    
                    cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }
                
            }
            
            
            
            return cell
            
        }
        
        
        if collectionView.tag == 0{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
        
            
            if let info = self.offerProducts?.eachCategory?[indexPath.row]{
                
                if let title = info.productName{
                    
                    cell.lblProductName.text = title
                    
                }
                
                if let off = info.offerPercentage{
                    
                    if Double(off)! > 0.0{
                        
                        cell.lblOff.text = off + "% OFF"
                        cell.offView.isHidden = false
                        
                        if info.is_special_offer_product ?? false {
                            
                            cell.offerTagView.isHidden = false
                        }else{
                             cell.offerTagView.isHidden = true
                        }
                        
                       
                        
                        if let price = info.groceryproductprice{
                            
                            let priceStr = mrp + currency + price
                            
                            cell.lblPrice.attributedText = priceStr.strikeThrough()
                            
                        }
                        
                        if let offerPrice = info.offerPrice{
                            
                            cell.lblOfferPrice.text = nrp + currency + offerPrice
                            
                        }
                        
                    }else{
                        
                        
                         if let price = info.groceryproductprice{
                        
                        cell.lblOfferPrice.text = nrp + currency + price
                        cell.offView.isHidden = true
                        if info.is_special_offer_product ?? false {
                                                   
                        cell.offerTagView.isHidden = false
                        }else{
                        cell.offerTagView.isHidden = true
                        }
                        
                        }
                    }
                    
                    
                }
                
                
                
                
                
                if let weight = info.weight {
                    
                    if let weightQty = info.qty_weight{
                        
                        let weightStr = weightQty + " " + weight
                        
                        cell.lblWeight.text = weightStr
                        
                        
                    }
                    
                }
                
                if let imgUrl = info.product_image1{
                    
                    let url = URL(string: imgUrl)
                    let phImage = UIImage(named: ph)
                    
                    cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }
                
                
            }
            
            
            
            
            return cell
            
            
            
        }
        
        if collectionView.tag == 1 {
            
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandCell, for: indexPath) as! BrandCollectionCell
            
            if let info = self.brands?[indexPath.row]{
                
                if let imgUrl = info.brandLogo{
                             
                             let url = URL(string: imgUrl)
                             let phImage = UIImage(named: ph)
                             
                             cell.imgBrand?.kf.setImage(with: url, placeholder: phImage)
                             
                             
                         }
                
                
            }
            

            return cell
            
        }
        
        
        
        if collectionView.tag == 2{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
            
            
            
            
            let info = self.subCategories[0][indexPath.row]
            
            
            
            if let title = info.productName{
                
                cell.lblProductName.text = title
                
            }
            
            if info.is_special_offer_product ?? false {
                                                                   
                                        cell.offerTagView.isHidden = false
                                        }else{
                                        cell.offerTagView.isHidden = true
                                        }
          
            
            if let off = info.offerPercentage{
                
                if Double(off)! > 0.0{
                    
                    cell.lblOff.text = off + "% OFF"
                    cell.offView.isHidden = false
                    cell.lblPrice.isHidden = false
                    
                    if let price = info.groceryproductprice{
                        
                        let priceStr = mrp + currency + price
                        
                        cell.lblPrice.attributedText = priceStr.strikeThrough()
                        
                    }
                    
                    if let offerPrice = info.offerPrice{
                        
                        cell.lblOfferPrice.text = nrp + currency + offerPrice
                        
                    }
                    
                }else{
                    
                    
                    
                    if let price = info.groceryproductprice{
                        
                        cell.lblOfferPrice.text = nrp + currency + price
                    }
                    
                    cell.lblPrice.isHidden = true
                    cell.offView.isHidden = true
                    
                    if info.is_special_offer_product ?? false {
                                               
                    cell.offerTagView.isHidden = false
                    }else{
                    cell.offerTagView.isHidden = true
                    }
                    
                    
                    
                }
                
                
            }
            
            
            
            if let weight = info.weight {
                
                if let weightQty = info.qty_weight{
                    
                    let weightStr = weightQty + " " + weight
                    
                    cell.lblWeight.text = weightStr
                    
                    
                }
                
            }
            
            if let imgUrl = info.product_image1{
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            
            
            
            
            
            return cell
            
            
            
        }
        
        if collectionView.tag == 3{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeCollectionCell, for: indexPath) as! HomeCollectionCell
            
            
            
            
            let info = self.subCategories[1][indexPath.row]
            
            
            
            if let title = info.productName{
                
                cell.lblProductName.text = title
                
                
            }
            
            
            if let off = info.offerPercentage{
                
                if Double(off)! > 0.0{
                    
                    cell.lblOff.text = off + "% OFF"
                    cell.offView.isHidden = false
                    cell.lblPrice.isHidden = false
                   
                    if info.is_special_offer_product ?? false {
                                               
                    cell.offerTagView.isHidden = false
                    }else{
                    cell.offerTagView.isHidden = true
                    }
                    
                    if let price = info.groceryproductprice{
                        
                        let priceStr = mrp + currency + price
                        
                        cell.lblPrice.attributedText = priceStr.strikeThrough()
                        
                    }
                    
                    if let offerPrice = info.offerPrice{
                        
                        cell.lblOfferPrice.text = nrp + currency + offerPrice
                        
                    }
                    
                }else{
                    
                    if let price = info.groceryproductprice{
                        
                        cell.lblOfferPrice.text = nrp + currency + price
                    }
                    
                    cell.lblPrice.isHidden = true
                    cell.offView.isHidden = true
                    cell.offerTagView.isHidden = true
                }
                
                
            }
            
            
            
            
            if let weight = info.weight {
                
                if let weightQty = info.qty_weight{
                    
                    let weightStr = weightQty + " " + weight
                    
                    cell.lblWeight.text = weightStr
                    
                    
                }
                
            }
            
            if let imgUrl = info.product_image1{
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            
            
            
            
            
            return cell
            
            
            
        }
        
        
        let cell = UICollectionViewCell()
        
        return cell
        
        
        
        
    }
    
}



extension HomeViewController: FSPagerViewDelegate,FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.bannerData?.count ?? 0
    }
    
    
    
    
    
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        
        
        if let info = self.bannerData?[index] {
            
            if let subId = info.sub_category_id {
                
                if Int(subId) ?? 0 > 0{
                    
                    
                    let LoadCategoryScene = LoadFromCategoryViewController.instantiate(fromAppStoryboard: .Main)
                                           
                                                  if let navigator = self.navigationController {
                                                   
                                                     let groupID = subId
                                                       
                                                       if let title = info.sub_category{

                                                            LoadCategoryScene.category_Title = title
                                                       }

                                                      
                                                       LoadCategoryScene.groupId =  groupID
                                                       LoadCategoryScene.isBanner = true
                                                       navigator.pushViewController(LoadCategoryScene, animated: true)
                                                                                             
                                                  }
                    
            
                    
                }
                
                
            }
            
            if let productId = info.product_Id {
                
                if Int(productId) ?? 0 > 0{
                    
                    
                    
                             let productScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
                                                               
                                                                      if let navigator = self.navigationController {
                                                                       
                             
                                                                           productScene.bannerProductId = productId
                                                                           productScene.isBannerProduct = true
                                                                           navigator.pushViewController(productScene, animated: true)
                                                                                                                 
                                                                      }
                    
                    
                     print("prodid")
                }
                
                
            }

            
            
        }
        
        
        
    }
    
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    
    
    
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: bannerCell, at: index)
        
       
      
        if let info = self.bannerData?[index]{
            
            if let imgUrl = info.banner_image {
                
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                cell.imageView?.contentMode = .redraw
                cell.imageView?.kf.setImage(with: url, placeholder: phImage)
                
            }
            
            
        }
        
        return cell
        
        
    }
    
    
    
}
extension HomeViewController:HomeTableDelegate{
    
    func viewAllProducts(at index: Int) {
        
        if index == 0{
            
            
            let ViewAllScene = ProductListingViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                ViewAllScene.categoryTitle = "Nesto Deals"
                ViewAllScene.bannerUrl = offerProducts?.appofferURL
                
                navigator.pushViewController(ViewAllScene, animated: true)
                
            }
            
            
        }
        
        if index == 2{
            
            
            let ViewAllScene = ProductListingViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                
                if let title = self.subCategories[0][0].category{
                    
                    ViewAllScene.categoryTitle = title
                    ViewAllScene.index = 1
                    
                }
                
            
                
                ViewAllScene.bannerUrl = self.firstSubCategoryBanner!
                navigator.pushViewController(ViewAllScene, animated: true)
                
            }
            
            
        }
        
        if index == 3{
            
      
            
            let ViewAllScene = ProductListingViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
             
                
                if let title = self.subCategories[1][0].category{
                    
                    ViewAllScene.categoryTitle = title
                    ViewAllScene.index = 2
                    
                }
                
                
                ViewAllScene.bannerUrl = self.secondSubCategoryBanner!
                navigator.pushViewController(ViewAllScene, animated: true)
                
            }
            
            
        }
        
        
        
    }
    
}

extension HomeViewController:UITextFieldDelegate{
    
 
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        
        if self.txtSearch.text?.count ?? 0 > 2{
            
            let count = 10
            
            self.search(product:self.txtSearch.text!, limit: "\(count)")
            
        }
        return true
    }
    
    
    
    
    
    
    
}

