//
//  LoginViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 24/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var txtEmail: TextfiledWithImage!
    @IBOutlet weak var txtPassword: TextfiledWithImage!
    
    var fcmStr:String?
    var customerId:String?
    
    var newPasswordSet:Bool = false
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtEmail.delegate = self
    
        
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        
        
        let SignScene = SignUpViewController.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
           
            navigator.pushViewController(SignScene, animated: true)
            
            
            
        }
        
        
        
    }
    
    
    
    @IBAction func loginPressed(_ sender: Any) {
        
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
        
        
               
        if isConnected(){
            
           
                   
                   guard let textMobile = self.txtEmail.text, let textPwd = self.txtPassword.text,!textMobile.isEmpty,!textPwd.isEmpty else {
                      
                    self.showNotification(message: notValidCredentials)
                      
                       return
                   }
            
               self.doLogin(username: textMobile, password: textPwd)
            
            
            
            if textMobile.count < 10 {
                
                 self.showNotification(message: notValidPhone)
                
            }else{
                
                
                self.doLogin(username: textMobile, password: textPwd)
                
                
            }
            
            

            
            
        }
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
        
    }
    
    
    func registerFcm(userId:String,fcm:String){
        
        let params = FcmRegisterParam(fcm_regId: fcm, user_id: userId).Values
                             
                             ApiRequest.signInFCM(withParameter: params) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                                 
                                                
                                                return

                                                   
                                        
                                             } else{
                                               
                                               
                                              return
                                               
                               }
        }
        
    }
    
    func doLogin(username:String,password:String){
        
        
        self.STProgress.show()
               
        let params = LoginParameters(username: username, password: password).Values
                      
                      ApiRequest.goLogin(withParameter: params) { (isSuccess,message) in
                       
                        self.STProgress.dismiss()
                                      
                                      if isSuccess {
                                          
                                        if let data = self.ApiRequest.LoginResponse?.customer_details{
                                            
                                            
                                         
                                            
                                            
                                            if let customerId = data[0].id {
                                                
                                                if let token = data[0].Authorization{
                                                    
                                                    defualts.set(token,forKey:"token")
                                                }
                                                
                                                defualts.set(customerId, forKey: "customerId")
                                                
                                                if let fcm = defualts.string(forKey: "fcmToken"){
                                                    
                                                    self.registerFcm(userId: "\(customerId)", fcm: fcm)
                                                    
                                                }
                                                
                                             
                                            }
                                            
                                            if let address = data[0].adress {
                                                
                                                defualts.set(address, forKey: "address")
                                                
                                                
                                            }
                                            if let name = data[0].firstname{
                                                
                                                 defualts.set(name, forKey: "name")
                                                
                                                if let lastName = data[0].lastname{
                                                    
                                                     defualts.set(lastName, forKey: "lastName")
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                            if self.newPasswordSet{
                                                
                                                self.navigationController?.popToViewController(ofClass: HomeViewController.self)
                                            }else{
                                                
                                                self.backNavigation()
                                            }
                                            
                                            
                                            
                                            
                                           
                                            

                                            
                                            
                                        
                                        }
                                      }else{
                                        
                                        
                                        self.showAlert(message: message)
                                        
                        }
        
        }
    }
    
    
    @IBAction func forgotPressed(_ sender: Any) {
        
        
        let ForgotScene = ForgotPasswordViewController.instantiate(fromAppStoryboard: .Main)
               
               if let navigator = self.navigationController {
                  
                
                 //  ForgotScene.isfo = true
                   navigator.pushViewController(ForgotScene, animated: true)
                   
                   
                   
               }
        
        
        
    }
    

}

extension LoginViewController:UITextFieldDelegate{

func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    // get the current text, or use an empty string if that failed
    let currentText = textField.text ?? ""

    // attempt to read the range they are trying to change, or exit if we can't
    guard let stringRange = Range(range, in: currentText) else { return false }

    // add their new text to the existing text
    let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

    // make sure the result is under 16 characters
    return updatedText.count <= 10
}
}
