//
//  StoreLocateViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 23/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import CoreLocation
import SystemConfiguration
class StoreLocateViewController: BaseViewController {
    
    
    @IBOutlet weak var lblGovernete: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblStreet: UILabel!
    
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var pickerView: UIView!
    
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var locationPinView: CurvedView!
    
    
    @IBOutlet weak var lblCity: UILabel!
    
    
    var isState:Bool? = true
    var isArea:Bool? = false
    var isCity:Bool? = false
    
    var selectedStateId:Int = 0
    var selectedCityId:Int = 0
    var selectedStreetid:Int = 0
    
    
    var stateData:[stateData]?
    var areaData:[citiesData]?
    var streetData:[streetData]?
    
    
    var groceryId:String?
    var groceryTitle:String?
    var locationLat: Double = 0
    var locationLong: Double = 0
    
    private var locationManager:CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
        // Do any additional setup after loading the view.
    }
    
    func initialSetup(){
        
        
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
//
//        self.STProgress.show()
//
//        if isConnected(){
//
//            getUserLocation()
//
//        }
        
        getSelectedLocation(stateId:"1", cityId: "1", streetId:"1")
         
        self.locationView.isHidden = true    //location view is hidden location set to 1
        self.locationPinView.isHidden = true
        self.pickerView.isHidden = true
        
        
        
        
        
    }
    
    
    
    func getUserLocation() {
        
        
        if locationManager?.location != nil  {
            
            
            if self.locationLong != 0.0 && self.locationLat != 0.0{
                
                
                
                getNearestStore(long:"\(self.locationLong)",lat:"\(self.locationLat)")
                
                
                
                
            }else{
                
               // locationManager?.delegate = self
                
                if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways){
                    
                   
                    locationManager?.delegate = self
                    locationManager?.startUpdatingLocation()
           
                }else{
                    
                     locationManager?.requestAlwaysAuthorization()
                }
                //
                
            }
            
            
        }else{
            
           
            if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways){
              
                
                locationManager?.delegate = self
                locationManager?.startUpdatingLocation()
               
            }else{
                
                 locationManager?.requestAlwaysAuthorization()
            }
            
           
          
        }
        
        
        
    }
    
    
    
    
    @IBAction func changePressed(_ sender: Any) {
        
        self.locationView.isHidden = true
        
        
    }
    
    
    @IBAction func chooseGovernatePressed(_ sender: Any) {
        
        self.isState = true
        self.isArea = false
        self.isCity = false
        
        if self.stateData?.count ?? 0 > 0{
            
            self.pickerView.isHidden = false
            self.picker.delegate = self
            self.picker.dataSource = self
            
            
        }else{
            
            if isConnected(){
                
                getStates()
                
            }
            
            
        }
        
        
    }
    
    
    
    @IBAction func pickerDonePressed(_ sender: Any) {
        
        self.pickerView.isHidden = true
        
    }
    
    
    @IBAction func chooseAreaPressed(_ sender: Any) {
        
        self.pickerView.isHidden = true
        
        
        self.isState = false
        self.isArea = true
        self.isCity = false
        
        
        if self.areaData?.count ?? 0 > 0 {
            
            if self.selectedStateId > 0 {
                
                self.pickerView.isHidden = false
                self.picker.delegate = self
                self.picker.dataSource = self
                
                
            }
            
            
        }else{
            
            if self.selectedStateId > 0 {
                
                getArea(stateId: "\(self.selectedStateId)")
            }else{
                
                self.showAlert(message:selectGovernerate)
            }
            
            
            
        }
        
        
        
    }
    
    
    @IBAction func chooseStreetPressed(_ sender: Any) {
        
        self.pickerView.isHidden = true
        
        self.isState = false
        self.isArea = false
        self.isCity = true
        
        
        if self.streetData?.count ?? 0 > 0 {
            
            if self.selectedCityId > 0 {
                
                self.pickerView.isHidden = false
                self.picker.delegate = self
                self.picker.dataSource = self
                
                
            }
            
            
        }else{
            
            if self.selectedCityId > 0 {
                
                getStreet(cityId: "\(self.selectedCityId)")
            }else{
                
                self.showAlert(message:selectGovCity)
            }
            
            
            
        }
        
    }
    
    @IBAction func startShoppingPressed(_ sender: Any) {
        
        if isConnected(){
            
            if self.selectedStateId > 0 && self.selectedCityId > 0 && self.selectedStreetid > 0{
                
                getSelectedLocation(stateId:"\(self.selectedStateId)", cityId: "\(self.selectedCityId)", streetId:"\(self.selectedStreetid)")
            }else{
                
                self.showAlert(message:selectGovCity)
                
            }
            
            
            
            
        }
        
        
        
        
        
        
    }
    
    
    @IBAction func continuePressed(_ sender: Any) {
        
        if let groceryId = self.groceryId{
            
            if let groceryTitle = self.groceryTitle{
                
                
                let LocationScene = HomeViewController.instantiate(fromAppStoryboard: .Main)
                
                LocationScene.groceryTitle = groceryTitle
                LocationScene.groceryId = groceryId
                
                if let navigator = self.navigationController {
                    
                    navigator.pushViewController(LocationScene, animated: true)
                    
                }
                
            }
            
        }
        
     
    }
    
    func getNearestStore(long:String,lat:String){
        
        self.STProgress.show()
        
        let Params = NearestParams(lattitude:lat,longitude:long).Values
        
        ApiRequest.getNearestLocation(withParameter:Params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                
                if let info = self.ApiRequest.NearestLocationResponse{
                    
                    if info.arrStore?.count ?? 0 > 0 {
                        
                        if let data = info.arrStore?[0]{
                            
                            
                            if let phone = data.phone_1{
                                                   
                            defualts.set(phone,forKey: "groceryPhone")
                                                   
                                               }
                            
                            if let groceryId = data.storeId{
                                
                                defualts.set(groceryId,forKey: "groceryId")
                            }
                            if let name = data.store_name{
                                
                                defualts.set(name,forKey: "groceryName")
                                
                            }
                            if let cityId = data.cityId{
                                
                                if let city = data.city {
                                    
                                    defualts.set(cityId,forKey: "cityId")
                                    defualts.set(city,forKey: "city")
                                    self.lblCity.text = city
                                    
                                }
                                
                                
                                
                                
                            }
                            
                            if let shopLat = data.latitude{
                                defualts.set(shopLat,forKey: "shopLat")
                                
                            }
                            
                            if let shopLong = data.longitude{
                                
                                defualts.set(shopLong,forKey: "shopLong")
                            }
                            
                            
                            
                            self.groceryId = "\(data.storeId ?? "2")"
                            
                            if let groceryTitle = data.store_name{
                                
                                self.groceryTitle = groceryTitle
                            }
                            
                            
                        }
                        
                        self.locationPinView.isHidden = false
                        
                        
                    }
                    
 
                    
                }
                
                
            }else{
                
                self.locationView.isHidden = true
                
        
            }
            
        }
        
        
    }
    
    
    func getSelectedLocation(stateId:String,cityId:String,streetId:String){
        
        self.STProgress.show()
        
        let locationParam = LocationParameter(stateId:stateId,cityId: cityId,streetId:streetId).Values
        
        ApiRequest.getSearchLocation(withParameter:locationParam) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.LocationResponse{
                    
                    let data = info.arrGrocery[0]
                    
                    
                    if let phone = data.phone_1{
                        
                         defualts.set(phone,forKey: "groceryPhone")
                        
                    }
                    
                    if let groceryId = data.id{
                        
                        defualts.set(groceryId,forKey: "groceryId")
                        
                        if let groceryTitle = data.store_name{
                            
                            self.groceryTitle = groceryTitle
                        }
                    }
                    if let name = data.store_name{
                        
                        defualts.set(name,forKey: "groceryName")
                        
                    }
                    if let cityId = data.cityId{
                        
                        if let city = data.city {
                            
                            defualts.set(cityId,forKey: "cityId")
                            defualts.set(city,forKey: "city")
                            self.lblCity.text = city
                            
                        }
                        
                        
                        
                        
                    }
                    
                    if let shopLat = data.latitude{
                        defualts.set(shopLat,forKey: "shopLat")
                        
                    }
                    
                    if let shopLong = data.longitude{
                        
                        defualts.set(shopLong,forKey: "shopLong")
                    }
                    
                    
                    
                    self.groceryId = "\(data.id ?? 2)"
                    
                    
                    
                    
                    
                    
                    if info.arrGrocery.count > 0 {
                        
                        let LocationScene = HomeViewController.instantiate(fromAppStoryboard: .Main)
                        
                       
                        
                        if let navigator = self.navigationController {
                            
                            
                            
                            navigator.pushViewController(LocationScene, animated: true)
                            
                            
                            
                        }
                        
                    }
                    
                    
                }
                
                
            }else{
                
                
                
                let LocationScene = HomeViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    navigator.pushViewController(LocationScene, animated: true)
                    
                    
                    
                }
            }
            
        }
        
        
    }
    
    
    
    
    
    
    func getStates(){
        
        
        self.STProgress.show()
        
        
        
        ApiRequest.getStates(withParameter: [:]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.StateData{
                    
                    self.stateData = info
                    
                    if self.stateData?.count ?? 0 > 0{
                        
                        DispatchQueue.main.async {
                            
                            if let info = self.stateData?[0]{
                                
                                self.lblGovernete.text = info.state
                                self.selectedStateId = Int(info.id) ?? 0
                                
                            }
                            
                            
                            self.picker.delegate = self
                            self.pickerView.isHidden = false
                            
                            
                        }
                        
                        
                        
                    }
                    
                    
                    
                }
                
                
            }
            
        }
        
        
        
        
    }
    
    
    
    func getArea(stateId:String){
        
        
        self.STProgress.show()
        
        
        
        ApiRequest.getAreas(withParameter: ["stateId":stateId]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.AreaData{
                    
                    self.areaData = info
                    
                    
                    if self.areaData?.count ?? 0 > 0{
                        
                        DispatchQueue.main.async {
                            
                            if let info = self.areaData?[0]{
                                
                                self.lblArea.text = info.city
                                self.selectedCityId = Int(info.id) ?? 0
                                
                            }
                            
                            
                            self.picker.delegate = self
                            self.pickerView.isHidden = false
                            
                            
                        }
                        
                        
                        
                    }
                    
                }
                
            }
            
        }
        
        
        
        
    }
    
    func retriveCurrentLocation(){
        let status = CLLocationManager.authorizationStatus()
        
        if(status == .denied || status == .restricted || !CLLocationManager.locationServicesEnabled()){
            // show alert to user telling them they need to allow location data to use some feature of your app
            return
        }
        
        // if haven't show location permission dialog before, show it to user
        if(status == .notDetermined){
            self.locationManager?.requestWhenInUseAuthorization()
            
            // if you want the app to retrieve location data even in background, use requestAlwaysAuthorization
            // locationManager.requestAlwaysAuthorization()
            return
        }
        
        // at this point the authorization status is authorized
        // request location data once
        self.locationManager?.requestLocation()
        
        // start monitoring location data and get notified whenever there is change in location data / every few seconds, until stopUpdatingLocation() is called
        // locationManager.startUpdatingLocation()
    }
    
    
    func getStreet(cityId:String){
        
        
        self.STProgress.show()
        
        
        
        ApiRequest.getCityStreet(withParameter: ["cityId":cityId]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.StreetData{
                    
                    self.streetData = info
                    
                    if self.streetData?.count ?? 0 > 0{
                        
                        DispatchQueue.main.async {
                            
                            if let info = self.streetData?[0]{
                                
                                self.lblStreet.text = info.street
                                self.selectedStreetid = Int(info.id) ?? 0
                                self.picker.delegate = self
                                self.pickerView.isHidden = false
                                
                            }
                            
                            
                            
                            
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            }
            
        }
        
        
        
        
    }
    
    
}
extension StoreLocateViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        
        return 1
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if self.isState!{
            
            return self.stateData?.count ?? 0
            
        }
        if self.isArea!{
            
            
            return self.areaData?.count ?? 0
            
        }
        
        if self.isCity!{
            
            
            return self.streetData?.count ?? 0
            
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.isState!{
            
            if let info = self.stateData?[row]{
                
                if let state = info.state{
                    
                    return state
                    
                }
                
                
            }
            
            
        }
        
        
        if self.isArea!{
            
            if let info = self.areaData?[row]{
                
                if let city = info.city{
                    
                    return city
                    
                }
                
                
            }
            
            
        }
        
        
        if self.isCity!{
            
            if let info = self.streetData?[row]{
                
                if let street = info.street{
                    
                    return street
                    
                }
                
                
            }
            
            
        }
        
        return ""
        
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if self.isState!{
            
            if let info = self.stateData?[row]{
                
                if let state = info.state{
                    
                    self.selectedStateId = Int(info.id) ?? 0
                    self.lblGovernete.text = state
                    
                }
                
                
            }
            
        }
        
        if self.isArea!{
            
            if let info = self.areaData?[row]{
                
                if let city = info.city{
                    
                    self.selectedCityId = Int(info.id) ?? 0
                    self.lblArea.text = city
                    
                }
                
                
            }
            
        }
        
        
        if self.isCity!{
            
            if let info = self.streetData?[row]{
                
                if let street = info.street{
                    
                    self.selectedStreetid = Int(info.id) ?? 0
                    self.lblStreet.text = street
                    
                }
                
                
            }
            
        }
        
        
    }
    
    
    
    
}

extension StoreLocateViewController:CLLocationManagerDelegate{
    

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        
 
         
         
     
                              if let location = locations.last {
                      
                                  if location != nil{
                      
                      
                                      self.locationLat = location.coordinate.latitude
                                      self.locationLong = location.coordinate.longitude
                                    
                                    
                                    if isConnected(){
                                              
                                              getNearestStore(long:"\(self.locationLong)",lat:"\(self.locationLat)")

                                    }else{
                                        
                                        self.STProgress.dismiss()
                                        self.locationView.isHidden = true
                                        
                                    }
                      
                      
                                      
                      
                      
                      
                                  }
                      
                              }else{
                                
                                self.STProgress.dismiss()
                                self.locationView.isHidden = true
                                
        }
             
        
                              self.locationManager?.stopUpdatingLocation()
                              self.locationManager?.delegate = nil
                             
      

      }
    
    
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.STProgress.dismiss()
        self.locationView.isHidden = true
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("location manager authorization status changed")
        
        switch status {
        case .authorizedAlways:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
           // retriveCurrentLocation()
            print("user allow app to get location data when app is active or in background")
        case .authorizedWhenInUse:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
            print("user allow app to get location data only when app is active")
        case .denied:
            
             locationManager?.requestAlwaysAuthorization()
             self.STProgress.dismiss()
             self.locationView.isHidden = true
            print("user tap 'disallow' on the permission dialog, cant get location data")
        case .restricted:
            
            self.STProgress.dismiss()
            self.locationView.isHidden = true
            print("parental control setting disallow location data")
        case .notDetermined:
            
             self.STProgress.dismiss()
             self.locationView.isHidden = true
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            break
        }
    }
    
    
    
    
    
}
