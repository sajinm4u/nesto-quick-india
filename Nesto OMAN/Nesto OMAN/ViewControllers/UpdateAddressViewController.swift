//
//  UpdateAddressViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 01/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import GoogleMaps


class UpdateAddressViewController: BaseViewController {
    
    
    @IBOutlet weak var txtNme: UITextField!
    @IBOutlet weak var txtBuilding: UITextField!
    @IBOutlet weak var txtRoomNo: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var mapContainerView: UIView!
    
    var addNewsAddress:Bool? = true
  
    
    var mapView: GMSMapView!
  //  var Nesto: GMSMarker?
    var UserMarker: GMSMarker?

    var NestoView: UIImageView?
    var address:DeliveryAddressData?

    var currentLat:Double = 0.0
    var currentlong:Double = 0.0
    var markerLong:Double = 0.0
    var markerLat:Double = 0.0
    
    var addressTitle:String? = "Home"
    var customerId:String?
    var isEdit:Bool = false
    var isHome:Bool? = true
    
    var flag:String? = "1"
    var isMarkerDragged:Bool = false
    var addressId:String?
    var deliverableArea:Double = 10.0
    var setCircle:Int = 10000
    var camera = GMSCameraPosition()
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgOffice: UIImageView!
    @IBOutlet weak var mapContainerHeight: NSLayoutConstraint!
    
    let marker = GMSMarker()
    
    var locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
    
    func initialSetup(){
        
        if defualts.string(forKey: "deliverableArea") != nil {
            
            if let area = defualts.string(forKey: "deliverableArea"){
                
                
                
                 self.deliverableArea = Double(area)!
                
                self.setCircle =  Int(self.deliverableArea * 1000)
                
              
                
            }
            
        }
  
        
        self.txtNme.delegate = self
        self.txtRoomNo.delegate = self
        self.txtBuilding.delegate = self
        self.txtMobile.delegate = self
        
        if let flag = self.flag{
            
            self.flag = flag
            
        }
        

        
        guard let custid = defualts.string(forKey: "customerId") else{
            
            return
        }
        
        
        if let shopLat = defualts.string(forKey: "shopLat"){
            
            self.currentLat = Double(shopLat)!
            
            
        }
        if let shopLang = defualts.string(forKey: "shopLong"){
            
            self.currentlong = Double(shopLang)!
        }
            
        
        
      
        
        self.customerId = custid
        
        if let add = self.address{
            
            if let name = add.customer{
                
                if let addressId = add.address_Id{
                    
                    self.addressId = addressId
                }
                
                if let area = add.customer_area{
                    
                    self.txtArea.text = area
                }
                
                
                
                self.txtNme.text = name.capitalizingFirstLetter()
                self.txtRoomNo.text = add.land_mark?.capitalizingFirstLetter()
                self.txtBuilding.text = add.house_name?.capitalizingFirstLetter()
                self.txtMobile.text = add.customer_mobile
             
                if let long = add.longitude {
                    
                    self.markerLong = Double(long) ?? 0.0
                    
                    if let lat = add.latitude {
                        
                        self.markerLat = Double(lat) ?? 0.0
                        
                    }
                    
                }
                
                
            }
            
            
        }
        
        

        
        self.mapContainerHeight.constant =
            (UIScreen.main.bounds.height/2) + 100
        
        camera = GMSCameraPosition.camera(withLatitude:self.currentLat,
                                              longitude: self.currentlong,
                                                 zoom: 12)
        self.mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.mapContainerHeight.constant), camera: camera)
  
        
        
        
       
             
        if (self.markerLat > 0.0) && (self.markerLong > 0.0) {  // user marker
            
           
            
            
            marker.position = CLLocationCoordinate2D(latitude: self.markerLat , longitude: self.markerLong)
            marker.isDraggable = true
            marker.map = mapView
            reverseGeocoding(marker: marker)
            
            
            
        }else{
            
            

            if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways){
                
                
                locationManager.startUpdatingLocation()
                locationManager.delegate = self
                
                
                
               // locationManager?.requestLocation()
            }
            
           
            
        }
        
        
            
           
        

        self.mapContainerView.addSubview(mapView)

         mapView.delegate = self

        let house = UIImage(named: "nestoMarker")!
        let markerView = UIImageView(image: house)
        NestoView = markerView

        let position = CLLocationCoordinate2D(latitude: self.currentLat, longitude: self.currentlong)
        let NestoMarker = GMSMarker(position: position)
        NestoMarker.iconView = markerView
        NestoMarker.tracksViewChanges = false
        NestoMarker.map = mapView
        
       // Nesto = marker
        
        
  
        
        
        let circleCenter = CLLocationCoordinate2D(latitude: self.currentLat, longitude: self.currentlong)
        let circ = GMSCircle(position: circleCenter, radius: CLLocationDistance(self.setCircle))

        circ.fillColor = UIColor(red:161/255, green: 242/255, blue: 124/255, alpha:0.5)
        circ.map = mapView
        circ.strokeColor = UIColor.green
        
        
       // self.mapContainerView.addSubview(mapView)
        
        
    }
    
    
    // MARK: - reverse geo
    
    func reverseGeocoding(marker: GMSMarker) {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
        
        var currentAddress = String()
        
//                    let camera = GMSCameraPosition.camera(withLatitude:Double(marker.position.latitude),
//                                                                longitude:Double(marker.position.longitude),
//                                                                   zoom: 10)
//                          self.mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.mapContainerHeight.constant), camera: camera)
//
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                                
                currentAddress = lines.joined(separator: "\n")
               
            }
           
            let firsLocation = CLLocation(latitude: self.currentLat, longitude: self.currentlong)
            let secondLocation = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
            let distance = firsLocation.distance(from: secondLocation) / 1000
            
            if self.isMarkerDragged {
          
                if distance > self.deliverableArea{
                
                self.lblLocation.text = "Sorry!, We won't be able to deliver here"
                self.lblLocation.textColor = UIColor.red
            }else{
                
                self.lblLocation.textColor = UIColor.black
                 self.lblLocation.text = currentAddress
                
            }
            
            }
           
            

        }
    }
    
    
    
    func updateAddress(name:String,landMark:String,houseName:String,mobile:String,addressTitle:String,area:String,customerId:String,long:String,lat:String,cityId:String){
        
        
        self.STProgress.show()
        
        
        var param = [String:String]()
        var urlStr = Api.addAddress
        
        if !self.isEdit {
            
            param = AddressParams(customerId:customerId, customer_name: name.capitalizingFirstLetter(), house_name: houseName.capitalizingFirstLetter(), land_mark: landMark.capitalizingFirstLetter(), customer_area: area.capitalizingFirstLetter(), latitude: lat, longitude: long, customer_city:cityId, customer_mobile: mobile, addressTitle: self.addressTitle!).Values
            
            
        }else{
            
            
            guard let addressId = self.addressId else{
                
                return
                
            }
            
            urlStr = Api.updateAddress
            
            param = UpdateAddressParams(addressId:addressId, customerId:customerId , customer_name: name.capitalizingFirstLetter(), house_name: houseName.capitalizingFirstLetter(), land_mark: landMark.capitalizingFirstLetter(), customer_area: area.capitalizingFirstLetter(), latitude: lat, longitude: long, customer_city:cityId, customer_mobile: mobile, addressTitle: self.addressTitle!).Values
            
        }
        
        
       
        

        self.ApiRequest.updateAddressData(url:urlStr,withParameter:param) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                
               
                
                DispatchQueue.main.async {
                    
                     self.showAlert(message: "Address Updated Successfully")
                    
                }
                self.backNavigation()
                 
                
            }else{
                
                self.showAlert(message: message)
                
            }
        }
        
    }
    
    
    
    
    @IBAction func saveAddressPressed(_ sender: Any) {
        
        if isConnected(){
            
            guard let name = self.txtNme.text , let roomNo = self.txtRoomNo.text, let building = self.txtBuilding.text, let mobile = self.txtMobile.text, !name.isEmpty, !roomNo.isEmpty, !building.isEmpty, !mobile.isEmpty else{
                
                showAlert(message: fillAllFields)
                return
            }
            
            if self.currentLat > 0.0 && self.currentlong > 0.0{
                
                guard let cityId = defualts.string(forKey: "cityId") else {
                    
                    return
                }
                
                self.updateAddress(name: name, landMark: roomNo, houseName: building, mobile: mobile, addressTitle: self.addressTitle!, area:self.txtArea.text ?? null, customerId: self.customerId!, long:  "\(self.markerLong)", lat: "\(self.markerLat)", cityId: cityId)
                
            }
                          
            
            
            
        }
        
        
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    @IBAction func homePressed(_ sender: Any) {
    
               
               self.imgOffice.image = UIImage(named: "ic_radio")
               self.imgHome.image = UIImage(named: "ic_radio_sel")
               self.isHome = true
         

    }
    
    @IBAction func officePressed(_ sender: Any) {
     
                  self.imgOffice.image = UIImage(named: "ic_radio_sel")
                  self.imgHome.image = UIImage(named: "ic_radio")
                  self.isHome = false
           
        
    }
    

}
extension UpdateAddressViewController:GMSMapViewDelegate{
    
    
 func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    mapView.isMyLocationEnabled = addNewsAddress!
 }
 
 func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {

  
//  if (gesture) {
//   mapView.selectedMarker = marker
//  }
 }
 
 func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
  mapView.isMyLocationEnabled = addNewsAddress!
  return false
 }
 
 func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
  print("COORDINATE \(coordinate)") // when you tapped coordinate
 }
 
 func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
    
    
//  mapView.isMyLocationEnabled = true
//  mapView.selectedMarker = nil
    
    
  return false
 }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        
        self.isMarkerDragged = true
       
       
           }
       
       func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
           
           
       }
       
       func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
      
        
          reverseGeocoding(marker: marker)
         
           //self.polyline.map = nil;
           print("marker dragged to location: \(marker.position.latitude),\(marker.position.longitude)")
          let locationMobi = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
           
        self.markerLat = marker.position.latitude
        self.markerLong = marker.position.longitude

       }

    
}
extension UpdateAddressViewController:CLLocationManagerDelegate
{
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location = locations.last

        if let location = location{
            
                      marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                      marker.isDraggable = true
                      marker.map = mapView
                      reverseGeocoding(marker: marker)
                      //self.mapView.delegate = self
            

            
           // let locationMobi = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                
            self.locationManager.delegate = nil
        }
       
        
        
      //  marker.position = CLLocationCoordinate2D(latitude: location?.coordinate.latitude ?? self.currentLat, longitude: location?.coordinate.longitude ?? self.currentlong )

        self.marker.map = mapView
         
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()

    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("location manager authorization status changed")
        
        switch status {
        case .authorizedAlways:
            
            locationManager.startUpdatingLocation()
            locationManager.delegate = self
            
           // retriveCurrentLocation()
            print("user allow app to get location data when app is active or in background")
        case .authorizedWhenInUse:
            print("user allow app to get location data only when app is active")
        case .denied:
            
            locationManager.requestAlwaysAuthorization()
            print("user tap 'disallow' on the permission dialog, cant get location data")
        case .restricted:
            print("parental control setting disallow location data")
        case .notDetermined:
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            break
        }
    }
    
    
    
    
}

extension UpdateAddressViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.txtMobile{
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // make sure the result is under 16 characters
        return updatedText.count <= 10
        }
        return true
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
    
    
    
    
}
