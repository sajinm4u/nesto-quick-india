//
//  SubMainCategoryViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 27/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import BadgeSwift

class SubMainCategoryViewController: BaseViewController {
    
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var lblBadge: BadgeSwift!
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var searchTableView: UITableView!
    var mainCataID:String?
    var categoryTitle:String?
    
    var searchFullArray = [CartProducts]()
    var totalSearchCount:String?
    
    var CategoryData:[subMainData]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()

    }
    
    func initialSetup(){
        
        self.searchView.isHidden = true
        self.txtSearch.delegate = self
        
        if let categoryTitle = self.categoryTitle{
            
            lblCategoryTitle.text = categoryTitle
        }
        
        
        self.categoryCollectionView.isHidden = true
        if isConnected(){
            
            if let id = self.mainCataID{
                
                 getCategory(catId:id)
                
            }
            
            
           
            
        }
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let custId = defualts.string(forKey: "customerId"){
        
        if let count = defualts.string(forKey: "badge"){
            
            if count != "0"{
                
                self.lblBadge.isHidden = false
                    self.lblBadge.text = count
                
            }else{
                 self.lblBadge.isHidden = true
                
            }
            
           
         }
        }else{
            
             self.lblBadge.isHidden = true
            
        }
        
    }
    
    @IBAction func cartPressed(_ sender: Any) {
        
        
        if let customerId = defualts.string(forKey: "customerId"){
            
            
            let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
                 
                 if let navigator = self.navigationController {
                     
                     navigator.pushViewController(CartScene, animated: true)
                 }
                 
            
            
            
        }else{
            
          
            
            
            self.STAlert.alert(title:"", message: pleaseLogin )
                        .action(.destructive("Login")){
                            
                            let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                            
                            if let navigator = self.navigationController {
                                
                                navigator.pushViewController(loginScene, animated: true)
                                
                                
                                
                            }
                            
                            
                    }.action(.cancel("Cancel"))
                        .show(on: self)
                    
            
            
        }
        
        
        
    }
    
    
    
    
    @IBAction func searchCancelPressed(_ sender: Any) {
           
           self.searchView.isHidden = true
       }
    
    
    
    
    
    func search(product:String,limit:String){
        
        
        guard let id = defualts.string(forKey: "groceryId") else{
            
            
            return
        }
        
        
        self.STProgress.show()
        
        
        let params = SearchParam(groceryId: id, searchKey: product, limit: limit).Values
        
        ApiRequest.searchItem(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                if let info = self.ApiRequest.SearchResponse?.products{
                    
                    
                    
                    if info.count > 0{
                        
                        self.totalSearchCount = self.ApiRequest.SearchResponse?.totalCount
                        
                        self.searchFullArray = info
                        self.searchTableView.delegate = self
                        self.searchTableView.dataSource = self
                        self.searchTableView.reloadData()
                        self.searchView.isHidden = false
                        
                        
                    }
                    
                    
                    
                }
                
                
                
                
            }
            
        }
        
        
    }
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    func getCategory(catId:String){
    
            
             self.STProgress.show()
            
            let cartParam = SubMainParam(mainCatId:catId).Values
            
            self.ApiRequest.getSubMainCategory(withParameter:cartParam) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                             
                                                if let info = self.ApiRequest.SubMainCategoryResponse{
                                                    
                                                    self.CategoryData = info.arrCategoryGroups
                                                    
                                                    if let imgUrl = info.mainCategoryBanner{
                                                                                             
                                                                                             let url = URL(string: imgUrl)
                                                                                              let phImage = UIImage(named: ph)
                                                                                             
                                                        self.imgBanner?.kf.setImage(with: url, placeholder: phImage)
                                                                                             
                                                                                             
                                                                                         }
                                                               
                                                    
                                                    
                                                    if self.CategoryData?.count ?? 0 > 0 {
                                                        
                                                  
                                                        self.categoryCollectionView.delegate = self
                                                        self.categoryCollectionView.dataSource = self
                                                        self.categoryCollectionView.reloadData()
                                                        self.categoryCollectionView.isHidden = false
                                                       
                                                    }
                                                                            
                                                    
                                                }
                                            
                                              
                                             }else{
                                                                                                                      
                                                self.showAlert(message: wentWrong)
                                                                                                                                                 
                                                                                                                                                    }
                }
               
               }


}
extension SubMainCategoryViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.CategoryData?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.categoryCollectionView.dequeueReusableCell(withReuseIdentifier: subCatCell, for: indexPath) as! SubMainCollectionCell
        
        if let info = self.CategoryData?[indexPath.row]{
            
            cell.lblTitle.text = info.groupName
           
            
            if let imgUrl = info.groupImage{
                                          
                                          let url = URL(string: imgUrl)
                                           let phImage = UIImage(named: ph)
                                          
                                           cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                                          
                                          
                                      }
            
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let info = self.CategoryData?[indexPath.row]{
                       
                       
                       let LoadCategoryScene = LoadFromCategoryViewController.instantiate(fromAppStoryboard: .Main)
                        
                               if let navigator = self.navigationController {
                                
                                if let groupID = info.groupId{
                                    
                                    if let title = info.groupName{
                                        
                                         LoadCategoryScene.category_Title = info.groupName
                                    }
                                    
                                   
                                    LoadCategoryScene.groupId =  groupID
                                    LoadCategoryScene.flagKey = "1"
                                    navigator.pushViewController(LoadCategoryScene, animated: true)
                                    
                                }
                                                                                                               
                               
                                                                                                                                               
                               }
                 
                     }
                   
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           
          
           
           let width = (self.view.frame.size.width - 6 * 2) / 2
           let height = width / 2 //ratio
           return CGSize(width: width, height: height)
           
       
}

}

extension SubMainCategoryViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if tableView == searchTableView{
            
            self.searchView.isHidden = true
            self.txtSearch.text = null
            
            let info = searchFullArray[indexPath.row]
            
            


            let ProductDetailsScene = ProductDetailViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {


                ProductDetailsScene.searchData = info
                ProductDetailsScene.isSearch = true
                ProductDetailsScene.similarProducts = searchFullArray
                navigator.pushViewController(ProductDetailsScene, animated: true)



            }
               
               
            
        }
        

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         
         
         if tableView == searchTableView{
             
             return self.searchFullArray.count
             
         }
         else{
             
             return 0
         }

     }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if tableView == searchTableView{
            
            
            let cell = searchTableView.dequeueReusableCell(withIdentifier: searchCell, for: indexPath) as! SearchCell
            
            
            
            let info = self.searchFullArray[indexPath.row]
            
            cell.lblTitle.text = info.product_name
            
            if let imgUrl = info.product_image1{
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named: ph)
                
                cell.itemPic?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            if (indexPath.row == self.searchFullArray.count - 1) {
                var count = 10
                let totalCount = Int(self.totalSearchCount ?? "10")!
                if self.searchFullArray.count < totalCount {
                    count = self.searchFullArray.count + 10
                    self.search(product: self.txtSearch.text!, limit: "\(count)")
                    
                }
            }
            
            return cell

            
        }
        
        let cell = UITableViewCell()
        return cell
    
      }
}

extension SubMainCategoryViewController:UITextFieldDelegate{
    
 
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        
        if self.txtSearch.text?.count ?? 0 > 2{
            
            let count = 10
            
            self.search(product:self.txtSearch.text!, limit: "\(count)")
            
        }
        return true
    }
    
    
    
}
