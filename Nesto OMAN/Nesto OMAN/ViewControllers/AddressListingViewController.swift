//
//  AddressListingViewController.swift
//  Nesto OMAN
//
//  Created by Sajin M on 01/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit

//protocol AddressSelectDelegate {
//
//    func selectedAddress(address:DeliveryAddressData)
//}

class AddressListingViewController: BaseViewController {
    
    @IBOutlet weak var addAddressView: CurvedView!
    
    @IBOutlet weak var addressList: UITableView!
    
    var addressData:[DeliveryAddressData]?
   // var delegate:AddressSelectDelegate?
    
    var customerId:String?
    var groceryId:String?
   // var addressFlag:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        initialSetup()
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func addNewAddress(_ sender: Any) {
        
                let AddressScene = UpdateAddressViewController.instantiate(fromAppStoryboard: .Main)
        
                    if let navigator = self.navigationController {
        
                      AddressScene.isEdit = false
        
                    navigator.pushViewController(AddressScene, animated: true)
        
        
        
                }
                
        
    }
    
    func initialSetup(){
        
        
        
          self.addressList.tableFooterView = UIView(frame:.zero)
        
//            self.addressList.delegate = self
//            self.addressList.dataSource = self
//            self.addressList.reloadData()
        self.addressList.isHidden = true
//
//        }
//
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        getAddressList()
    }
    
    
    func deleteAddress(addressId:String){
        
        guard  let customerId = defualts.string(forKey: "customerId") else {
            return
        }
            
            self.STProgress.show()
            
            let Param = DeleteParam(customerId:customerId,addressId: addressId).Values
            
            self.ApiRequest.deleteUserAddress(withParameter:Param) { (isSuccess,message) in
                
                self.STProgress.dismiss()
                
                if isSuccess {
                    
                 
                    self.getAddressList()
                     
                    
                }else{
                    
                    self.showAlert(message: message)
                    
                }
            }
            
        }
        
        
    func getAddressList(){
    
        
            
             self.STProgress.show()
        
        guard let custId = defualts.string(forKey: "customerId") else{
            
            return
            
        }
        
        guard let grocId = defualts.string(forKey: "groceryId") else{
                  
                  return
                  
              }
        
        
            
            let cartParam = CartParams(customerId:custId,groceryId:grocId).Values
            
            self.ApiRequest.getAddressList(withParameter:cartParam) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                             

                                                self.addressData = self.ApiRequest.DeliveryAddressResponse?.arrAdress
                                                
                                                if self.addressData?.count ?? 0 > 0 {
                                                    
                                                    if self.addressData?.count == 1{
                                                        
                                                        guard let info = self.addressData?[0] else { return }
                                                        
                                                        
                                                         NotificationCenter.default.post(name: Notification.Name(addressSelectNotification), object: nil,userInfo:["address":info])
                                                    }
                                             

                                                    
                                                    DispatchQueue.main.async {
                                                        
                                                        
                                                        
                                                        
                                                        self.addressList.dataSource = self
                                                        self.addressList.delegate = self
                                                        self.addressList.reloadData()
                                                        self.addressList.isHidden = false
                                                      //   self.addAddressView.isHidden = false
                                                    
                                                    }
                                                    
                                                    
                                                    
                                                }else{
                                                    
                                                    let info:[DeliveryAddressData]? = nil
                                                    
                                                    NotificationCenter.default.post(name: Notification.Name(addressSelectNotification), object: nil,userInfo:["address":info!])
                                                    
                                                    
                                                }
                                              
                                             }else{
                                                self.addressData?.removeAll()
                                                DispatchQueue.main.async {
                                                    
                                                    self.addressList.reloadData()
                                                    
                                                }
                                                
                                                 
                                                // self.addAddressView.isHidden = true
                                               // self.showAlert(message: message)
                                                                                                                                                 
                                    }
    
               
               }
        
    }
    

    @IBAction func backPressed(_ sender: Any) {
        
        if addressData?.count == 0 || addressData == nil{
            
             NotificationCenter.default.post(name: Notification.Name(addressEmptyNotification), object: nil)
        }
        
        
        
        self.backNavigation()
    }
    

}
extension AddressListingViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.addressData?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.addressList.dequeueReusableCell(withIdentifier: addressCell, for: indexPath) as! AddressTableCell
        
        
        let info = self.addressData?[indexPath.row]
        
        cell.lblName.text = info?.customer
        cell.addressOne.text = info?.house_name
        if let landmark = info?.land_mark {
            
            if let city = info?.customer_City {
            
             cell.addressThree.text = landmark + landMark + ", " + city
                
            }
        }
        
       
        
        if let area = info?.customer_area{
            
                
                 cell.addressTwo.text = area
     
        }
       
        if let contact = info?.customer_mobile{
            
            cell.addressFour.text = "Mobile: " + contact
            
        }
        
        

        
        
        cell.cellIndex = indexPath.row
        cell.delegate = self
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let info = self.addressData?[indexPath.row] else { return }
        
        //delegate?.selectedAddress(address: info)
        
        NotificationCenter.default.post(name: Notification.Name(addressSelectNotification), object: nil,userInfo:["address":info])
        
        
        backNavigation()
        
//        let AddressScene = DeliveryDetailsViewController.instantiate(fromAppStoryboard: .Main)
//
//                              if let navigator = self.navigationController {
//
//                                AddressScene.selectedAddress = info
//                              navigator.pushViewController(AddressScene, animated: true)
//
//              }
        
        
        
       

     
    }
 
}

extension AddressListingViewController:AddressTableDelegate{
    
    
    func EditAddress(at index: Int) {
        
        
        let AddressScene = UpdateAddressViewController.instantiate(fromAppStoryboard: .Main)
            
                        if let navigator = self.navigationController {
                            

                        
                            AddressScene.address = addressData?[index]
                            AddressScene.isEdit = true
                            
            
                        navigator.pushViewController(AddressScene, animated: true)
        
        }
    }
    
    func RemoveAddress(at index: Int) {
        
        guard let id = self.addressData?[index].address_Id else {return}
   
          self.deleteAddress(addressId: id)
           
            
     
    }
    
    
    
    
}
