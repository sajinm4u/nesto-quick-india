//
//  AttributedString.swift
//  Nesto Quick
//
//  Created by Sajin M on 20/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {

func highlightTarget(target: String, color: UIColor) -> NSMutableAttributedString {
    let targetValue = String(target.dropFirst().dropLast())
    let regPattern = "\\[\(targetValue)\\]"
    if let regex = try? NSRegularExpression(pattern: regPattern, options: []) {
        let matchesArray = regex.matches(in: self.string, options: [], range: NSRange(location: 0, length: self.length))
        for match in matchesArray {
            let attributedText = NSMutableAttributedString(string: targetValue)
            attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(location: 0, length: attributedText.length))
            self.replaceCharacters(in: match.range, with: attributedText)
        }
    }
    return self
}
}

