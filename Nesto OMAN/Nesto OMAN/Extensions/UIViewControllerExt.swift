//
//  UIViewControllerExt.swift
//  7TICKETS
//
//  Created by Sajin M on 08/05/2020.
//  Copyright © 2020 Netstager. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{

    func dismissKey()
    {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer( target: self, action: #selector(UIViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard()
    {
    view.endEditing(true)
    }
    
    
    func backNavigation() {
        self.navigationController?.popViewController(animated:true)
    }
    
}
