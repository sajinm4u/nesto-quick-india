//
//  FlowLayoutClass.swift
//  Nesto OMAN
//
//  Created by Sajin M on 08/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation
import UIKit


class FlowLayoutClass:UICollectionViewFlowLayout {
override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {

    let attributes = super.layoutAttributesForElements(in: rect)

    var leftMargin = sectionInset.left
    var maxY: CGFloat = 2.0

    let horizontalSpacing:CGFloat = 5

    attributes?.forEach { layoutAttribute in
        if layoutAttribute.frame.origin.y >= maxY
            || layoutAttribute.frame.origin.x == sectionInset.left {
            leftMargin = sectionInset.left
        }

        if layoutAttribute.frame.origin.x == sectionInset.left {
            leftMargin = sectionInset.left
        }
        else {
            layoutAttribute.frame.origin.x = leftMargin
        }

        leftMargin += layoutAttribute.frame.width + horizontalSpacing
        maxY = max(layoutAttribute.frame.maxY, maxY)
    }

    return attributes
}
}
