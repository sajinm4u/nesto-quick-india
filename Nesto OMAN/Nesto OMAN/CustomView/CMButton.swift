//
//  CMButton.swift
//  Nesto OMAN
//
//  Created by Sajin M on 18/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation
import UIKit


class CMButton: UIButton {

   let corner_radius : CGFloat =  5.0

   override func draw(_ rect: CGRect) {
       
       super.draw(rect)
       self.layer.cornerRadius = corner_radius
       self.clipsToBounds = true
   

   }

}
