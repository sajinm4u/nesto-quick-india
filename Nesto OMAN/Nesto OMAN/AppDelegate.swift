//
//  AppDelegate.swift
//  Nesto OMAN
//
//  Created by Sajin M on 13/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import GoogleMaps
import IQKeyboardManagerSwift
import Alertift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {


    var window: UIWindow?
   

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
       
        
        if #available(iOS 10.0, *) {
              // For iOS 10 display notification (sent via APNS)
              UNUserNotificationCenter.current().delegate = self
              
              let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
              UNUserNotificationCenter.current().requestAuthorization(
                  options: authOptions,
                  completionHandler: {_, _ in })
          } else {
              let settings: UIUserNotificationSettings =
                  UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
              application.registerUserNotificationSettings(settings)
          }
          
           UIApplication.shared.applicationIconBadgeNumber = 0
          application.registerForRemoteNotifications()
          FirebaseApp.configure()
          Messaging.messaging().delegate = self
          GMSServices.provideAPIKey(GoogleKey)
          IQKeyboardManager.shared.enable = true
        
        
        
        
        if !Reachability.isConnectedToNetwork(){
                      
            let alert = UIAlertController(title: noInternetTitle, message: noInternetMessage, preferredStyle: .alert)

            let actionYes = UIAlertAction(title: "Ok", style: .default, handler: { action in
            print("action yes handler")
            })

            alert.addAction(actionYes)
        
            DispatchQueue.main.async {
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
            
            return false
                      
            }
          
        
       
           Switcher.updateRootVC()
        // Override point for customization after application launch.
        return true
    }

  func applicationWillResignActive(_ application: UIApplication) {
         // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
         // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
     }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
      //  print(response.notification.description)
        print(response.notification.request.content.userInfo)
        
//        print("original identifier was : \(response.notification.request.identifier)")
//        print("original body was : \(response.notification.request.content.body)")
//        print("Tapped in notification")
    }

     func applicationDidEnterBackground(_ application: UIApplication) {
         

     }

     func applicationWillEnterForeground(_ application: UIApplication) {
         // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
     }

     func applicationDidBecomeActive(_ application: UIApplication) {
         // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     }

     func applicationWillTerminate(_ application: UIApplication) {
         // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
     }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
    }


}

extension AppDelegate : MessagingDelegate{
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        //print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        
        defualts.set(fcmToken, forKey: "fcmToken")
        
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
         
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }

    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
        
        print(remoteMessage)
    }
    
    
}

