//
//  Constants.swift
//  Nesto OMAN
//
//  Created by Sajin M on 18/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation
import UIKit







//Cell Identifiers


let bannerCell:String = "cell"
let homeCell:String = "homeTableCell"
let homeCollectionCell:String = "homeCollectionCell"
let mainCategoryCell:String = "MainCatCell"
let cartCell:String = "cartListCell"
let subCatCell:String = "SubCategoryCell"
let titleCell:String = "titleTextCell"
let subCategorySubCell:String = "subCatSubCell"
let addressCell:String = "AddressCell"
let orderCell:String = "OrderCell"
let searchCell:String = "SearchCell"
let sortCell:String = "SortCell"
let BrandTableCell:String = "brandTableCell"
let BrandCell:String = "brandCollectionCell"


//App Alert
let AppName = "Nesto Quick"

let noInternetTitle = "No Internet Connection"
let noInternetMessage = "Please check your internet connection."
let notValidEmail = "Please enter a valid email id"
let notValidCredentials = "Credentials entered not valid"
let cannotConnect = "Can't connect to server. Please try again later."
let fillAllFields = "Please fil all fields"
let passwordNotSame = "Password and Confirm Password should be same"
let paymentFailed = "Payment Failed"
let tryAgain = "Please try again!"
let pleaseLogin = "Please Log In to Continue"
let wentWrong = "Oops.. Something Went wrong"
let selectHotel = "Please select a hotel"
let roomSelection = "You have already made a selection"
let noHotels = "No Results Found"
let reashedLimit = "Reached Maximum Limit"
let selectLocation = "Please Select a Location"
let nameMandatory = "Name and Phone number should not be empty"
let selectGovernerate = "Please select a Governerate,Area and Street"
let selectGovCity = "Please select a Governerate,Area and Street"
let productAdded = "Product Added to cart successfully"
let notValidPhone = "Phone number is not valid"
let passwordMessage = "Password should contain at least 5 characters"
let confirm = "Confirm Cancellation"
let confimMessage = "Are you sure you want to cancel this order?"
let varyMessage = "Please remember the delivered quantity may vary from the actual ordered quantity"




//type def

let DashBoardReload:String = "HomeReload"
let MyOrderReload = "OrderReaload"
let ph:String = "ph"
let phSmall:String = "phSmall"

let currency:String = "\u{20B9} "
let countryCode:String = "91"
let null:String = ""
let landMark:String = " (Landmark)"
let mob:String = "Mobile "
let sub:String = "Subtotal "
let addressSelectNotification = "nq.selectAddress"
let addressEmptyNotification = "nq.emptyAddress"
let mrp:String = "MRP: "
let nrp:String = "NRP: "


//Google API key

let GoogleKey:String = "AIzaSyBd5SFAIX-tUYqxqDH5308SOM3N2JBfQoI"


//User Defaults

let defualts = UserDefaults.standard

let height: CGFloat = 230


enum Colors {
    
    static let NOGreen =  UIColor(red: 72/255, green: 158/255, blue: 89/255, alpha: 1.0)
    static let NOGray =  UIColor(red: 56/255, green: 56/255, blue: 56/255, alpha: 1.0)
}



//API Urls

//let BaseUrl:String =  "https://nestohypermarket.com/om/webservice/"

let BaseUrl:String =  "https://nestohypermarket.com/in/api/"
let tcUrl:String = "https://nestohypermarket.com/terms_and_conditions_india.html"
let customerCareEmail:String = "nestoquick@gmail.com"



enum Api{


static let banners = BaseUrl + "grocery_banners_list"
static let category = BaseUrl + "customer_grocery_main_products_category"
static let offerProduct =  BaseUrl + "grocery_offer_products"
static let homeSubCategory = BaseUrl + "customer_grocery_category_first_15_products"
static let getState = BaseUrl + "getGroceryStates"
static let getStateCity = BaseUrl + "getStateCity"
static let getStateCityStreet = BaseUrl + "getStateCityStreets"
static let getCustomerGrocery = BaseUrl + "customer_grocery_search"
static let getNearestGrocery = BaseUrl + "customer_nearest_grocery_search"
static let Login = BaseUrl + "signin"
static let SignUp = BaseUrl + "signup"
static let addCart = BaseUrl + "cart"
static let fcmRegistration = BaseUrl + "fcmRegister"
static let cartdetails = BaseUrl + "cartdetails"
static let subMainCategory = BaseUrl + "customer_grocery_category_groups"
static let subCategory = BaseUrl + "customer_grocery_category_groups_subcategory"
static let subCategoryProducts = BaseUrl + "customer_grocery_base_products_category"
static let getDeliveryAddress = BaseUrl + "list_customer_delivery_addresses"
static let myOrders = BaseUrl + "customer_order_history"
static let myWishList = BaseUrl + "customer_favourite_products_list"
static let cartCheckout = BaseUrl + "cart_check_out"
static let removeCart = BaseUrl + "remove_cart_data"
static let cartUpdate = BaseUrl + "cart_update"
static let updateAddress =  BaseUrl + "update_customer_address"                      //"update_delivery_details"
static let searchProduct = BaseUrl + "productsearchofgrocery"
static let logout = BaseUrl + "customerlogout"
static let search = BaseUrl + "productsearchofgrocery"
static let deleteAddress = BaseUrl + "delete_customer_address"
static let fcmRegister = BaseUrl + "fcmRegister"
static let orderDetails = BaseUrl + "customer_order_history_detail"
static let refundPolicy = BaseUrl + "grocery_return_policy"
static let makeFavorite = BaseUrl + "customer_favourite_products"
static let makeUnFavorite = BaseUrl + "customer_unfavourite_products"
static let forgotPassword = BaseUrl + "forgotPassword"
static let resendOtp = BaseUrl + "resendOtp"
static let fcmSignIn = BaseUrl + "fcmSignIn"
static let updatePassword = BaseUrl + "password_change"
static let versionUpdate = BaseUrl + "app_update"
static let singleProduct = BaseUrl + "interactive_banner_list_product"
static let brandList = BaseUrl + "brand_list"
static let byBrands = BaseUrl + "grocery_products_lists_based_on_brands"
static let cancelOrder = BaseUrl + "customer_order_cancel"
static let currentAddress = BaseUrl + "customer_shipping_details"
static let addAddress = BaseUrl + "add_customer_addess"
static let timeSlots = BaseUrl + "list_time_slotes"
static let deliveryMethod = BaseUrl + "list_delivery_methods"
   

}
