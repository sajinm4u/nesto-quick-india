//
//  ParameterModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 18/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation




struct LoginParameters {
    
    
    var username:String = ""
    var password:String = ""
   
    
    
    var Values: [String: String] {
        return [

            "user_name":username,
            "password":password
        ]
    }
  
}




struct TimeSlotParameters {
    
    
    var groceryId:String = ""
    var methodId:String = ""
    var customerId:String = ""
   
    
    
    var Values: [String: String] {
        return [

            "groceryId":groceryId,
            "customerId":customerId,
            "methodId":methodId
        ]
    }
  
}




struct AddressParams {
    
    
    var customerId:String = ""
    var customer_name:String = ""
    var house_name:String = ""
    var land_mark:String = ""
    var customer_area:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var customer_city:String = ""
    var customer_mobile:String = ""
    var addressTitle:String = ""
    
      var Values: [String: String] {
          return [

              "customerId":customerId,
              "customer_name":customer_name,
              "house_name":house_name,
              "land_mark":land_mark,
              "customer_area":customer_area,
              "latitude":latitude,
              "longitude":longitude,
              "customer_city":customer_city,
              "customer_mobile":customer_mobile,
              "adress_title":addressTitle,
          ]
      }
    
}




struct UpdateAddressParams {
    
    var addressId:String = ""
    var customerId:String = ""
    var customer_name:String = ""
    var house_name:String = ""
    var land_mark:String = ""
    var customer_area:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var customer_city:String = ""
    var customer_mobile:String = ""
    var addressTitle:String = ""
    
      var Values: [String: String] {
          return [
 
              "addressId":addressId,
              "customerId":customerId,
              "customer_name":customer_name,
              "house_name":house_name,
              "land_mark":land_mark,
              "customer_area":customer_area,
              "latitude":latitude,
              "longitude":longitude,
              "customer_city":customer_city,
              "customer_mobile":customer_mobile,
              "adress_title":addressTitle
          ]
      }
    
}






struct FcmRegisterParam {
    
    var fcm_regId:String = ""
     var user_id:String = ""
    

     
     var Values: [String: String] {
           return [

               "device_type":"2",
               "user_type":"1",
               "fcm_regId":fcm_regId,
               "user_id":user_id
               
               
           ]
       }
    
 
    
}

struct SearchParam {
    
    var groceryId:String = ""
     var searchKey:String = ""
    var limit:String = ""
     
     var Values: [String: String] {
           return [

               "groceryId":groceryId,
               "searchKey":searchKey,
               "status":"1",
               "limit":limit
               
               
           ]
       }
    
 
    
}

struct DeleteParam {
    

       var customerId:String = ""
        var addressId:String = ""
       
        var Values: [String: String] {
              return [

                  "customerId":customerId,
                  "addressId":addressId
                  
                 

              ]
          }
       
    
    
}






struct updateParam {
    
    var id:String = ""
    var quantity:String = ""
    var price:String = ""
    var customerId:String = ""
    
    var Values: [String: String] {
          return [

              "id":id,
              "quantity":quantity,
              "price":price,
              "customerId":customerId
              
              
          ]
      }
}


struct CustomerParams {
    
    var customerId:String = ""
    var limit:String = ""
    
    var Values: [String: String] {
          return [

              "customerId":customerId,
              "limit":limit
              
              
          ]
      }
}

struct FcmRegisterParams{
    
    
    var fcmToken:String = ""
    var customerId:String = ""
    var otpVerify:String = ""

  
    
    
    var Values: [String: String] {
          return [

              "fcm_regId":fcmToken,
              "user_id":customerId,
              "user_type":"1",
              "device_type":"2",
              "otpVerify":otpVerify
              
          ]
      }
    
  
    
}

struct SendSms{
    
    
    var mobile:String = ""
    
    
    var Values: [String: String] {
          return [

              "phone":mobile
              
          ]
      }
    
  
    
}


struct SubMainParam {
    
    var mainCatId:String
       
       var Values: [String: String] {
             return [
               
               "mainCatId":mainCatId
               
             ]
         }
       

}

struct BrandProductParams {
    
     var groceryId:String
     var brand:String
     var customerId:String
     var limit:String
     var sortKey:String = ""
    
        
        var Values: [String: String] {
              return [
                
                "groceryId":groceryId,
                "brand":brand,
                "customeId":customerId,
                "page":limit,
                "sortKey":sortKey
                
                
                
              ]
          }
        

    
}



struct SubCatProductParams {
    
     var groceryId:String
     var subCategoryId:String
     var customerId:String
     var limit:String
     var sortKey:String = ""
    
        
        var Values: [String: String] {
              return [
                
                "groceryId":groceryId,
                "subCategoryId":subCategoryId,
                "customeId":customerId,
                "page":limit,
                "sortKey":sortKey
                
                
                
              ]
          }
        

    
}


struct VersionParams {
    
    var version:String
  
    var Values: [String: String] {
                 return [
                   
                   "version":version,
                   "deviceType":"2",
                  
                 ]
             }
    
    
}


struct CategoryParam {
    
    var groupId:String
    var keyFlag:String
       
       var Values: [String: String] {
             return [
               
               "groupId":groupId,
               "keyFlag":keyFlag
               
             ]
         }
       

}





struct FavParams{
    
    
    var customerId:String = ""
    var groceryId:String = ""
    var productId:String = ""
    
    
    var Values: [String: String] {
          return [

              "customerId":customerId,
              "groceryId":groceryId,
              "productId":productId
          ]
      }
    
}


struct CartParams{
    
    
    var customerId:String = ""
    var groceryId:String = ""
    
    
    var Values: [String: String] {
          return [

              "customerId":customerId,
              "groceryId":groceryId
          ]
      }
    
}

struct SignUpParameters {
    
    
    var firstname:String = ""
    var email_id:String = ""
    var password:String = ""
    var phone:String = ""
    
    
    var Values: [String: String] {
        return [

            "firstname":firstname,
            "lastname":"",
            "email_id":email_id,
            "password":password,
            "phone_1":phone,
        ]
    }
  
}

struct LogOutParams {
    
    
    var customerId:String
    var fcmId:String
    
    var Values: [String: String] {
        return [

            "customerId":customerId,
            "fcmId":fcmId
            
        ]
    }
    
    
}



struct NearestParams{
    
    
    var lattitude:String = ""
    var longitude:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "lattitude":lattitude,
            "longitude":longitude
            
          ]
      }
    
    
}


struct LocationParameter {
    
    var stateId:String = ""
    var cityId:String = ""
    var streetId:String = ""
    
    var Values: [String: String] {
          return [
            
            "stateId":stateId,
            "cityId":cityId,
            "streetId":streetId
            
          ]
      }
    
    
}



struct AddtoCartParam{
    
    var productId:String = ""
    var groceryId:String = ""
    var quantity:String = ""
    var weight:String = ""
    var price:String = ""
    var article:String = ""
    var customerId:String = ""
    var description:String = ""
    
    var Values: [String: String] {
        return [
          
          "productId":productId,
          "groceryId":groceryId,
          "quantity":quantity,
          "weight":weight,
          "price":price,
          "article":article,
          "customerId":customerId,
          "description":description
          
        ]
    }
    
    
}


struct BannerParameters {
    
    var groceryId:String
    
    var Values: [String: String] {
          return [
            
            "groceryId":groceryId
            
          ]
      }
    
}


struct CategoryParameters {
    
    var groceryId:String
    
    
    var Values: [String: String] {
          return [
            
            "groceryId":groceryId
            
          ]
      }
    
}



struct CartCheckOut{
    
    
    var paymentMethod:String
    var specialInstructions:String
    var addressId:String
    var groceryId:String
    var customerId:String
    var timeSlotId:String
    var deliveryTypeId:String
    
    var Values: [String: String] {
        
          return [
            
            "paymentMethod":paymentMethod,
            "specialInstruction":specialInstructions,
            "addressId":addressId,
            "groceryId":groceryId,
            "customerId":customerId,
            "timeSlotId":timeSlotId,
            "deliveryTypeId":deliveryTypeId
            
          ]
      }
    
    
}

struct CategoryWithCutomerId{
    
    
    var groceryId:String
    var customerId:String
    var sortKey:String = ""
    
    var Values: [String: String] {
          return [
            
            "groceryId":groceryId,
            "customerId":customerId,
            "sortKey":sortKey
            
          ]
      }
    
    
}

struct OfferProductParameters {
    
    var groceryId:String
    var customerId:String
    var sortKey:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "groceryId":groceryId,
            "customerId":customerId,
            "sortKey":sortKey
           
          ]
      }
    
}


struct CancelProductParameters {
    
    var orderId:String
    var customerId:String
    
    
    var Values: [String: String] {
          return [
            
            "orderId":orderId,
            "customerId":customerId
          ]
      }
    
}




