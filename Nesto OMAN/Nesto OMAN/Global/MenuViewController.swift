//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32,_ title:String)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    var titleArray:[String]?
    var sortedTitleArray:[String]?
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
    }
    
   func updateArrayMenuOptions(){
       
    
    if defualts.string(forKey: "customerId") != nil{
        
               arrayMenuOptions.append(["title":"Home", "icon":"ic_home"])
               arrayMenuOptions.append(["title":"My Orders", "icon":"ic_orders"])
               arrayMenuOptions.append(["title":"My Wishlist", "icon":"ic_wishList"])
               arrayMenuOptions.append(["title":"Terms & Conditions", "icon":"ic_terms"])
               arrayMenuOptions.append(["title":"About Us", "icon":"ic_about"])
               arrayMenuOptions.append(["title":"Sign Out", "icon":"ic_signOut"])
        
    }else{
        
          arrayMenuOptions.append(["title":"Home", "icon":"ic_home"])
          arrayMenuOptions.append(["title":"Terms & Conditions", "icon":"ic_terms"])
          arrayMenuOptions.append(["title":"About Us", "icon":"ic_about"])
          arrayMenuOptions.append(["title":"Sign In", "icon":"ic_signIn"])
    }
    
    
      
        tblMenuOptions.reloadData()
    }
    
    
    @IBAction func chatPressed(_ sender: Any) {
        
        guard let phone = defualts.string(forKey: "groceryPhone") else{
            
            return
        }
        


        let whatsUrl = "whatsapp://send?phone="
        let whatsMessage = "&text=Hi%2C%20Nesto"
        
        var whatsStr = whatsUrl + phone + whatsMessage
        
       
        
        whatsStr = whatsStr.replacingOccurrences(of: "+", with: "")
       
        
        let whatsappURL = URL(string: whatsStr)
        if let whatsappURL = whatsappURL {
            if UIApplication.shared.canOpenURL(whatsappURL) {
                UIApplication.shared.open(whatsappURL)
            }
        }
        

    }
    
    
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            let indexValue = Int(Int32(button.tag))
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            
        
          
            delegate?.slideMenuItemSelectedAtIndex(index,arrayMenuOptions[indexValue]["title"]!)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParent()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
     

        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}
