//
//  TimeSlotModel.swift
//  Nesto Quick
//
//  Created by Sajin M on 18/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct TimeSlotModel:Codable {
    
    var status:Bool
    var message:String
    var arrAvailableTimeSlotes:[timeSlotData]?
}


struct timeSlotData:Codable{
    

        var id:String?
        var slotDate:String?
        var timeslotes:String?
        var alert_message:String?
    
    
}
