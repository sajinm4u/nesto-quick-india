//
//  VerifyUser.swift
//  Nesto OMAN
//
//  Created by Sajin M on 02/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct VerifyUser:Codable{
    
    var status:Bool
    var message:String
    var customerId:String?
    var regId:String?
    var customerName:String?
    var customerEmailId:String?
    var customerPhone:String?
    var authorization:String?
    
   
}
