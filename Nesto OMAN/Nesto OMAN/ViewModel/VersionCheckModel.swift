//
//  VersionCheckModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 23/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct VersionCheckModel:Codable {
    var status:Bool
}
