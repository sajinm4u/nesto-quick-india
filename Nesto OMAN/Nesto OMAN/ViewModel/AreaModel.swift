//
//  AreaModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 23/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct AreaModel:Codable {
    
    var status:Bool
    var message:String
    var streets:[streetData]?
    var state:[stateData]?
    var cities:[citiesData]?
}


struct streetData:Codable{
    
    var id:String
    var street:String?
    
}


struct stateData:Codable{
    
    var id:String
    var state:String?
    
}

struct citiesData:Codable {
    
    var id:String
    var city:String?
}
