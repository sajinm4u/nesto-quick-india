//
//  OtpResendModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 10/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct OtpResendModel:Codable {

        var status:Bool
        var otp:String?
        var message:String
  
}
