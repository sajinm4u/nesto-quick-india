//
//  WishlistModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 03/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct WishlistModel:Codable {
    
    var status:Bool
    var message:String
    var favouriteProducts:[OfferProductData]?
    var data_count:String
    var appFavouriteURL:String?
        
    
    
}
