//
//  MainCategoryModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 20/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct MainCategoryModel:Codable {
    
    var status:Bool
    var message:String
    var mainCategory:[MainCategoryData]
    var data_count:String?
    var cartCount:String?
    var rating:String?
    var order_id:String?
    var order_status:String?
    var min_purchase_amount:String?
    var delivery_charge:String?
    var payment_types:String?
    var duration_for_delivery:String?
    var delivery_time: String?
    
}

struct MainCategoryData:Codable{
    
    var id:Int
    var grocery_id:String?
    var categoryImage:String?
    var mainCategory:String?
    var description:String?
    var isGroup:Int?
    
  
}
