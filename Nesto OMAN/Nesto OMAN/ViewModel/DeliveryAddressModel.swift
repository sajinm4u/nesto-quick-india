//
//  DeliveryAddressModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 01/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct DeliveryAddressModel:Codable {
    var status:Bool
    var message:String
    var arrAdress:[DeliveryAddressData]?
    var deliverableKm:String?
    var data_count:String?
}

struct DeliveryAddressData:Codable {
    
    var address_Id:String?
    var customer:String?
    var address_title:String?
    var land_mark:String?
    var house_name:String?
    var customer_mobile:String?
    var latitude:String?
    var longitude:String?
    var is_deliverable:String?
    var customer_City:String?
    var customer_area:String?
    
}



