//
//  SubMainModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 27/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct SubMainModel:Codable{
    
    var status:Bool
    var message:String
    var arrCategoryGroups:[subMainData]?
    var mainCategoryBanner:String?
    var count:String?
    
}


struct subMainData:Codable{
    
    var groupId:String?
    var groupName:String?
    var groupImage:String?
    
    
}
