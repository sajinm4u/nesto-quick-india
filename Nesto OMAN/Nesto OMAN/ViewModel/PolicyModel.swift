//
//  PolicyModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 07/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct PolicyModel:Codable{
    
    var status:Bool
    var message:String
    var arrPolicy:[PolicyData]?
    
    
    
}

struct PolicyData:Codable {
    
    var id:String?
    var storeId:String?
    var return_policy:String?
    
}
