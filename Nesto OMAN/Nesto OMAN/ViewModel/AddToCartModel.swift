//
//  AddToCartModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 25/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct AddToCartModel:Codable{

        var status:Bool
        var message:String
        var cartCount:String?
  
}
