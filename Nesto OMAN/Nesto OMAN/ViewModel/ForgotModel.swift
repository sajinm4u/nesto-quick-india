//
//  ForgotModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 14/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct ForgotModel:Codable {
    
    var status:Bool
    var otp:String?
    var customerId:String?
    var message:String
    
}

