//
//  OrderDetails.swift
//  Nesto OMAN
//
//  Created by Sajin M on 07/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct OrderDetails:Codable {
    

    var status: Bool
    var message: String
    var shipAddress:String?
    var order_id: String?
    var order_status:String?
    var data_count:String?
    var orderDetails:[orderDetailsData]?
}


struct orderDetailsData:Codable{
    
     var product_name: String?
     var weight:String?
     var qty_weight:String?
     var qty:String?
     var product_price:String?
     var product_image1:String?
    
    
}
     
