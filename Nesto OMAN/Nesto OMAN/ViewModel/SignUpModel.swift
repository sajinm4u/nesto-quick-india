//
//  SignUpModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 26/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct SignUpModel:Codable {

    
        var status:Bool
        var message:String
        var customer_id:String?
        var verify:Bool
    
    
}
