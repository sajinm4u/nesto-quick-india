//
//  BrandModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 10/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct BrandModel:Codable {
    
    var result:Bool
    var message:String
    var brands:[BrandModelData]?
}


struct BrandModelData:Codable {
    
    var brand:String?
    var brandLogo:String?
    var brandBanner:String?
    
}

