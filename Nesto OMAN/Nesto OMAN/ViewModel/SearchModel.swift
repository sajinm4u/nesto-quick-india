//
//  SearchModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 04/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct SearchModel:Codable{
    
    var status:Bool
    var message:String
    var products:[CartProducts]?
    var data_count:String?
    var totalCount:String?
    
}
