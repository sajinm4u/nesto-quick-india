//
//  OrderHistory.swift
//  Nesto OMAN
//
//  Created by Sajin M on 03/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct OrderHistory:Codable {
    
    var status:Bool
    var message:String
    var customerhistory:[OrderData]?
    var data_count:String?
    var totalCount:String?
    
}


struct OrderData:Codable{
    
    
    var order_id:String?
    var orderIdSys:String?
    var totItems:String?
    var grocery_id:String?
    var net_amount:String?
    var store_name:String?
    var order_status:String?
    var delivery_date:String?
    
}
