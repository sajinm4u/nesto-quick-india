//
//  ProductListModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 28/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct ProductListModel:Codable{
    
    var status:Bool
    var message:String
    var data_count:String?
    var total_count:String?
    var total_pages:String?
    var products:[OfferProductData]?
}
