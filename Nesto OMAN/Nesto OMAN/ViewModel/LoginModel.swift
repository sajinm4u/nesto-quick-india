//
//  LoginModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 24/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct LoginModel:Codable {
    
    var status:Bool
    var message:String
    var customer_details:[CustomerData]?
    var user_exist:Bool
    
}

struct CustomerData:Codable {
    
    var id:Int?
    var firstname:String?
    var lastname:String?
    var user_name:String?
    var adress:String?
    var gender:String?
    var email_id:String?
    var phone_1:String?
    var makani_no_1:String?
    var Authorization:String?
    
}
