//
//  OfferProducts.swift
//  Nesto OMAN
//
//  Created by Sajin M on 21/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct OfferProducts:Codable {
    
    var status:Bool
    var message:String
    var eachCategory:[OfferProductData]?
    var launching_offer:String?
    var data_count:String
    var appFifteenURL_Image_1:String?
    var appFifteenURL_Image_2:String?
    var appofferURL:String?
    
}

struct OfferProductData:Codable {
    
    var id:String?
    var category_id:String?
    var category:String?
    var brand:String?
    var productName:String?
    var product_price:String?
    var groceryproductprice:String?
    var offerPrice:String?
    var offerPercentage:String?
    var weight:String?
    var qty_weight:String?
    var productCode:String?
    var productID:String?
    var favourite:String?
    var max_purchase_qty:String?
    var product_image1:String?
    var quantity:String?
    var image1:String?
    var price:String?
    var offer_price:String?
    var product:String?
    var descriptions:String?
    var offer_percentage:String?
    var is_special_offer_product:Bool?
    var gorceryproductprice:String?
    
    
    
    
}


