//
//  SingleProductModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 06/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct SingleProductModel:Codable {
    
    var status:Bool
    var message:String
    var products:[CartProducts]?
    var data_count:String?
    
    
}
