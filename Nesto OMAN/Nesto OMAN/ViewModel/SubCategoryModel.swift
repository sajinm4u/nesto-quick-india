//
//  SubCategoryModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 21/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct SubCategoryModel:Codable{
    
    var status:Bool
       var message:String
       var arrCategoryGroups:[MainCategoryData]
       var count:Int?
    
    
}


struct SubCategoryData:Codable{
    
    var groupId:String
    var groupName:String?
    var groupImage:String?
    
    
}
