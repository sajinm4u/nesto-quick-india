//
//  BrandProductModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 11/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct BrandProductModel:Codable{
    
    
    var status:Bool
    var message:String
    var products:[OfferProductData]?
    var brand_banner:String?
    var data_count:String?
    var total_count:String?
    var total_pages:String?
    
}
