//
//  DeliveryModel.swift
//  Nesto Quick
//
//  Created by Sajin M on 19/08/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct DeliveryModel:Codable{
    
    var status:Bool
    var message:String
    var result:[methodData]?
}


struct methodData:Codable{
    
    var id:String?
    var method:String?
    var addition_charge:String?
    var is_method_active:Int?
    
}
