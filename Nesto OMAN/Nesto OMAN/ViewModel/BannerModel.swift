//
//  BannerModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 18/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct BannerModel:Codable{
    
    
    var result:Bool
    var message:String?
    var banners:[BannerData]?
    var bannerTitle:String?
    var bannerBody:String?
    var bannerIcon:String?
    var homeGif:String?
    
}

struct BannerData:Codable{
    
    var id:String?
    var title:String?
    var banner_image:String?
    var is_category_banner:String?
    var sub_category_id:String?
    var sub_category:String?
    var product_Id:String?
}


