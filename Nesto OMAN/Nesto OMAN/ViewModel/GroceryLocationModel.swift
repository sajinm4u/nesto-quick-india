//
//  GroceryLocationModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 24/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct GroceryLocationModel:Codable {
    
    var status:Bool
    var message:String
    var arrGrocery: [LocationData]
    var data_count:Int
    
    
}


struct LocationData:Codable {
    
    
    var id:Int?
    var store_name:String?
    var contact_person:String?
    var address:String?
    var phone_1:String?
    var latitude:String?
    var longitude:String?
    var email_1:String?
    var rating:String?
    var rating_person_count:String?
    var min_purchase_amount:String?
    var cityId:String?
    var city:String?
    var image1:String?
    var is_active:Int?
             
    
}
