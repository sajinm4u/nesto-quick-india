//
//  CartModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 26/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct CartModel:Codable {
    
    var status:Bool
    var message:String
    var products:[CartProducts]?
    var data_count:String?
    var min_purchase_amount:String?
    var delivery_time:String?
}


struct CartProducts:Codable {
    
    var id:Int?
    var product_id:String?
    var product_name:String?
    var qty_weight:String?
    var qty:String?
    var product_image1:String?
    var grocery_id:String?
    var descriptions:String?
    var price:String?
    var weight:String?
    var vat_perc:String?
    var article:String?
    var category_id:String?
    var max_purchase_quantity:String?
    var is_active:String?
    var offer_percentage:String?
    var offer_price:String?
    var favourite:String?
    var image1:String?
    var is_special_offer_product:Bool?
}
