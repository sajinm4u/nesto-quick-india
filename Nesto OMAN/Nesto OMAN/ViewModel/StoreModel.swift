//
//  StoreModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 30/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct StoreModel:Codable{
    
    var status:Bool
    var message:String
    var arrStore:[StoreData]?
    
    
}

struct StoreData:Codable{
    
    var storeId:String?
    var store_name:String?
    var phone_1:String?
    var address:String?
    var latitude:String?
    var longitude:String?
    var cityId:String?
    var city:String?
    
}
